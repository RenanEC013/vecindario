package br.unicamp.vecindario;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.google.android.gms.analytics.Tracker;

import java.util.ArrayList;

/**
 * Created by renan on 23/07/2016.
 */
public class SopaDeLetras2Activity extends AppCompatActivity {
    public SopaDeLetras2Activity() {

    }

    Toolbar toolbar;

    /**
     * [Ricardo] GoogleAnalytics Tracker
     * The {@link Tracker} used to record screen views.
     */
    private static final String TAG = "SopaDeLetras2Activity";

    private static final int NONE = 0;
    private static final int SWIPE = 1;
    private int mode = NONE;
    private float startX;
    private float stopX;
    // We will only detect a swipe if the difference is at least 100 pixels
// Change this value to your needs
    private static final int TRESHOLD = 100;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // [Ricardo] --------------- Logging activities ---------------------
        Log.d(TAG, "[1] onCreate");
        AnalyticsApplication application = (AnalyticsApplication) getApplication();

        // Log = ThisClass
        // Logging the overall usage of this screen. Does not log the user or current time.
        LoginActivity.logMessageToAnalytics(TAG + "- onCreate", application);

        // Log = [Ricardo Edgard Caceffo][Thu Nov 17 14:36:22 GMT-02:00 2016][LoginActivity][onCreate]
        String s = LoginActivity.generateLogMessage(TAG,"onCreate");
        LoginActivity.logMessageToAnalytics(s, application);
        // [Ricardo] --------------- Logging activities ---------------------

        setContentView(R.layout.activity_vocabulario2);

        SeekBar seekBar = (SeekBar)findViewById(R.id.seekBar2);
        seekBar.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return true;
            }
        });

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Sopa de Letras");
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        RelativeLayout relativeLayout = (RelativeLayout)findViewById(R.id.vocabulario2);
        relativeLayout.setOnTouchListener(
                new RelativeLayout.OnTouchListener() {

                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        switch (event.getAction() & MotionEvent.ACTION_MASK) {

                            case MotionEvent.ACTION_DOWN:


                                // [Ricardo] --------------- Logging activities ---------------------
                                Log.d(TAG, "[92] MotionEvent.ACTION_UP");
                                AnalyticsApplication application1 = (AnalyticsApplication) getApplication();

                                // Log = ThisClass
                                // Logging the overall usage of this screen. Does not log the user or current time.
                                LoginActivity.logMessageToAnalytics(TAG + "- MotionEvent.ACTION_UP", application1);

                                // Log = [Ricardo Edgard Caceffo][Thu Nov 17 14:36:22 GMT-02:00 2016][LoginActivity][MotionEvent.ACTION_UP]
                                String s1 = LoginActivity.generateLogMessage(TAG, "MotionEvent.ACTION_UP");
                                LoginActivity.logMessageToAnalytics(s1, application1);
                                // [Ricardo] --------------- Logging activities ---------------------


                                // This happens when you touch the screen with two fingers
                                mode = SWIPE;
                                // You can also use event.getY(1) or the average of the two
                                startX = event.getX(0);
                                break;

                            case MotionEvent.ACTION_UP:

                                // [Ricardo] --------------- Logging activities ---------------------
                                Log.d(TAG, "[92] MotionEvent.ACTION_UP");
                                AnalyticsApplication application2 = (AnalyticsApplication) getApplication();

                                // Log = ThisClass
                                // Logging the overall usage of this screen. Does not log the user or current time.
                                LoginActivity.logMessageToAnalytics(TAG + "- MotionEvent.ACTION_UP", application2);

                                // Log = [Ricardo Edgard Caceffo][Thu Nov 17 14:36:22 GMT-02:00 2016][LoginActivity][MotionEvent.ACTION_UP]
                                String s2 = LoginActivity.generateLogMessage(TAG, "MotionEvent.ACTION_UP");
                                LoginActivity.logMessageToAnalytics(s2, application2);
                                // [Ricardo] --------------- Logging activities ---------------------

                                // This happens when you release the second finger
                                mode = NONE;
                                if (Math.abs(startX - stopX) > TRESHOLD) {
                                    if (startX < stopX) {
                                        Intent intent = new Intent(v.getContext(), SopaDeLetras1Activity.class);
                                        startActivity(intent);
                                    }
                                }
                                mode = NONE;
                                break;

                            case MotionEvent.ACTION_MOVE:

                                // [Ricardo] --------------- Logging activities ---------------------
                                Log.d(TAG, "[93] MotionEvent.ACTION_MOVE");
                                AnalyticsApplication application3 = (AnalyticsApplication) getApplication();

                                // Log = ThisClass
                                // Logging the overall usage of this screen. Does not log the user or current time.
                                LoginActivity.logMessageToAnalytics(TAG + "- MotionEvent.ACTION_MOVE", application3);

                                // Log = [Ricardo Edgard Caceffo][Thu Nov 17 14:36:22 GMT-02:00 2016][LoginActivity][MotionEvent.ACTION_MOVE]
                                String s3 = LoginActivity.generateLogMessage(TAG, "MotionEvent.ACTION_MOVE");
                                LoginActivity.logMessageToAnalytics(s3, application3);
                                // [Ricardo] --------------- Logging activities ---------------------

                                if (mode == SWIPE) {
                                    stopX = event.getX(0);
                                }
                                break;
                        }

                        return true;
                    }
                });

        TextView argentina = (TextView) findViewById(R.id.textView130);
        TextView madrid = (TextView) findViewById(R.id.textView127);
        TextView mexico = (TextView) findViewById(R.id.textView126);
        TextView nino = (TextView) findViewById(R.id.textView138);
        TextView raton = (TextView) findViewById(R.id.textView135);
        TextView coche = (TextView) findViewById(R.id.textView137);
        TextView carretera = (TextView) findViewById(R.id.textView136);
        TextView zapatos = (TextView) findViewById(R.id.textView133);
        TextView gato = (TextView) findViewById(R.id.textView134);
        TextView vaca = (TextView) findViewById(R.id.textView131);
        TextView hoy = (TextView) findViewById(R.id.textView132);
        TextView lluvia = (TextView) findViewById(R.id.textView128);

        argentina.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // [Ricardo] --------------- Logging activities ---------------------
                Log.d(TAG, "[91] Play Argentina" );
                AnalyticsApplication application = (AnalyticsApplication) getApplication();

                // Log = ThisClass
                // Logging the overall usage of this screen. Does not log the user or current time.
                LoginActivity.logMessageToAnalytics(TAG + "- Play Argentina", application);

                // Log = [Ricardo Edgard Caceffo][Thu Nov 17 14:36:22 GMT-02:00 2016][LoginActivity][Play Argentina"]
                String s = LoginActivity.generateLogMessage(TAG,"Play Argentina");
                LoginActivity.logMessageToAnalytics(s, application);
                // [Ricardo] --------------- Logging activities ---------------------

                Intent intent = new Intent(v.getContext(), ArgentinaActivity.class);
                intent.putExtra("key","argentina");
                startActivity(intent);
            }
        });

        madrid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // [Ricardo] --------------- Logging activities ---------------------
                Log.d(TAG, "[92] Play Madrid" );
                AnalyticsApplication application = (AnalyticsApplication) getApplication();

                // Log = ThisClass
                // Logging the overall usage of this screen. Does not log the user or current time.
                LoginActivity.logMessageToAnalytics(TAG + "- Play Madrid", application);

                // Log = [Ricardo Edgard Caceffo][Thu Nov 17 14:36:22 GMT-02:00 2016][LoginActivity][Play Madrid"]
                String s = LoginActivity.generateLogMessage(TAG,"Play Madrid");
                LoginActivity.logMessageToAnalytics(s, application);
                // [Ricardo] --------------- Logging activities ---------------------


                Intent intent = new Intent(v.getContext(), ArgentinaActivity.class);
                intent.putExtra("key", "madrid");
                startActivity(intent);
            }
        });

       zapatos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // [Ricardo] --------------- Logging activities ---------------------
                Log.d(TAG, "[93] Play Zapatos" );
                AnalyticsApplication application = (AnalyticsApplication) getApplication();

                // Log = ThisClass
                // Logging the overall usage of this screen. Does not log the user or current time.
                LoginActivity.logMessageToAnalytics(TAG + "- Play Zapatos", application);

                // Log = [Ricardo Edgard Caceffo][Thu Nov 17 14:36:22 GMT-02:00 2016][LoginActivity][PPlay Zapatos"]
                String s = LoginActivity.generateLogMessage(TAG,"Play Zapatos");
                LoginActivity.logMessageToAnalytics(s, application);
                // [Ricardo] --------------- Logging activities ---------------------

                Intent intent = new Intent(v.getContext(), ArgentinaActivity.class);
                intent.putExtra("key", "zapatos");
                startActivity(intent);
            }
        });

        mexico.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // [Ricardo] --------------- Logging activities ---------------------
                Log.d(TAG, "[94] Play Mexico" );
                AnalyticsApplication application = (AnalyticsApplication) getApplication();

                // Log = ThisClass
                // Logging the overall usage of this screen. Does not log the user or current time.
                LoginActivity.logMessageToAnalytics(TAG + "- Play Mexico", application);

                // Log = [Ricardo Edgard Caceffo][Thu Nov 17 14:36:22 GMT-02:00 2016][LoginActivity][Play Mexico"]
                String s = LoginActivity.generateLogMessage(TAG,"Play Mexico");
                LoginActivity.logMessageToAnalytics(s, application);
                // [Ricardo] --------------- Logging activities ---------------------

                Intent intent = new Intent(v.getContext(), ArgentinaActivity.class);
                intent.putExtra("key", "mexico");
                startActivity(intent);
            }
        });

        nino.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // [Ricardo] --------------- Logging activities ---------------------
                Log.d(TAG, "[95] Play Nino" );
                AnalyticsApplication application = (AnalyticsApplication) getApplication();

                // Log = ThisClass
                // Logging the overall usage of this screen. Does not log the user or current time.
                LoginActivity.logMessageToAnalytics(TAG + "- Play Nino", application);

                // Log = [Ricardo Edgard Caceffo][Thu Nov 17 14:36:22 GMT-02:00 2016][LoginActivity][Play Nino"]
                String s = LoginActivity.generateLogMessage(TAG,"Play Nino");
                LoginActivity.logMessageToAnalytics(s, application);
                // [Ricardo] --------------- Logging activities ---------------------

                Intent intent = new Intent(v.getContext(), ArgentinaActivity.class);
                intent.putExtra("key", "nino");
                startActivity(intent);
            }
        });

        coche.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // [Ricardo] --------------- Logging activities ---------------------
                Log.d(TAG, "[96] Play Coche" );
                AnalyticsApplication application = (AnalyticsApplication) getApplication();

                // Log = ThisClass
                // Logging the overall usage of this screen. Does not log the user or current time.
                LoginActivity.logMessageToAnalytics(TAG + "- Play Coche", application);

                // Log = [Ricardo Edgard Caceffo][Thu Nov 17 14:36:22 GMT-02:00 2016][LoginActivity][Play Coche"]
                String s = LoginActivity.generateLogMessage(TAG,"Play Coche");
                LoginActivity.logMessageToAnalytics(s, application);
                // [Ricardo] --------------- Logging activities ---------------------

                Intent intent = new Intent(v.getContext(), ArgentinaActivity.class);
                intent.putExtra("key", "coche");
                startActivity(intent);
            }
        });

        vaca.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // [Ricardo] --------------- Logging activities ---------------------
                Log.d(TAG, "[97] Play Vaca" );
                AnalyticsApplication application = (AnalyticsApplication) getApplication();

                // Log = ThisClass
                // Logging the overall usage of this screen. Does not log the user or current time.
                LoginActivity.logMessageToAnalytics(TAG + "- Play Vaca", application);

                // Log = [Ricardo Edgard Caceffo][Thu Nov 17 14:36:22 GMT-02:00 2016][LoginActivity][Play Vaca"]
                String s = LoginActivity.generateLogMessage(TAG,"Play Vaca");
                LoginActivity.logMessageToAnalytics(s, application);
                // [Ricardo] --------------- Logging activities ---------------------

                Intent intent = new Intent(v.getContext(), ArgentinaActivity.class);
                intent.putExtra("key", "vaca");
                startActivity(intent);
            }
        });

        gato.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // [Ricardo] --------------- Logging activities ---------------------
                Log.d(TAG, "[98] Play Gato" );
                AnalyticsApplication application = (AnalyticsApplication) getApplication();

                // Log = ThisClass
                // Logging the overall usage of this screen. Does not log the user or current time.
                LoginActivity.logMessageToAnalytics(TAG + "- Play Gato", application);

                // Log = [Ricardo Edgard Caceffo][Thu Nov 17 14:36:22 GMT-02:00 2016][LoginActivity][Play Gato"]
                String s = LoginActivity.generateLogMessage(TAG,"Play Gato");
                LoginActivity.logMessageToAnalytics(s, application);
                // [Ricardo] --------------- Logging activities ---------------------

                Intent intent = new Intent(v.getContext(), ArgentinaActivity.class);
                intent.putExtra("key", "gato");
                startActivity(intent);
            }
        });

        raton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // [Ricardo] --------------- Logging activities ---------------------
                Log.d(TAG, "[99] Play Raton" );
                AnalyticsApplication application = (AnalyticsApplication) getApplication();

                // Log = ThisClass
                // Logging the overall usage of this screen. Does not log the user or current time.
                LoginActivity.logMessageToAnalytics(TAG + "- Play Raton", application);

                // Log = [Ricardo Edgard Caceffo][Thu Nov 17 14:36:22 GMT-02:00 2016][LoginActivity][Play Raton"]
                String s = LoginActivity.generateLogMessage(TAG,"Play Raton");
                LoginActivity.logMessageToAnalytics(s, application);
                // [Ricardo] --------------- Logging activities ---------------------


                Intent intent = new Intent(v.getContext(), ArgentinaActivity.class);
                intent.putExtra("key", "raton");
                startActivity(intent);
            }
        });

        lluvia.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Log.d(TAG, "[100] Play Iluvia" );
                AnalyticsApplication application = (AnalyticsApplication) getApplication();

                // Log = ThisClass
                // Logging the overall usage of this screen. Does not log the user or current time.
                LoginActivity.logMessageToAnalytics(TAG + "- Play Iluvia", application);

                // Log = [Ricardo Edgard Caceffo][Thu Nov 17 14:36:22 GMT-02:00 2016][LoginActivity][Play Iluvia"]
                String s = LoginActivity.generateLogMessage(TAG,"Play Iluvia");
                LoginActivity.logMessageToAnalytics(s, application);

                Intent intent = new Intent(v.getContext(), ArgentinaActivity.class);
                intent.putExtra("key", "lluvia");
                startActivity(intent);
            }
        });

        carretera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Log.d(TAG, "[101] Play Carretera" );
                AnalyticsApplication application = (AnalyticsApplication) getApplication();

                // Log = ThisClass
                // Logging the overall usage of this screen. Does not log the user or current time.
                LoginActivity.logMessageToAnalytics(TAG + "- Play Carretera", application);

                // Log = [Ricardo Edgard Caceffo][Thu Nov 17 14:36:22 GMT-02:00 2016][LoginActivity][Play Carretera"]
                String s = LoginActivity.generateLogMessage(TAG,"Play Carretera");
                LoginActivity.logMessageToAnalytics(s, application);

                Intent intent = new Intent(v.getContext(), ArgentinaActivity.class);
                intent.putExtra("key", "carretera");
                startActivity(intent);
            }
        });

        hoy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Log.d(TAG, "[102] Play Hoy" );
                AnalyticsApplication application = (AnalyticsApplication) getApplication();

                // Log = ThisClass
                // Logging the overall usage of this screen. Does not log the user or current time.
                LoginActivity.logMessageToAnalytics(TAG + "- Play Hoy", application);

                // Log = [Ricardo Edgard Caceffo][Thu Nov 17 14:36:22 GMT-02:00 2016][LoginActivity][Play Hoy"]
                String s = LoginActivity.generateLogMessage(TAG,"Play Hoy");
                LoginActivity.logMessageToAnalytics(s, application);

                Intent intent = new Intent(v.getContext(), ArgentinaActivity.class);
                intent.putExtra("key", "hoy");
                startActivity(intent);
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu (Menu menu) {

        // [Ricardo] --------------- Logging activities ---------------------
        Log.d(TAG, "[103] onCreateOptionsMenu");
        AnalyticsApplication application = (AnalyticsApplication) getApplication();

        // Log = ThisClass
        // Logging the overall usage of this screen. Does not log the user or current time.
        LoginActivity.logMessageToAnalytics(TAG + "- onCreateOptionsMenu", application);

        // Log = [Ricardo Edgard Caceffo][Thu Nov 17 14:36:22 GMT-02:00 2016][LoginActivity][onCreateOptionsMenu]
        String s = LoginActivity.generateLogMessage(TAG,"onCreateOptionsMenu");
        LoginActivity.logMessageToAnalytics(s, application);
        // [Ricardo] --------------- Logging activities ---------------------



        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.drawer, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        // [Ricardo] --------------- Logging activities ---------------------
        Log.d(TAG, "[104] onOptionsItemSelected");
        AnalyticsApplication application = (AnalyticsApplication) getApplication();

        // Log = ThisClass
        // Logging the overall usage of this screen. Does not log the user or current time.
        LoginActivity.logMessageToAnalytics(TAG + "- onOptionsItemSelected  [item.getItemId() = " + item.getItemId() + "]", application);

        // Log = [Ricardo Edgard Caceffo][Thu Nov 17 14:36:22 GMT-02:00 2016][LoginActivity][onOptionsItemSelected]
        String s = LoginActivity.generateLogMessage(TAG,"onOptionsItemSelected [item.getItemId()= " + item.getItemId() + "]");
        LoginActivity.logMessageToAnalytics(s, application);
        // [Ricardo] --------------- Logging activities ---------------------


        switch (item.getItemId()) {
            case R.id.traduccion:
                final AlertDialog alertDialog = new AlertDialog.Builder(this).create();
                alertDialog.setTitle("Traducción");
                alertDialog.setMessage("Vamos conhecer as principais características do alfabeto." + "\n" +
                        "Toque nas palavras em espanhol que você conhece.\n");
                alertDialog.setCanceledOnTouchOutside(true);

                alertDialog.show();

                break;

            case R.id.drawernav_modules:
                Intent intent = new Intent(this, Menu3ModulosActivity.class);
                startActivity(intent);
                break;

            case R.id.drawernav_home:
                intent = new Intent(this, MenuModulosActivity.class);
                startActivity(intent);
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    /****************************** Google Analytics Cycle Control ********************************/

    @Override
    public void onStart() {
        super.onStart();
        Log.d(TAG, "[1] onStart");

        // [Ricardo] --------------- Logging activities ---------------------
        AnalyticsApplication application = (AnalyticsApplication) getApplication();

        // Log = ThisClass
        // Logging the overall usage of this screen. Does not log the user or current time.
        LoginActivity.logMessageToAnalytics(TAG + "- onStart", application);

        // Log = [Ricardo Edgard Caceffo][Thu Nov 17 14:36:22 GMT-02:00 2016][LoginActivity][onStart]
        String s = LoginActivity.generateLogMessage(TAG,"onStart");
        LoginActivity.logMessageToAnalytics(s, application);
        // [Ricardo] --------------- Logging activities ---------------------


    }


    @Override
    public void onResume() {
        super.onResume();
        Log.d(TAG, "[1] onResume");

        // [Ricardo] --------------- Logging activities ---------------------
        AnalyticsApplication application = (AnalyticsApplication) getApplication();

        // Log = ThisClass
        // Logging the overall usage of this screen. Does not log the user or current time.
        LoginActivity.logMessageToAnalytics(TAG + "- onResume", application);

        // Log = [Ricardo Edgard Caceffo][Thu Nov 17 14:36:22 GMT-02:00 2016][LoginActivity][onResume]
        String s = LoginActivity.generateLogMessage(TAG,"onResume");
        LoginActivity.logMessageToAnalytics(s, application);
        // [Ricardo] --------------- Logging activities ---------------------


    }


    @Override
    public void onStop() {
        super.onStop();
        Log.d(TAG, "[1] onStop");

        // [Ricardo] --------------- Logging activities ---------------------
        AnalyticsApplication application = (AnalyticsApplication) getApplication();

        // Log = ThisClass
        // Logging the overall usage of this screen. Does not log the user or current time.
        LoginActivity.logMessageToAnalytics(TAG + "- onStop", application);


        // Log = [Ricardo Edgard Caceffo][Thu Nov 17 14:36:22 GMT-02:00 2016][LoginActivity][onStop]
        String s = LoginActivity.generateLogMessage(TAG,"onStop");
        LoginActivity.logMessageToAnalytics(s, application);
        // [Ricardo] --------------- Logging activities ---------------------

    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "[1] onDestroy");

        // [Ricardo] --------------- Logging activities ---------------------
        AnalyticsApplication application = (AnalyticsApplication) getApplication();

        // Log = ThisClass
        // Logging the overall usage of this screen. Does not log the user or current time.
        LoginActivity.logMessageToAnalytics(TAG + "- onDestroy", application);

        // Log = [Ricardo Edgard Caceffo][Thu Nov 17 14:36:22 GMT-02:00 2016][LoginActivity][onDestroy]
        String s = LoginActivity.generateLogMessage(TAG,"onDestroy");
        LoginActivity.logMessageToAnalytics(s, application);
        // [Ricardo] --------------- Logging activities ---------------------


    }

    @Override
    public void onRestart() {
        super.onRestart();
        Log.d(TAG, "[1] onRestart");

        // [Ricardo] --------------- Logging activities ---------------------
        AnalyticsApplication application = (AnalyticsApplication) getApplication();

        // Log = ThisClass
        // Logging the overall usage of this screen. Does not log the user or current time.
        LoginActivity.logMessageToAnalytics(TAG + "- onRestart", application);

        // Log = [Ricardo Edgard Caceffo][Thu Nov 17 14:36:22 GMT-02:00 2016][LoginActivity][onRestart]
        String s = LoginActivity.generateLogMessage(TAG,"onRestart");
        LoginActivity.logMessageToAnalytics(s, application);
        // [Ricardo] --------------- Logging activities ---------------------


    }
    /****************************** Google Analytics Cycle Control ********************************/

}
