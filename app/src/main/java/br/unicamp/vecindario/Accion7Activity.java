package br.unicamp.vecindario;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.google.android.gms.analytics.Tracker;
import com.google.android.gms.appindexing.AppIndex;

/**
 * Created by renan on 03/09/2016.
 */
public class Accion7Activity extends AppCompatActivity {

    public Accion7Activity() {
        // Required empty public constructor
    }
    Toolbar toolbar;

    /**
     * [Ricardo] GoogleAnalytics Tracker
     * The {@link Tracker} used to record screen views.
     */
    private static final String TAG = "Accion7Activity";

    int numeroVerbos = 0;

    private static final int NONE = 0;
    private static final int SWIPE = 1;
    private int mode = NONE;
    private float startX;
    private float stopX;
    // We will only detect a swipe if the difference is at least 100 pixels
// Change this value to your needs
    private static final int TRESHOLD = 100;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // [Ricardo] --------------- Logging activities ---------------------
        Log.d(TAG, "[1] onCreate");
        AnalyticsApplication application = (AnalyticsApplication) getApplication();

        // Log = ThisClass
        // Logging the overall usage of this screen. Does not log the user or current time.
        LoginActivity.logMessageToAnalytics(TAG + "- onCreate", application);

        // Log = [Ricardo Edgard Caceffo][Thu Nov 17 14:36:22 GMT-02:00 2016][LoginActivity][onCreate]
        String s = LoginActivity.generateLogMessage(TAG,"onCreate");
        LoginActivity.logMessageToAnalytics(s, application);
        // [Ricardo] --------------- Logging activities ---------------------

        setContentView(R.layout.activity_accion7);

        final TextView totalVerbos = (TextView) findViewById(R.id.textView102);

        SeekBar seekBar = (SeekBar)findViewById(R.id.seekBar2);
        seekBar.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return true;
            }
        });

        final TextView verbo1= (TextView) findViewById(R.id.textView35);

        verbo1.setOnClickListener(new View.OnClickListener() {
            int selecionado = 0;

            @Override
            public void onClick(View v) {
                if (selecionado == 0) {
                    selecionado = 1;
                    numeroVerbos++;

                    verbo1.setTextColor(0xFFFD0505);

                    totalVerbos.setText("Verbos: " + numeroVerbos);
                }
            }
        });

        final TextView verbo2 = (TextView) findViewById(R.id.textView37);
        verbo2.setOnClickListener(new View.OnClickListener() {
            int selecionado = 0;
            @Override
            public void onClick(View v) {
                if (selecionado == 0) {
                    selecionado = 1;
                    numeroVerbos++;

                    verbo2.setTextColor(0xFFFD0505);

                    totalVerbos.setText("Verbos: " + numeroVerbos);
                }
            }
        });

        final TextView verbo3 = (TextView) findViewById(R.id.textView103);
        verbo3.setOnClickListener(new View.OnClickListener() {
            int selecionado = 0;
            @Override
            public void onClick(View v) {
                if (selecionado == 0) {
                    selecionado = 1;
                    numeroVerbos++;

                    verbo3.setTextColor(0xFFFD0505);

                    totalVerbos.setText("Verbos: " + numeroVerbos);
                }
            }
        });

        final TextView verbo4 = (TextView) findViewById(R.id.textView40);
        verbo4.setOnClickListener(new View.OnClickListener() {
            int selecionado = 0;
            @Override
            public void onClick(View v) {
                if (selecionado == 0) {
                    selecionado = 1;
                    numeroVerbos++;

                    verbo4.setTextColor(0xFFFD0505);

                    totalVerbos.setText("Verbos: " + numeroVerbos);
                }
            }
        });

        final TextView verbo5 = (TextView) findViewById(R.id.textView42);
        verbo5.setOnClickListener(new View.OnClickListener() {
            int selecionado = 0;
            @Override
            public void onClick(View v) {
                if (selecionado == 0) {
                    selecionado = 1;
                    numeroVerbos++;

                    verbo5.setTextColor(0xFFFD0505);

                    totalVerbos.setText("Verbos: " + numeroVerbos);
                }
            }
        });

        final TextView verbo6 = (TextView) findViewById(R.id.textView45);
        verbo6.setOnClickListener(new View.OnClickListener() {
            int selecionado = 0;
            @Override
            public void onClick(View v) {
                if (selecionado == 0) {
                    selecionado = 1;
                    numeroVerbos++;

                    verbo6.setTextColor(0xFFFD0505);

                    totalVerbos.setText("Verbos: " + numeroVerbos);
                }
            }
        });

        final TextView verbo7 = (TextView) findViewById(R.id.textView48);
        verbo7.setOnClickListener(new View.OnClickListener() {
            int selecionado = 0;
            @Override
            public void onClick(View v) {
                if (selecionado == 0) {
                    selecionado = 1;
                    numeroVerbos++;

                    verbo7.setTextColor(0xFFFD0505);

                    totalVerbos.setText("Verbos: " + numeroVerbos);
                }
            }
        });

        final TextView verbo8 = (TextView) findViewById(R.id.textView50);
        verbo8.setOnClickListener(new View.OnClickListener() {
            int selecionado = 0;
            @Override
            public void onClick(View v) {
                if (selecionado == 0) {
                    selecionado = 1;
                    numeroVerbos++;

                    verbo8.setTextColor(0xFFFD0505);

                    totalVerbos.setText("Verbos: " + numeroVerbos);
                }
            }
        });

        final TextView verbo9 = (TextView) findViewById(R.id.textView52);
        verbo9.setOnClickListener(new View.OnClickListener() {
            int selecionado = 0;
            @Override
            public void onClick(View v) {
                if (selecionado == 0) {
                    selecionado = 1;
                    numeroVerbos++;

                    verbo9.setTextColor(0xFFFD0505);

                    totalVerbos.setText("Verbos: " + numeroVerbos);
                }
            }
        });

        final TextView verbo10 = (TextView) findViewById(R.id.textView56);
        verbo10.setOnClickListener(new View.OnClickListener() {
            int selecionado = 0;
            @Override
            public void onClick(View v) {
                if (selecionado == 0) {
                    selecionado = 1;
                    numeroVerbos++;

                    verbo10.setTextColor(0xFFFD0505);

                    totalVerbos.setText("Verbos: " + numeroVerbos);
                }
            }
        });

        final TextView verbo11 = (TextView) findViewById(R.id.textView59);
        verbo11.setOnClickListener(new View.OnClickListener() {
            int selecionado = 0;
            @Override
            public void onClick(View v) {
                if (selecionado == 0) {
                    selecionado = 1;
                    numeroVerbos++;

                    verbo11.setTextColor(0xFFFD0505);

                    totalVerbos.setText("Verbos: " + numeroVerbos);
                }
            }
        });

        final TextView verbo12 = (TextView) findViewById(R.id.textView61);
        verbo12.setOnClickListener(new View.OnClickListener() {
            int selecionado = 0;
            @Override
            public void onClick(View v) {
                if (selecionado == 0) {
                    selecionado = 1;
                    numeroVerbos++;

                    verbo12.setTextColor(0xFFFD0505);

                    totalVerbos.setText("Verbos: " + numeroVerbos);
                }
            }
        });

        final TextView verbo13 = (TextView) findViewById(R.id.textView64);
        verbo13.setOnClickListener(new View.OnClickListener() {
            int selecionado = 0;
            @Override
            public void onClick(View v) {
                if (selecionado == 0) {
                    selecionado = 1;
                    numeroVerbos++;

                    verbo13.setTextColor(0xFFFD0505);

                    totalVerbos.setText("Verbos: " + numeroVerbos);
                }
            }
        });

        final TextView verbo14 = (TextView) findViewById(R.id.textView66);
        verbo14.setOnClickListener(new View.OnClickListener() {
            int selecionado = 0;
            @Override
            public void onClick(View v) {
                if (selecionado == 0) {
                    selecionado = 1;
                    numeroVerbos++;

                    verbo14.setTextColor(0xFFFD0505);

                    totalVerbos.setText("Verbos: " + numeroVerbos);
                }
            }
        });

        final TextView verbo15 = (TextView) findViewById(R.id.textView68);
        verbo15.setOnClickListener(new View.OnClickListener() {
            int selecionado = 0;
            @Override
            public void onClick(View v) {
                if (selecionado == 0) {
                    selecionado = 1;
                    numeroVerbos++;

                    verbo15.setTextColor(0xFFFD0505);

                    totalVerbos.setText("Verbos: " + numeroVerbos);
                }
            }
        });

        final TextView verbo16 = (TextView) findViewById(R.id.textView71);
        verbo16.setOnClickListener(new View.OnClickListener() {
            int selecionado = 0;
            @Override
            public void onClick(View v) {
                if (selecionado == 0) {
                    selecionado = 1;
                    numeroVerbos++;

                    verbo16.setTextColor(0xFFFD0505);

                    totalVerbos.setText("Verbos: " + numeroVerbos);
                }
            }
        });

        final TextView verbo17 = (TextView) findViewById(R.id.textView74);
        verbo17.setOnClickListener(new View.OnClickListener() {
            int selecionado = 0;
            @Override
            public void onClick(View v) {
                if (selecionado == 0) {
                    selecionado = 1;
                    numeroVerbos++;

                    verbo17.setTextColor(0xFFFD0505);

                    totalVerbos.setText("Verbos: " + numeroVerbos);
                }
            }
        });

        final TextView verbo18 = (TextView) findViewById(R.id.textView76);
        verbo18.setOnClickListener(new View.OnClickListener() {
            int selecionado = 0;
            @Override
            public void onClick(View v) {
                if (selecionado == 0) {
                    selecionado = 1;
                    numeroVerbos++;

                    verbo18.setTextColor(0xFFFD0505);

                    totalVerbos.setText("Verbos: " + numeroVerbos);
                }
            }
        });

        final TextView verbo19 = (TextView) findViewById(R.id.textView79);
        verbo19.setOnClickListener(new View.OnClickListener() {
            int selecionado = 0;
            @Override
            public void onClick(View v) {
                if (selecionado == 0) {
                    selecionado = 1;
                    numeroVerbos++;

                    verbo19.setTextColor(0xFFFD0505);

                    totalVerbos.setText("Verbos: " + numeroVerbos);
                }
            }
        });

        final TextView verbo20 = (TextView) findViewById(R.id.textView82);
        verbo20.setOnClickListener(new View.OnClickListener() {
            int selecionado = 0;
            @Override
            public void onClick(View v) {
                if (selecionado == 0) {
                    selecionado = 1;
                    numeroVerbos++;

                    verbo20.setTextColor(0xFFFD0505);

                    totalVerbos.setText("Verbos: " + numeroVerbos);
                }
            }
        });

        final TextView verbo21 = (TextView) findViewById(R.id.textView84);
        verbo21.setOnClickListener(new View.OnClickListener() {
            int selecionado = 0;
            @Override
            public void onClick(View v) {
                if (selecionado == 0) {
                    selecionado = 1;
                    numeroVerbos++;

                    verbo21.setTextColor(0xFFFD0505);

                    totalVerbos.setText("Verbos: " + numeroVerbos);
                }
            }
        });

        final TextView verbo22 = (TextView) findViewById(R.id.textView85);
        verbo22.setOnClickListener(new View.OnClickListener() {
            int selecionado = 0;
            @Override
            public void onClick(View v) {
                if (selecionado == 0) {
                    selecionado = 1;
                    numeroVerbos++;

                    verbo22.setTextColor(0xFFFD0505);

                    totalVerbos.setText("Verbos: " + numeroVerbos);
                }
            }
        });

        final TextView verbo23 = (TextView) findViewById(R.id.textView88);
        verbo23.setOnClickListener(new View.OnClickListener() {
            int selecionado = 0;
            @Override
            public void onClick(View v) {
                if (selecionado == 0) {
                    selecionado = 1;
                    numeroVerbos++;

                    verbo23.setTextColor(0xFFFD0505);

                    totalVerbos.setText("Verbos: " + numeroVerbos);
                }
            }
        });

        final TextView verbo24 = (TextView) findViewById(R.id.textView89);
        verbo24.setOnClickListener(new View.OnClickListener() {
            int selecionado = 0;
            @Override
            public void onClick(View v) {
                if (selecionado == 0) {
                    selecionado = 1;
                    numeroVerbos++;

                    verbo24.setTextColor(0xFFFD0505);

                    totalVerbos.setText("Verbos: " + numeroVerbos);
                }
            }
        });

        final TextView verbo25 = (TextView) findViewById(R.id.textView92);
        verbo25.setOnClickListener(new View.OnClickListener() {
            int selecionado = 0;
            @Override
            public void onClick(View v) {
                if (selecionado == 0) {
                    selecionado = 1;
                    numeroVerbos++;

                    verbo25.setTextColor(0xFFFD0505);

                    totalVerbos.setText("Verbos: " + numeroVerbos);
                }
            }
        });

        final TextView verbo26 = (TextView) findViewById(R.id.textView96);
        verbo26.setOnClickListener(new View.OnClickListener() {
            int selecionado = 0;
            @Override
            public void onClick(View v) {
                if (selecionado == 0) {
                    selecionado = 1;
                    numeroVerbos++;

                    verbo26.setTextColor(0xFFFD0505);

                    totalVerbos.setText("Verbos: " + numeroVerbos);
                }
            }
        });

        final TextView verbo27 = (TextView) findViewById(R.id.textView98);
        verbo27.setOnClickListener(new View.OnClickListener() {
            int selecionado = 0;
            @Override
            public void onClick(View v) {
                if (selecionado == 0) {
                    selecionado = 1;
                    numeroVerbos++;

                    verbo27.setTextColor(0xFFFD0505);

                    totalVerbos.setText("Verbos: " + numeroVerbos);
                }
            }
        });

        final TextView verbo28 = (TextView) findViewById(R.id.textView80);
        verbo28.setOnClickListener(new View.OnClickListener() {
            int selecionado = 0;
            @Override
            public void onClick(View v) {
                if (selecionado == 0) {
                    selecionado = 1;
                    numeroVerbos++;

                    verbo28.setTextColor(0xFFFD0505);

                    totalVerbos.setText("Verbos: " + numeroVerbos);
                }
            }
        });

        final TextView verbo29 = (TextView) findViewById(R.id.textView100);
        verbo29.setOnClickListener(new View.OnClickListener() {
            int selecionado = 0;
            @Override
            public void onClick(View v) {
                if (selecionado == 0) {
                    selecionado = 1;
                    numeroVerbos++;

                    verbo29.setTextColor(0xFFFD0505);

                    totalVerbos.setText("Verbos: " + numeroVerbos);
                }
            }
        });

        final TextView verbo30 = (TextView) findViewById(R.id.textView93);
        verbo30.setOnClickListener(new View.OnClickListener() {
            int selecionado = 0;
            @Override
            public void onClick(View v) {
                if (selecionado == 0) {
                    selecionado = 1;
                    numeroVerbos++;

                    verbo30.setTextColor(0xFFFD0505);

                    totalVerbos.setText("Verbos: " + numeroVerbos);
                }
            }
        });



        RelativeLayout relativeLayout = (RelativeLayout)findViewById(R.id.accion7);
        relativeLayout.setOnTouchListener (
                new RelativeLayout.OnTouchListener() {

                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        switch (event.getAction() & MotionEvent.ACTION_MASK) {
                            case MotionEvent.ACTION_DOWN:
                                // This happens when you touch the screen with two fingers
                                mode = SWIPE;
                                // You can also use event.getY(1) or the average of the two
                                startX = event.getX(0);
                                break;

                            case MotionEvent.ACTION_UP:
                                // This happens when you release the second finger
                                mode = NONE;
                                if ((numeroVerbos == 30) && (Math.abs(startX - stopX) > TRESHOLD)) {
                                    if (startX > stopX) {
                                        Intent intent = new Intent(v.getContext(), MenuModulosActivity.class);
                                        startActivity(intent);
                                    } else {
                                        Intent intent = new Intent(v.getContext(), Accion1Activity.class);
                                        startActivity(intent);
                                    }
                                }
                                mode = NONE;
                                break;

                            case MotionEvent.ACTION_MOVE:
                                if (mode == SWIPE) {
                                    stopX = event.getX(0);
                                }
                                break;
                        }

                        return true;
                    }
                });

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Acción");
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

    }

    @Override
    public boolean onCreateOptionsMenu (Menu menu) {

        // [Ricardo] --------------- Logging activities ---------------------
        Log.d(TAG, "[1] onCreateOptionsMenu");
        AnalyticsApplication application = (AnalyticsApplication) getApplication();

        // Log = ThisClass
        // Logging the overall usage of this screen. Does not log the user or current time.
        LoginActivity.logMessageToAnalytics(TAG + "- onCreateOptionsMenu", application);

        // Log = [Ricardo Edgard Caceffo][Thu Nov 17 14:36:22 GMT-02:00 2016][LoginActivity][onCreateOptionsMenu]
        String s = LoginActivity.generateLogMessage(TAG,"onCreateOptionsMenu");
        LoginActivity.logMessageToAnalytics(s, application);
        // [Ricardo] --------------- Logging activities ---------------------


        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.drawer, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        // [Ricardo] --------------- Logging activities ---------------------
        Log.d(TAG, "[1] onOptionsItemSelected");
        AnalyticsApplication application = (AnalyticsApplication) getApplication();

        // Log = ThisClass
        // Logging the overall usage of this screen. Does not log the user or current time.
        LoginActivity.logMessageToAnalytics(TAG + "- onOptionsItemSelected  [item.getItemId() = " + item.getItemId() + "]", application);

        // Log = [Ricardo Edgard Caceffo][Thu Nov 17 14:36:22 GMT-02:00 2016][LoginActivity][onOptionsItemSelected]
        String s = LoginActivity.generateLogMessage(TAG,"onOptionsItemSelected [item.getItemId()= " + item.getItemId() + "]");
        LoginActivity.logMessageToAnalytics(s, application);
        // [Ricardo] --------------- Logging activities ---------------------


        switch (item.getItemId()) {
            case R.id.traduccion:
                final AlertDialog alertDialog = new AlertDialog.Builder(this).create();
                alertDialog.setTitle("Traducción");
                alertDialog.setMessage("Jadisha: Foi muito difícil conseguir entender bem aos brasileiros.\n" +
                        "Manoel: Quando cheguei eu não sabia nada do idioma e não conhecia ninguém." +
                        " Por sorte, na viagem eu encontrei pessoas que são da minha cidade no Equador." +
                        " Conversei com eles e eles moram aqui em Campinas. Então vim com eles até Campinas e me" +
                        " trouxeram até uma pensão universitária perto do terminal. Então eu fiquei ali" +
                        " por um dia até encontrar onde morar.\n" +
                        "Marcelo: Bom, eu já conhecia antes o Brasil. Na segunda vez que cheguei aqui, foi… no dia em que" +
                        " cheguei, estava chovendo bastante quando sai com o meu tio, veio buscar a mim e ao meu pai," +
                        " veio nos buscar no aeroporto de Guarulhos, ficamos na cada dele e no dia seguinte pegamos" +
                        " um ônibus para vir conhecer Campinas e a Unicamp.");
                alertDialog.show();

                break;

            case R.id.drawernav_modules:
                Intent intent = new Intent(this, Menu3ModulosActivity.class);
                startActivity(intent);
                break;

            case R.id.drawernav_home:
                intent = new Intent(this, MenuModulosActivity.class);
                startActivity(intent);
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    /****************************** Google Analytics Cycle Control ********************************/

    /****************************** Google Analytics Cycle Control ********************************/

    @Override
    public void onStart() {
        super.onStart();
        Log.d(TAG, "[1] onStart");

        // [Ricardo] --------------- Logging activities ---------------------
        AnalyticsApplication application = (AnalyticsApplication) getApplication();

        // Log = ThisClass
        // Logging the overall usage of this screen. Does not log the user or current time.
        LoginActivity.logMessageToAnalytics(TAG + "- onStart", application);

        // Log = [Ricardo Edgard Caceffo][Thu Nov 17 14:36:22 GMT-02:00 2016][LoginActivity][onStart]
        String s = LoginActivity.generateLogMessage(TAG,"onStart");
        LoginActivity.logMessageToAnalytics(s, application);
        // [Ricardo] --------------- Logging activities ---------------------


    }


    @Override
    public void onResume() {
        super.onResume();
        Log.d(TAG, "[1] onResume");

        // [Ricardo] --------------- Logging activities ---------------------
        AnalyticsApplication application = (AnalyticsApplication) getApplication();

        // Log = ThisClass
        // Logging the overall usage of this screen. Does not log the user or current time.
        LoginActivity.logMessageToAnalytics(TAG + "- onResume", application);

        // Log = [Ricardo Edgard Caceffo][Thu Nov 17 14:36:22 GMT-02:00 2016][LoginActivity][onResume]
        String s = LoginActivity.generateLogMessage(TAG,"onResume");
        LoginActivity.logMessageToAnalytics(s, application);
        // [Ricardo] --------------- Logging activities ---------------------


    }


    @Override
    public void onStop() {
        super.onStop();
        Log.d(TAG, "[1] onStop");

        // [Ricardo] --------------- Logging activities ---------------------
        AnalyticsApplication application = (AnalyticsApplication) getApplication();

        // Log = ThisClass
        // Logging the overall usage of this screen. Does not log the user or current time.
        LoginActivity.logMessageToAnalytics(TAG + "- onStop", application);


        // Log = [Ricardo Edgard Caceffo][Thu Nov 17 14:36:22 GMT-02:00 2016][LoginActivity][onStop]
        String s = LoginActivity.generateLogMessage(TAG,"onStop");
        LoginActivity.logMessageToAnalytics(s, application);
        // [Ricardo] --------------- Logging activities ---------------------

    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "[1] onDestroy");

        // [Ricardo] --------------- Logging activities ---------------------
        AnalyticsApplication application = (AnalyticsApplication) getApplication();

        // Log = ThisClass
        // Logging the overall usage of this screen. Does not log the user or current time.
        LoginActivity.logMessageToAnalytics(TAG + "- onDestroy", application);

        // Log = [Ricardo Edgard Caceffo][Thu Nov 17 14:36:22 GMT-02:00 2016][LoginActivity][onDestroy]
        String s = LoginActivity.generateLogMessage(TAG,"onDestroy");
        LoginActivity.logMessageToAnalytics(s, application);
        // [Ricardo] --------------- Logging activities ---------------------


    }

    @Override
    public void onRestart() {
        super.onRestart();
        Log.d(TAG, "[1] onRestart");

        // [Ricardo] --------------- Logging activities ---------------------
        AnalyticsApplication application = (AnalyticsApplication) getApplication();

        // Log = ThisClass
        // Logging the overall usage of this screen. Does not log the user or current time.
        LoginActivity.logMessageToAnalytics(TAG + "- onRestart", application);

        // Log = [Ricardo Edgard Caceffo][Thu Nov 17 14:36:22 GMT-02:00 2016][LoginActivity][onRestart]
        String s = LoginActivity.generateLogMessage(TAG,"onRestart");
        LoginActivity.logMessageToAnalytics(s, application);
        // [Ricardo] --------------- Logging activities ---------------------


    }
    /****************************** Google Analytics Cycle Control ********************************/

}