/**
 * Created by Ricardo on 2016-11-09.
 *
 */
package br.unicamp.vecindario;

import android.app.Application;
import android.util.Log;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;



/**
 * This is a subclass of {@link Application} used to provide shared objects for this app, such as
 * the {@link Tracker}.
 */
public class AnalyticsApplication extends Application {

    private Tracker mTracker;
    private static final String TAG = "AnalyticsApplication";

    /**
     * Gets the default {@link Tracker} for this {@link Application}.
     * @return tracker
     */
    synchronized public Tracker getDefaultTracker() {
        if (mTracker == null) {
            Log.d(TAG,"1-getDefaultTracker");

            GoogleAnalytics analytics = GoogleAnalytics.getInstance(this);
            Log.d(TAG,"2-getDefaultTracker");

            // To enable debug logging use: adb shell setprop log.tag.GAv4 DEBUG
            mTracker = analytics.newTracker(R.xml.global_tracker);
            Log.d(TAG,"3-getDefaultTracker");

        }
        return mTracker;
    }

}
