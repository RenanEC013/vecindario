package br.unicamp.vecindario;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.google.android.gms.analytics.Tracker;

/**
 * Created by renan on 16/09/2016.
 */
public class UnaPalabra10Activity extends AppCompatActivity {
    public UnaPalabra10Activity() {

    }

    /* 0: não preenchida;  1: preenchida corretamente; -1: preenchida incorretamente */
    int lacuna1 = 0;
    int lacuna2 = 0;

    Toolbar toolbar;
    /**
     * [Ricardo] GoogleAnalytics Tracker
     * The {@link Tracker} used to record screen views.
     */
    private static final String TAG = "UnaPalabra10Activity";

    private static final int NONE = 0;
    private static final int SWIPE = 1;
    private int mode = NONE;
    private float startX;
    private float stopX;

    float Xstr1, Xstr2, Ystr1, Ystr2;

    // We will only detect a swipe if the difference is at least 100 pixels
// Change this value to your needs
    private static final int TRESHOLD = 100;

    private float x, y = 0.0f;
    private float deX, deY, delX, delY, alX, alY, aX, aY;
    boolean moving = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // [Ricardo] --------------- Logging activities ---------------------
        Log.d(TAG, "[1] onCreate");
        AnalyticsApplication application = (AnalyticsApplication) getApplication();

        // Log = ThisClass
        // Logging the overall usage of this screen. Does not log the user or current time.
        LoginActivity.logMessageToAnalytics(TAG + "- onCreate", application);

        // Log = [Ricardo Edgard Caceffo][Thu Nov 17 14:36:22 GMT-02:00 2016][LoginActivity][onCreate]
        String s = LoginActivity.generateLogMessage(TAG,"onCreate");
        LoginActivity.logMessageToAnalytics(s, application);
        // [Ricardo] --------------- Logging activities ---------------------

        setContentView(R.layout.activity_unapalabra10);

        final AlertDialog alertDialog2 = new AlertDialog.Builder(this).create();
        alertDialog2.setCanceledOnTouchOutside(true);
        alertDialog2.setTitle("Respuesta");

        SeekBar seekBar = (SeekBar)findViewById(R.id.seekBar2);
        seekBar.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return true;
            }
        });

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Una Palabra");
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        final TextView str1 = (TextView) findViewById(R.id.textView110);
        final TextView str2 = (TextView) findViewById(R.id.textView115);


        RelativeLayout relativeLayout = (RelativeLayout) findViewById(R.id.unapalabra10);
        relativeLayout.setOnTouchListener (
                new RelativeLayout.OnTouchListener() {

                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        switch (event.getAction() & MotionEvent.ACTION_MASK) {
                            case MotionEvent.ACTION_DOWN:

                                // [Ricardo] --------------- Logging activities ---------------------
                                String n5 = "ACTION Down";
                                AnalyticsApplication application5 = (AnalyticsApplication) getApplication();

                                // Log = ThisClass
                                // Logging the overall usage of this screen. Does not log the user or current time.
                                LoginActivity.logMessageToAnalytics(TAG + n5, application5);

                                // Log = [Ricardo Edgard Caceffo][Thu Nov 17 14:36:22 GMT-02:00 2016][LoginActivity][onStart]
                                String s5 = LoginActivity.generateLogMessage(TAG,n5);
                                LoginActivity.logMessageToAnalytics(s5, application5);
                                // [Ricardo] --------------- Logging activities ---------------------


                                // This happens when you touch the screen with two fingers
                                mode = SWIPE;
                                // You can also use event.getY(1) or the average of the two
                                startX = event.getX(0);
                                break;

                            case MotionEvent.ACTION_UP:

                                // [Ricardo] --------------- Logging activities ---------------------
                                String n2 = "ACTION UP";
                                AnalyticsApplication application2 = (AnalyticsApplication) getApplication();

                                // Log = ThisClass
                                // Logging the overall usage of this screen. Does not log the user or current time.
                                LoginActivity.logMessageToAnalytics(TAG + n2, application2);

                                // Log = [Ricardo Edgard Caceffo][Thu Nov 17 14:36:22 GMT-02:00 2016][LoginActivity][onStart]
                                String s2 = LoginActivity.generateLogMessage(TAG,n2);
                                LoginActivity.logMessageToAnalytics(s2, application2);
                                // [Ricardo] --------------- Logging activities ---------------------


                                // This happens when you release the second finger
                                mode = NONE;
                                if (Math.abs(startX - stopX) > TRESHOLD) {
                                    if (startX > stopX) {
                                        System.out.println(str1.getText());
                                        System.out.println(str2.getText());
                                        if ((lacuna1 == 1) && (lacuna2 == 1)) {
                                            Intent intent = new Intent(v.getContext(), UnaPalabra10bActivity.class);
                                            startActivity(intent);
                                        } else if ((lacuna1 == 0) || (lacuna2 == 0)) {
                                            alertDialog2.setMessage("Completar todos los huecos.");
                                            alertDialog2.setCanceledOnTouchOutside(true);
                                            alertDialog2.show();
                                        } else {
                                            Intent intent = new Intent(v.getContext(), UnaPalabra10aActivity.class);
                                            startActivity(intent);
                                            lacuna1 = 0;
                                            lacuna2 = 0;
                                            str1.setText("____");
                                            str2.setText("____");

                                        }
                                    } else {
                                        Intent intent = new Intent(v.getContext(), UnaPalabra9Activity.class);
                                        startActivity(intent);
                                    }
                                }
                                mode = NONE;
                                break;

                            case MotionEvent.ACTION_MOVE:

                                // [Ricardo] --------------- Logging activities ---------------------
                                String n3 = "ACTION MOVE";
                                AnalyticsApplication application3 = (AnalyticsApplication) getApplication();

                                // Log = ThisClass
                                // Logging the overall usage of this screen. Does not log the user or current time.
                                LoginActivity.logMessageToAnalytics(TAG + n3, application3);

                                // Log = [Ricardo Edgard Caceffo][Thu Nov 17 14:36:22 GMT-02:00 2016][LoginActivity][onStart]
                                String s3 = LoginActivity.generateLogMessage(TAG,n3);
                                LoginActivity.logMessageToAnalytics(s3, application3);
                                // [Ricardo] --------------- Logging activities ---------------------


                                if (mode == SWIPE) {
                                    stopX = event.getX(0);
                                }
                                break;
                        }

                        return true;
                    }
                });


        final Button de = (Button) findViewById(R.id.un);

        final int[] changedDe = {0};

        de.setOnTouchListener(
                new RelativeLayout.OnTouchListener() {

                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        Xstr1 = str1.getX();
                        Ystr1 = str1.getY();
                        Xstr2 = str2.getX();
                        Ystr2 = str2.getY();
                        switch (event.getAction() & MotionEvent.ACTION_MASK) {
                            case MotionEvent.ACTION_DOWN:
                                // This happens when you touch the screen with two fingers
                                mode = SWIPE;
                                moving = true;

                                break;

                            case MotionEvent.ACTION_UP:
                                // This happens when you release the second finger
                                mode = NONE;
                                moving = false;

                                if ((Math.abs(x - Xstr1) < 50) && (Math.abs(y - Ystr1) < 50)) {
                                    x = deX;
                                    y = deY;

                                    lacuna1 = -1;
                                    str1.setText("DE");

                                } else if ((Math.abs(x - Xstr2) < 50) && (Math.abs(y - Ystr2) < 50)) {
                                    x = deX;
                                    y = deY;

                                    lacuna2 = -1;
                                    str2.setText("DE");
                                }

                                de.setX(x);
                                de.setY(y);

                                break;

                            case MotionEvent.ACTION_MOVE:
                                if (mode == SWIPE) {
                                    stopX = event.getX(0);
                                }

                                if (moving) {
                                    if (changedDe[0] == 0) {
                                        changedDe[0] = 1;
                                        deX = de.getX();
                                        deY = de.getY();
                                    }
                                    x = event.getRawX() - de.getWidth() / 2;
                                    y = event.getRawY() - de.getHeight() / 2;

                                    de.setX(x);
                                    de.setY(y);

                                }

                                break;
                        }

                        return true;
                    }

                });


        final Button del = (Button) findViewById(R.id.una);

        final int[] changedDel = {0};

        del.setOnTouchListener(
                new RelativeLayout.OnTouchListener() {

                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        Xstr1 = str1.getX();
                        Ystr1 = str1.getY();
                        Xstr2 = str2.getX();
                        Ystr2 = str2.getY();
                        switch (event.getAction() & MotionEvent.ACTION_MASK) {
                            case MotionEvent.ACTION_DOWN:
                                // This happens when you touch the screen with two fingers
                                mode = SWIPE;
                                moving = true;

                                break;

                            case MotionEvent.ACTION_UP:
                                // This happens when you release the second finger
                                mode = NONE;
                                moving = false;

                                if ((Math.abs(x - Xstr1) < 50) && (Math.abs(y - Ystr1) < 50)) {
                                    x = delX;
                                    y = delY;

                                    lacuna1 = -1;
                                    str1.setText("DEL");

                                } else  if ((Math.abs(x - Xstr2) < 50) && (Math.abs(y - Ystr2) < 50))  {
                                    x = delX;
                                    y = delY;

                                    lacuna2 = 1;
                                    str2.setText("DEL");
                                }

                                del.setX(x);
                                del.setY(y);


                                break;

                            case MotionEvent.ACTION_MOVE:
                                if (mode == SWIPE) {
                                    stopX = event.getX(0);
                                }

                                if (moving) {
                                    if (changedDel[0] == 0) {
                                        changedDel[0] = 1;
                                        delX = del.getX();
                                        delY = del.getY();
                                    }
                                    x = event.getRawX() - del.getWidth() / 2;
                                    y = event.getRawY() - del.getHeight() / 2;

                                    del.setX(x);
                                    del.setY(y);


                                }

                                break;
                        }

                        return true;
                    }

                });

        final Button al = (Button) findViewById(R.id.unos);

        final int[] changedAl = {0};

        al.setOnTouchListener(
                new RelativeLayout.OnTouchListener() {

                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        Xstr1 = str1.getX();
                        Ystr1 = str1.getY();
                        Xstr2 = str2.getX();
                        Ystr2 = str2.getY();
                        switch (event.getAction() & MotionEvent.ACTION_MASK) {
                            case MotionEvent.ACTION_DOWN:
                                // This happens when you touch the screen with two fingers
                                mode = SWIPE;
                                moving = true;

                                break;

                            case MotionEvent.ACTION_UP:
                                // This happens when you release the second finger
                                mode = NONE;
                                moving = false;

                                if  ((Math.abs(x - Xstr1) < 50) && (Math.abs(y - Ystr1) < 50))  {
                                    x = alX;
                                    y = alY;

                                    lacuna1 = 1;
                                    str1.setText("AL");

                                } else  if ((Math.abs(x - Xstr2) < 50) && (Math.abs(y - Ystr2) < 50)) {
                                    x = alX;
                                    y = alY;

                                    lacuna2 = -1;
                                    str2.setText("AL");
                                }

                                al.setX(x);
                                al.setY(y);

                                break;

                            case MotionEvent.ACTION_MOVE:
                                if (mode == SWIPE) {
                                    stopX = event.getX(0);
                                }

                                if (moving) {
                                    if (changedAl[0] == 0) {
                                        changedAl[0] = 1;
                                        alX = al.getX();
                                        alY = al.getY();
                                    }
                                    x = event.getRawX() - al.getWidth() / 2;
                                    y = event.getRawY() - al.getHeight() / 2;

                                    al.setX(x);
                                    al.setY(y);

                                }

                                break;
                        }

                        return true;
                    }

                });

        final Button a = (Button) findViewById(R.id.unas);

        final int[] changedA = {0};

        a.setOnTouchListener(
                new RelativeLayout.OnTouchListener() {

                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        Xstr1 = str1.getX();
                        Ystr1 = str1.getY();
                        Xstr2 = str2.getX();
                        Ystr2 = str2.getY();
                        switch (event.getAction() & MotionEvent.ACTION_MASK) {
                            case MotionEvent.ACTION_DOWN:
                                // This happens when you touch the screen with two fingers
                                mode = SWIPE;
                                moving = true;
                                // You can also use event.getY(1) or the average of the two
                                startX = event.getX(0);
                                break;

                            case MotionEvent.ACTION_UP:
                                // This happens when you release the second finger
                                mode = NONE;
                                moving = false;

                                if ((Math.abs(x - Xstr1) < 50) && (Math.abs(y - Ystr1) < 50)) {
                                    x = aX;
                                    y = aY;

                                    lacuna1 = -1;
                                    str1.setText("A");

                                } else  if ((Math.abs(x - Xstr2) < 50) && (Math.abs(y - Ystr2) < 50)) {
                                    x = aX;
                                    y = aY;

                                    lacuna2 = -1;
                                    str2.setText("A");
                                }

                                a.setX(x);
                                a.setY(y);

                                break;

                            case MotionEvent.ACTION_MOVE:
                                if (mode == SWIPE) {
                                    stopX = event.getX(0);
                                }

                                if (moving) {
                                    if (changedA[0] == 0) {
                                        changedA[0] = 1;
                                        aX = a.getX();
                                        aY = a.getY();
                                    }
                                    x = event.getRawX() - a.getWidth() / 2;
                                    y = event.getRawY() - a.getHeight() / 2;

                                    a.setX(x);
                                    a.setY(y);

                                }

                                break;
                        }

                        return true;
                    }

                });

    }


    @Override
    public boolean onCreateOptionsMenu (Menu menu) {

        // [Ricardo] --------------- Logging activities ---------------------
        Log.d(TAG, "[1] onCreateOptionsMenu");
        AnalyticsApplication application = (AnalyticsApplication) getApplication();

        // Log = ThisClass
        // Logging the overall usage of this screen. Does not log the user or current time.
        LoginActivity.logMessageToAnalytics(TAG + "- onCreateOptionsMenu", application);

        // Log = [Ricardo Edgard Caceffo][Thu Nov 17 14:36:22 GMT-02:00 2016][LoginActivity][onCreateOptionsMenu]
        String s = LoginActivity.generateLogMessage(TAG,"onCreateOptionsMenu");
        LoginActivity.logMessageToAnalytics(s, application);
        // [Ricardo] --------------- Logging activities ---------------------



        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.drawer, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        // [Ricardo] --------------- Logging activities ---------------------
        Log.d(TAG, "[1] onOptionsItemSelected");
        AnalyticsApplication application = (AnalyticsApplication) getApplication();

        // Log = ThisClass
        // Logging the overall usage of this screen. Does not log the user or current time.
        LoginActivity.logMessageToAnalytics(TAG + "- onOptionsItemSelected  [item.getItemId() = " + item.getItemId() + "]", application);

        // Log = [Ricardo Edgard Caceffo][Thu Nov 17 14:36:22 GMT-02:00 2016][LoginActivity][onOptionsItemSelected]
        String s = LoginActivity.generateLogMessage(TAG,"onOptionsItemSelected [item.getItemId()= " + item.getItemId() + "]");
        LoginActivity.logMessageToAnalytics(s, application);
        // [Ricardo] --------------- Logging activities ---------------------


        switch (item.getItemId()) {
            case R.id.traduccion:
                final AlertDialog alertDialog = new AlertDialog.Builder(this).create();
                alertDialog.setTitle("Traducción");
                alertDialog.setMessage("José: Desculpe, senhor, posso passar? Quero ir ao banheiro.\n" +
                        "Senhor: Sim, passe.\n\n" +
                        "(5 minutos depois)\n\n" +
                        "José: Desculpe o incômodo, eu gostaria de passar outra vez, venho do banheiro.");
                alertDialog.setCanceledOnTouchOutside(true);

                alertDialog.show();

                break;

            case R.id.drawernav_modules:
                Intent intent = new Intent(this, Menu3ModulosActivity.class);
                startActivity(intent);
                break;

            case R.id.drawernav_home:
                intent = new Intent(this, MenuModulosActivity.class);
                startActivity(intent);
                break;
        }

        return super.onOptionsItemSelected(item);
    }
    /****************************** Google Analytics Cycle Control ********************************/

    @Override
    public void onStart() {
        super.onStart();
        Log.d(TAG, "[1] onStart");

        // [Ricardo] --------------- Logging activities ---------------------
        AnalyticsApplication application = (AnalyticsApplication) getApplication();

        // Log = ThisClass
        // Logging the overall usage of this screen. Does not log the user or current time.
        LoginActivity.logMessageToAnalytics(TAG + "- onStart", application);

        // Log = [Ricardo Edgard Caceffo][Thu Nov 17 14:36:22 GMT-02:00 2016][LoginActivity][onStart]
        String s = LoginActivity.generateLogMessage(TAG,"onStart");
        LoginActivity.logMessageToAnalytics(s, application);
        // [Ricardo] --------------- Logging activities ---------------------


    }


    @Override
    public void onResume() {
        super.onResume();
        Log.d(TAG, "[1] onResume");

        // [Ricardo] --------------- Logging activities ---------------------
        AnalyticsApplication application = (AnalyticsApplication) getApplication();

        // Log = ThisClass
        // Logging the overall usage of this screen. Does not log the user or current time.
        LoginActivity.logMessageToAnalytics(TAG + "- onResume", application);

        // Log = [Ricardo Edgard Caceffo][Thu Nov 17 14:36:22 GMT-02:00 2016][LoginActivity][onResume]
        String s = LoginActivity.generateLogMessage(TAG,"onResume");
        LoginActivity.logMessageToAnalytics(s, application);
        // [Ricardo] --------------- Logging activities ---------------------


    }


    @Override
    public void onStop() {
        super.onStop();
        Log.d(TAG, "[1] onStop");

        // [Ricardo] --------------- Logging activities ---------------------
        AnalyticsApplication application = (AnalyticsApplication) getApplication();

        // Log = ThisClass
        // Logging the overall usage of this screen. Does not log the user or current time.
        LoginActivity.logMessageToAnalytics(TAG + "- onStop", application);


        // Log = [Ricardo Edgard Caceffo][Thu Nov 17 14:36:22 GMT-02:00 2016][LoginActivity][onStop]
        String s = LoginActivity.generateLogMessage(TAG,"onStop");
        LoginActivity.logMessageToAnalytics(s, application);
        // [Ricardo] --------------- Logging activities ---------------------

    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "[1] onDestroy");

        // [Ricardo] --------------- Logging activities ---------------------
        AnalyticsApplication application = (AnalyticsApplication) getApplication();

        // Log = ThisClass
        // Logging the overall usage of this screen. Does not log the user or current time.
        LoginActivity.logMessageToAnalytics(TAG + "- onDestroy", application);

        // Log = [Ricardo Edgard Caceffo][Thu Nov 17 14:36:22 GMT-02:00 2016][LoginActivity][onDestroy]
        String s = LoginActivity.generateLogMessage(TAG,"onDestroy");
        LoginActivity.logMessageToAnalytics(s, application);
        // [Ricardo] --------------- Logging activities ---------------------


    }

    @Override
    public void onRestart() {
        super.onRestart();
        Log.d(TAG, "[1] onRestart");

        // [Ricardo] --------------- Logging activities ---------------------
        AnalyticsApplication application = (AnalyticsApplication) getApplication();

        // Log = ThisClass
        // Logging the overall usage of this screen. Does not log the user or current time.
        LoginActivity.logMessageToAnalytics(TAG + "- onRestart", application);

        // Log = [Ricardo Edgard Caceffo][Thu Nov 17 14:36:22 GMT-02:00 2016][LoginActivity][onRestart]
        String s = LoginActivity.generateLogMessage(TAG,"onRestart");
        LoginActivity.logMessageToAnalytics(s, application);
        // [Ricardo] --------------- Logging activities ---------------------


    }
    /****************************** Google Analytics Cycle Control ********************************/
}
