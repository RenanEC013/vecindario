package br.unicamp.vecindario;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.google.android.gms.analytics.Tracker;

/**
 * Created by renan on 18/09/2016.
 */
public class UnaPalabra12Activity extends AppCompatActivity {
    public UnaPalabra12Activity() {

    }

    /* 0: não preenchida;  1: preenchida corretamente; -1: preenchida incorretamente */
    int lacuna1 = 0;
    int lacuna2 = 0;
    int lacuna3 = 0;

    Toolbar toolbar;
    /**
     * [Ricardo] GoogleAnalytics Tracker
     * The {@link Tracker} used to record screen views.
     */
    private static final String TAG = "UnaPalabra12Activity";

    private static final int NONE = 0;
    private static final int SWIPE = 1;
    private int mode = NONE;
    private float startX;
    private float stopX;

    float Xstr1, Xstr2, Xstr3, Ystr1, Ystr2, Ystr3;

    // We will only detect a swipe if the difference is at least 100 pixels
// Change this value to your needs
    private static final int TRESHOLD = 100;

    private float x, y = 0.0f;
    private float unX, unY, unaX, unaY, unosX, unosY, unasX, unasY;
    boolean moving = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // [Ricardo] --------------- Logging activities ---------------------
        Log.d(TAG, "[1] onCreate");
        AnalyticsApplication application = (AnalyticsApplication) getApplication();

        // Log = ThisClass
        // Logging the overall usage of this screen. Does not log the user or current time.
        LoginActivity.logMessageToAnalytics(TAG + "- onCreate", application);

        // Log = [Ricardo Edgard Caceffo][Thu Nov 17 14:36:22 GMT-02:00 2016][LoginActivity][onCreate]
        String s = LoginActivity.generateLogMessage(TAG,"onCreate");
        LoginActivity.logMessageToAnalytics(s, application);
        // [Ricardo] --------------- Logging activities ---------------------

        setContentView(R.layout.activity_unapalabra12);

        final AlertDialog alertDialog2 = new AlertDialog.Builder(this).create();
        alertDialog2.setCanceledOnTouchOutside(true);
        alertDialog2.setTitle("Respuesta");

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Una Palabra");
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        final TextView str1 = (TextView) findViewById(R.id.textView119);
        final TextView str2 = (TextView) findViewById(R.id.textView117);
        final TextView str3 = (TextView) findViewById(R.id.textView120);

        SeekBar seekBar = (SeekBar)findViewById(R.id.seekBar2);
        seekBar.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return true;
            }
        });

        RelativeLayout relativeLayout = (RelativeLayout) findViewById(R.id.unapalabra12);
        relativeLayout.setOnTouchListener (
                new RelativeLayout.OnTouchListener() {

                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        switch (event.getAction() & MotionEvent.ACTION_MASK) {
                            case MotionEvent.ACTION_DOWN:

                                // [Ricardo] --------------- Logging activities ---------------------
                                String n5 = "ACTION Down";
                                AnalyticsApplication application5 = (AnalyticsApplication) getApplication();

                                // Log = ThisClass
                                // Logging the overall usage of this screen. Does not log the user or current time.
                                LoginActivity.logMessageToAnalytics(TAG + n5, application5);

                                // Log = [Ricardo Edgard Caceffo][Thu Nov 17 14:36:22 GMT-02:00 2016][LoginActivity][onStart]
                                String s5 = LoginActivity.generateLogMessage(TAG,n5);
                                LoginActivity.logMessageToAnalytics(s5, application5);
                                // [Ricardo] --------------- Logging activities ---------------------


                                // This happens when you touch the screen with two fingers
                                mode = SWIPE;
                                // You can also use event.getY(1) or the average of the two
                                startX = event.getX(0);
                                break;

                            case MotionEvent.ACTION_UP:

                                // [Ricardo] --------------- Logging activities ---------------------
                                String n2 = "ACTION UP";
                                AnalyticsApplication application2 = (AnalyticsApplication) getApplication();

                                // Log = ThisClass
                                // Logging the overall usage of this screen. Does not log the user or current time.
                                LoginActivity.logMessageToAnalytics(TAG + n2, application2);

                                // Log = [Ricardo Edgard Caceffo][Thu Nov 17 14:36:22 GMT-02:00 2016][LoginActivity][onStart]
                                String s2 = LoginActivity.generateLogMessage(TAG,n2);
                                LoginActivity.logMessageToAnalytics(s2, application2);
                                // [Ricardo] --------------- Logging activities ---------------------


                                // This happens when you release the second finger
                                mode = NONE;
                                if (Math.abs(startX - stopX) > TRESHOLD) {
                                    if (startX > stopX) {
                                        if ((lacuna1 == 1) && (lacuna2 == 1) && (lacuna3 == 1)) {
                                            Intent intent = new Intent(v.getContext(), UnaPalabra12bActivity.class);
                                            startActivity(intent);
                                        } else if ((lacuna1 == 0) || (lacuna2 == 0) || (lacuna3 == 0)) {
                                            alertDialog2.setMessage("Completar todos los huecos.");
                                            alertDialog2.setCanceledOnTouchOutside(true);
                                            alertDialog2.show();
                                        } else {
                                            Intent intent = new Intent(v.getContext(), UnaPalabra12aActivity.class);
                                            startActivity(intent);
                                            lacuna1 = 0;
                                            lacuna2 = 0;
                                            lacuna3 = 0;
                                            str1.setText("____");
                                            str2.setText("____");
                                            str3.setText("____");

                                        }
                                    } else {
                                        Intent intent = new Intent(v.getContext(), UnaPalabra11Activity.class);
                                        startActivity(intent);
                                    }
                                }
                                mode = NONE;
                                break;

                            case MotionEvent.ACTION_MOVE:

                                // [Ricardo] --------------- Logging activities ---------------------
                                String n3 = "ACTION MOVE";
                                AnalyticsApplication application3 = (AnalyticsApplication) getApplication();

                                // Log = ThisClass
                                // Logging the overall usage of this screen. Does not log the user or current time.
                                LoginActivity.logMessageToAnalytics(TAG + n3, application3);

                                // Log = [Ricardo Edgard Caceffo][Thu Nov 17 14:36:22 GMT-02:00 2016][LoginActivity][onStart]
                                String s3 = LoginActivity.generateLogMessage(TAG,n3);
                                LoginActivity.logMessageToAnalytics(s3, application3);
                                // [Ricardo] --------------- Logging activities ---------------------


                                if (mode == SWIPE) {
                                    stopX = event.getX(0);
                                }
                                break;
                        }

                        return true;
                    }
                });


        final Button un = (Button) findViewById(R.id.un);

        final int[] changedUn = {0};

        un.setOnTouchListener(
                new RelativeLayout.OnTouchListener() {

                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        Xstr1 = str1.getX();
                        Ystr1 = str1.getY();
                        Xstr2 = str2.getX();
                        Ystr2 = str2.getY();
                        Xstr3 = str3.getX();
                        Ystr3 = str3.getY();
                        switch (event.getAction() & MotionEvent.ACTION_MASK) {
                            case MotionEvent.ACTION_DOWN:
                                // This happens when you touch the screen with two fingers
                                mode = SWIPE;
                                moving = true;

                                break;

                            case MotionEvent.ACTION_UP:
                                // This happens when you release the second finger
                                mode = NONE;
                                moving = false;

                                if ((Math.abs(x - Xstr1) < 50) && (Math.abs(y - Ystr1) < 50)) {
                                    x = unX;
                                    y = unY;

                                    lacuna1 = -1;
                                    str1.setText("UN");

                                } else if ((Math.abs(x - Xstr2) < 50) && (Math.abs(y - Ystr2) < 50)) {
                                    x = unX;
                                    y = unY;

                                    lacuna2 = -1;
                                    str2.setText("UN");

                                } else if ((Math.abs(x - Xstr3) < 60) && (Math.abs(y - Ystr3) < 60)) {
                                    x = unX;
                                    y = unY;

                                    lacuna3 = -1;
                                    str3.setText("UN");
                                }

                                un.setX(x);
                                un.setY(y);


                                break;

                            case MotionEvent.ACTION_MOVE:
                                if (mode == SWIPE) {
                                    stopX = event.getX(0);
                                }

                                if (moving) {
                                    if (changedUn[0] == 0) {
                                        changedUn[0] = 1;
                                        unX = un.getX();
                                        unY = un.getY();
                                    }

                                    x = event.getRawX() - un.getWidth() / 2;
                                    y = event.getRawY() - un.getHeight() / 2;

                                    un.setX(x);
                                    un.setY(y);

                                }

                                break;
                        }

                        return true;
                    }

                });


        final Button una = (Button) findViewById(R.id.una);

        final int[] changedUna = {0};

        una.setOnTouchListener(
                new RelativeLayout.OnTouchListener() {

                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        Xstr1 = str1.getX();
                        Ystr1 = str1.getY();
                        Xstr2 = str2.getX();
                        Ystr2 = str2.getY();
                        Xstr3 = str3.getX();
                        Ystr3 = str3.getY();
                        switch (event.getAction() & MotionEvent.ACTION_MASK) {
                            case MotionEvent.ACTION_DOWN:
                                // This happens when you touch the screen with two fingers
                                mode = SWIPE;
                                moving = true;

                                break;

                            case MotionEvent.ACTION_UP:
                                // This happens when you release the second finger
                                mode = NONE;
                                moving = false;

                                if ((Math.abs(x - Xstr1) < 50) && (Math.abs(y - Ystr1) < 50)) {
                                    x = unaX;
                                    y = unaY;

                                    lacuna1 = 1;
                                    str1.setText("UNA");


                                } else  if ((Math.abs(x - Xstr2) < 50) && (Math.abs(y - Ystr2) < 50))  {
                                    x = unaX;
                                    y = unaY;

                                    lacuna2 = -1;
                                    str2.setText("UNA");

                                } else if ((Math.abs(x - Xstr3) < 60) && (Math.abs(y - Ystr3) < 60)) {
                                    x = unaX;
                                    y = unaY;

                                    lacuna3 = -1;
                                    str3.setText("UNA");
                                }

                                una.setX(x);
                                una.setY(y);

                                break;

                            case MotionEvent.ACTION_MOVE:
                                if (mode == SWIPE) {
                                    stopX = event.getX(0);
                                }

                                if (moving) {
                                    if (changedUna[0] == 0) {
                                        changedUna[0] = 1;
                                        unaX = una.getX();
                                        unaY = una.getY();
                                    }

                                    x = event.getRawX() - una.getWidth() / 2;
                                    y = event.getRawY() - una.getHeight() / 2;

                                    una.setX(x);
                                    una.setY(y);


                                }

                                break;
                        }

                        return true;
                    }

                });

        final Button unos = (Button) findViewById(R.id.unos);

        final int[] changedUnos = {0};

        unos.setOnTouchListener(
                new RelativeLayout.OnTouchListener() {

                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        Xstr1 = str1.getX();
                        Ystr1 = str1.getY();
                        Xstr2 = str2.getX();
                        Ystr2 = str2.getY();
                        Xstr3 = str3.getX();
                        Ystr3 = str3.getY();
                        switch (event.getAction() & MotionEvent.ACTION_MASK) {
                            case MotionEvent.ACTION_DOWN:
                                // This happens when you touch the screen with two fingers
                                mode = SWIPE;
                                moving = true;

                                break;

                            case MotionEvent.ACTION_UP:
                                // This happens when you release the second finger
                                mode = NONE;
                                moving = false;

                                if  ((Math.abs(x - Xstr1) < 50) && (Math.abs(y - Ystr1) < 50))  {
                                    x = unosX;
                                    y = unosY;

                                    lacuna1 = -1;
                                    str1.setText("UNOS");

                                } else  if ((Math.abs(x - Xstr2) < 50) && (Math.abs(y - Ystr2) < 50)) {
                                    x = unosX;
                                    y = unosY;

                                    lacuna2 = 1;
                                    str2.setText("UNOS");

                                } else if ((Math.abs(x - Xstr3) < 60) && (Math.abs(y - Ystr3) < 60)) {
                                    x = unosX;
                                    y = unosY;

                                    lacuna3 = -1;
                                    str3.setText("UNOS");
                                }

                                unos.setX(x);
                                unos.setY(y);

                                break;

                            case MotionEvent.ACTION_MOVE:
                                if (mode == SWIPE) {
                                    stopX = event.getX(0);
                                }

                                if (moving) {
                                    if (changedUnos[0] == 0) {
                                        changedUnos[0] = 1;
                                        unosX = unos.getX();
                                        unosY = unos.getY();
                                    }

                                    x = event.getRawX() - unos.getWidth() / 2;
                                    y = event.getRawY() - unos.getHeight() / 2;

                                    unos.setX(x);
                                    unos.setY(y);

                                }

                                break;
                        }

                        return true;
                    }

                });

        final Button unas = (Button) findViewById(R.id.unas);

        final int[] changedUnas = {0};

        unas.setOnTouchListener(
                new RelativeLayout.OnTouchListener() {

                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        Xstr1 = str1.getX();
                        Ystr1 = str1.getY();
                        Xstr2 = str2.getX();
                        Ystr2 = str2.getY();
                        Xstr3 = str3.getX();
                        Ystr3 = str3.getY();
                        switch (event.getAction() & MotionEvent.ACTION_MASK) {
                            case MotionEvent.ACTION_DOWN:
                                // This happens when you touch the screen with two fingers
                                mode = SWIPE;
                                moving = true;
                                // You can also use event.getY(1) or the average of the two
                                startX = event.getX(0);
                                break;

                            case MotionEvent.ACTION_UP:
                                // This happens when you release the second finger
                                mode = NONE;
                                moving = false;

                                if ((Math.abs(x - Xstr1) < 50) && (Math.abs(y - Ystr1) < 50)) {
                                    x = unasX;
                                    y = unasY;

                                    lacuna1 = -1;
                                    str1.setText("UNAS");


                                } else  if ((Math.abs(x - Xstr2) < 50) && (Math.abs(y - Ystr2) < 50)) {
                                    x = unasX;
                                    y = unasY;

                                    lacuna2 = -1;
                                    str2.setText("UNAS");

                                } else if ((Math.abs(x - Xstr3) < 60) && (Math.abs(y - Ystr3) < 60)) {
                                    x = unasX;
                                    y = unasY;

                                    lacuna3 = 1;
                                    str3.setText("UNAS");
                                }

                                unas.setX(x);
                                unas.setY(y);

                                break;


                            case MotionEvent.ACTION_MOVE:
                                if (mode == SWIPE) {
                                    stopX = event.getX(0);
                                }

                                if (moving) {
                                    if (changedUnas[0] == 0) {
                                        changedUnas[0] = 1;
                                        unasX = unas.getX();
                                        unasY = unas.getY();
                                    }

                                    x = event.getRawX() - unas.getWidth() / 2;
                                    y = event.getRawY() - unas.getHeight() / 2;

                                    unas.setX(x);
                                    unas.setY(y);

                                }

                                break;
                        }

                        return true;
                    }

                });

    }


    @Override
    public boolean onCreateOptionsMenu (Menu menu) {

        // [Ricardo] --------------- Logging activities ---------------------
        Log.d(TAG, "[1] onCreateOptionsMenu");
        AnalyticsApplication application = (AnalyticsApplication) getApplication();

        // Log = ThisClass
        // Logging the overall usage of this screen. Does not log the user or current time.
        LoginActivity.logMessageToAnalytics(TAG + "- onCreateOptionsMenu", application);

        // Log = [Ricardo Edgard Caceffo][Thu Nov 17 14:36:22 GMT-02:00 2016][LoginActivity][onCreateOptionsMenu]
        String s = LoginActivity.generateLogMessage(TAG,"onCreateOptionsMenu");
        LoginActivity.logMessageToAnalytics(s, application);
        // [Ricardo] --------------- Logging activities ---------------------



        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.drawer, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        // [Ricardo] --------------- Logging activities ---------------------
        Log.d(TAG, "[1] onOptionsItemSelected");
        AnalyticsApplication application = (AnalyticsApplication) getApplication();

        // Log = ThisClass
        // Logging the overall usage of this screen. Does not log the user or current time.
        LoginActivity.logMessageToAnalytics(TAG + "- onOptionsItemSelected  [item.getItemId() = " + item.getItemId() + "]", application);

        // Log = [Ricardo Edgard Caceffo][Thu Nov 17 14:36:22 GMT-02:00 2016][LoginActivity][onOptionsItemSelected]
        String s = LoginActivity.generateLogMessage(TAG,"onOptionsItemSelected [item.getItemId()= " + item.getItemId() + "]");
        LoginActivity.logMessageToAnalytics(s, application);
        // [Ricardo] --------------- Logging activities ---------------------

        switch (item.getItemId()) {
            case R.id.traduccion:

                final AlertDialog alertDialog = new AlertDialog.Builder(this).create();
                alertDialog.setTitle("Traducción");
                alertDialog.setMessage("José: Tenho uma amiga e conheço uns três estudantes colombianos.\n" +
                        "Senhor: Eu estive na Colômbia umas quatro ou cinco vezes, é um país encantador.\n" +
                        "José: Espero que eu goste.");
                alertDialog.setCanceledOnTouchOutside(true);

                alertDialog.show();

                break;

            case R.id.drawernav_modules:
                Intent intent = new Intent(this, Menu3ModulosActivity.class);
                startActivity(intent);
                break;

            case R.id.drawernav_home:
                intent = new Intent(this, MenuModulosActivity.class);
                startActivity(intent);
                break;
        }

        return super.onOptionsItemSelected(item);
    }
    /****************************** Google Analytics Cycle Control ********************************/

    @Override
    public void onStart() {
        super.onStart();
        Log.d(TAG, "[1] onStart");

        // [Ricardo] --------------- Logging activities ---------------------
        AnalyticsApplication application = (AnalyticsApplication) getApplication();

        // Log = ThisClass
        // Logging the overall usage of this screen. Does not log the user or current time.
        LoginActivity.logMessageToAnalytics(TAG + "- onStart", application);

        // Log = [Ricardo Edgard Caceffo][Thu Nov 17 14:36:22 GMT-02:00 2016][LoginActivity][onStart]
        String s = LoginActivity.generateLogMessage(TAG,"onStart");
        LoginActivity.logMessageToAnalytics(s, application);
        // [Ricardo] --------------- Logging activities ---------------------


    }


    @Override
    public void onResume() {
        super.onResume();
        Log.d(TAG, "[1] onResume");

        // [Ricardo] --------------- Logging activities ---------------------
        AnalyticsApplication application = (AnalyticsApplication) getApplication();

        // Log = ThisClass
        // Logging the overall usage of this screen. Does not log the user or current time.
        LoginActivity.logMessageToAnalytics(TAG + "- onResume", application);

        // Log = [Ricardo Edgard Caceffo][Thu Nov 17 14:36:22 GMT-02:00 2016][LoginActivity][onResume]
        String s = LoginActivity.generateLogMessage(TAG,"onResume");
        LoginActivity.logMessageToAnalytics(s, application);
        // [Ricardo] --------------- Logging activities ---------------------


    }


    @Override
    public void onStop() {
        super.onStop();
        Log.d(TAG, "[1] onStop");

        // [Ricardo] --------------- Logging activities ---------------------
        AnalyticsApplication application = (AnalyticsApplication) getApplication();

        // Log = ThisClass
        // Logging the overall usage of this screen. Does not log the user or current time.
        LoginActivity.logMessageToAnalytics(TAG + "- onStop", application);


        // Log = [Ricardo Edgard Caceffo][Thu Nov 17 14:36:22 GMT-02:00 2016][LoginActivity][onStop]
        String s = LoginActivity.generateLogMessage(TAG,"onStop");
        LoginActivity.logMessageToAnalytics(s, application);
        // [Ricardo] --------------- Logging activities ---------------------

    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "[1] onDestroy");

        // [Ricardo] --------------- Logging activities ---------------------
        AnalyticsApplication application = (AnalyticsApplication) getApplication();

        // Log = ThisClass
        // Logging the overall usage of this screen. Does not log the user or current time.
        LoginActivity.logMessageToAnalytics(TAG + "- onDestroy", application);

        // Log = [Ricardo Edgard Caceffo][Thu Nov 17 14:36:22 GMT-02:00 2016][LoginActivity][onDestroy]
        String s = LoginActivity.generateLogMessage(TAG,"onDestroy");
        LoginActivity.logMessageToAnalytics(s, application);
        // [Ricardo] --------------- Logging activities ---------------------


    }

    @Override
    public void onRestart() {
        super.onRestart();
        Log.d(TAG, "[1] onRestart");

        // [Ricardo] --------------- Logging activities ---------------------
        AnalyticsApplication application = (AnalyticsApplication) getApplication();

        // Log = ThisClass
        // Logging the overall usage of this screen. Does not log the user or current time.
        LoginActivity.logMessageToAnalytics(TAG + "- onRestart", application);

        // Log = [Ricardo Edgard Caceffo][Thu Nov 17 14:36:22 GMT-02:00 2016][LoginActivity][onRestart]
        String s = LoginActivity.generateLogMessage(TAG,"onRestart");
        LoginActivity.logMessageToAnalytics(s, application);
        // [Ricardo] --------------- Logging activities ---------------------


    }
    /****************************** Google Analytics Cycle Control ********************************/
}
