package br.unicamp.vecindario;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.appindexing.Thing;
import com.google.android.gms.common.api.GoogleApiClient;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.Date;

import br.unicamp.vecindario.model.User;

public class LoginActivity extends AppCompatActivity {


    private LoginButton btnLogin;
    private TextView txtUsername;
    public static CallbackManager callbackManager;
    private User self;

    /**
     * [Ricardo] GoogleAnalytics Tracker
     * The {@link Tracker} used to record screen views.
     */
    private static final String TAG = "LoginActivity";

    // [Ricardo] Nome do usuario, obtido atraves do Facebook
    public static String userName = "notInitialized";
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // [Ricardo] --------------- Logging activities ---------------------
        Log.d(TAG, "[1] onCreate");
        AnalyticsApplication application = (AnalyticsApplication) getApplication();

        // Log = ThisClass
        // Logging the overall usage of this screen. Does not log the user or current time.
        LoginActivity.logMessageToAnalytics(TAG + "- onCreate", application);

        // Log = [Ricardo Edgard Caceffo][Thu Nov 17 14:36:22 GMT-02:00 2016][LoginActivity][onCreate]
        String s = LoginActivity.generateLogMessage(TAG,"onCreate");
        LoginActivity.logMessageToAnalytics(s, application);
        // [Ricardo] --------------- Logging activities ---------------------

        FacebookSdk.sdkInitialize(getApplicationContext());

        setContentView(R.layout.activity_login);

        txtUsername = (TextView) findViewById(R.id.txtUsername);
        btnLogin = (LoginButton) findViewById(R.id.btn_login);

        /* [Ricardo]
           1) O software solicita o login do facebook apenas se o usuário não logou previamente.
           2) Pela atual implementação, apenas é possível acessar os dados do usuário no momento do login.
           Problema:  Como acessar os dados do usuário nos acessos subsequentes ao primeiro?
           Solução: No primeiro login, o nome do usuario é salvo num registro dentro do dispositivo.
           Assim, nos demais acessos, basta fazer o load do nome do usuário
         */
        if (AccessToken.getCurrentAccessToken() != null) {
            Log.d(TAG, "[2] onCreate  - hasAccessToken");

            // Load do nome do usuario do disco
            LoginActivity.userName = retrieveUserName();
            Log.d(TAG, "[21] onCreate  - LoginActivity.userName = " + LoginActivity.userName);

            startApplication();
        } else {
            Log.d(TAG, "[3] onCreate - hasNotAccessToken");
            txtUsername.setVisibility(View.INVISIBLE);
            setupLogin();

        }

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }

    private void startApplication() {
        Log.d(TAG, "[4] startApplication");
        Intent intent = new Intent(this, Menu3ModulosActivity.class);
        intent.putExtra("user", self);
        startActivity(intent);
    }

    private void setupLogin() {
        Log.d(TAG, "[5] setupLogin");
        txtUsername.setVisibility(View.VISIBLE);

        btnLogin.setReadPermissions(Arrays.asList("email", "user_photos", "public_profile"));
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                doFacebookLogin();
            }
        });
        btnLogin.setVisibility(View.VISIBLE);
    }


    private void doFacebookLogin() {

        // [Ricardo] Retrieve user's name
        Log.d(TAG, "[6] doFacebookLogin" );
        callbackManager = CallbackManager.Factory.create();

        // Set permissions
        //LoginManager.getInstance().logInWithReadPermissions
        // (this, Arrays.asList("email", "user_photos", "public_profile"));

        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {

                    public void onSuccess(LoginResult loginResult) {


                        GraphRequest.newMeRequest(
                                loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {

                                    public void onCompleted(JSONObject me, GraphResponse response) {
                                        if (response.getError() != null) {
                                            Log.d(TAG, "[13] doFacebookLogin - onSucess --> error" );
                                            // handle error
                                        } else {
                                            Log.d(TAG, "[14] doFacebookLogin - onSucess --> success" );

                                            // Inicializa a aplicacao
                                            startApplication();

                                            // get email, id and name of the user
                                            String email = me.optString("email");
                                            String id = me.optString("id");
                                            String name = me.optString("name");
                                            Log.d(TAG, "[15] doFacebookLogin - name = " + name );
                                            Log.d(TAG, "[16] doFacebookLogin - id = " + id );
                                            Log.d(TAG, "[17] doFacebookLogin - email = " + email );

                                            // Seta a variavel estática
                                            LoginActivity.userName = name;
                                            Log.d(TAG, "[18] LoginActivity - name = " + LoginActivity.userName);

                                            // Grava o nome num registro no celular, para acessos posteriores
                                            storeUserName(name);

                                        }
                                    }
                                }
                        ).executeAsync();


                    }

                    public void onCancel() {
                        Log.d(TAG, "[11] doFacebookLogin - onCancel" );
                        Log.d("CANCEL", "On cancel");
                    }

                    public void onError(FacebookException error) {
                        Log.d(TAG, "[12] doFacebookLogin - onError" );
                        Log.d("ERROR", error.toString());
                    }
                });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d(TAG, "[7] onActivityResult");
        super.onActivityResult(requestCode, resultCode, data);

        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    public Action getIndexApiAction() {
        Thing object = new Thing.Builder()
                .setName("Login Page") // TODO: Define a title for the content shown.
                // TODO: Make sure this auto-generated URL is correct.
                .setUrl(Uri.parse("http://[ENTER-YOUR-URL-HERE]"))
                .build();
        return new Action.Builder(Action.TYPE_VIEW)
                .setObject(object)
                .setActionStatus(Action.STATUS_TYPE_COMPLETED)
                .build();
    }



    /**
     * Metodos de store e retrieve do nome do usuario.
     * São usados para, após o primeiro login, salvar o nome do usuário no celular
     * Assim, nos acessos subsequentes - qdo o login ao facebook não é feito - o app consegue
     * acessar o nome do usuário.
     * Source: http://samir-mangroliya.blogspot.com.br/p/android-shared-preferences.html
     */

    private void storeUserName(String userName) {
        Log.d(TAG, "[19] storeUserName - userName = " + userName);

        SharedPreferences sharedPref = getSharedPreferences("mypref", 0);
        SharedPreferences.Editor mEditor = sharedPref.edit();
        mEditor.putString("userName", userName).commit();
        mEditor.commit();

    }

    private String retrieveUserName() {

        SharedPreferences sharedPref = getSharedPreferences("mypref", 0);
        String s =  sharedPref.getString("userName", "error_retrieving_user_name");
        Log.d(TAG, "[20] retrieveUserName - userName = " + s);
        return s;
    }


    /**
     * Gera a mensagem de log para o usuario atual.
     * Recebe como parametro o nome da classe e do metodo.
     * Mensagem de log considera tb a hora atual e o nome
     * @return msgLog
     */
    public static String generateLogMessage(String classe, String metodo) {
        Log.d(classe+ "->" + metodo, "[30] generateLogMessage: classe  = " + classe + " metodo = " + metodo);
        String msgLog = "";

        Date hora = new Date();
        hora.getTime();
        Log.d(TAG, "[31] generateLogMessage: hora  = " + hora);

        msgLog = "[" + LoginActivity.userName + "]" + "[" + hora + "]" + "[" + classe + "]" + "[" + metodo + "]";
        Log.d(TAG, "[32] generateLogMessage = " + msgLog);

        return msgLog;


    }

    // Send the message to the GoogleAnalytics
    public static void logMessageToAnalytics(String message, AnalyticsApplication application) {

        Log.d(TAG, "[33] logMessageToAnalytics = " + message);

        Tracker mTracker;
        mTracker = application.getDefaultTracker();
        mTracker.setScreenName(message);
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
    }


    /****************************** Google Analytics Cycle Control ********************************/

    @Override
    public void onStart() {
        super.onStart();
        Log.d(TAG, "[1] onStart");

        // [Ricardo] --------------- Logging activities ---------------------
        AnalyticsApplication application = (AnalyticsApplication) getApplication();

        // Log = ThisClass
        // Logging the overall usage of this screen. Does not log the user or current time.
        LoginActivity.logMessageToAnalytics(TAG + "- onStart", application);

        // Log = [Ricardo Edgard Caceffo][Thu Nov 17 14:36:22 GMT-02:00 2016][LoginActivity][onStart]
        String s = LoginActivity.generateLogMessage(TAG,"onStart");
        LoginActivity.logMessageToAnalytics(s, application);
        // [Ricardo] --------------- Logging activities ---------------------


    }


    @Override
    public void onResume() {
        super.onResume();
        Log.d(TAG, "[1] onResume");

        // [Ricardo] --------------- Logging activities ---------------------
        AnalyticsApplication application = (AnalyticsApplication) getApplication();

        // Log = ThisClass
        // Logging the overall usage of this screen. Does not log the user or current time.
        LoginActivity.logMessageToAnalytics(TAG + "- onResume", application);

        // Log = [Ricardo Edgard Caceffo][Thu Nov 17 14:36:22 GMT-02:00 2016][LoginActivity][onResume]
        String s = LoginActivity.generateLogMessage(TAG,"onResume");
        LoginActivity.logMessageToAnalytics(s, application);
        // [Ricardo] --------------- Logging activities ---------------------


    }


    @Override
    public void onStop() {
        super.onStop();
        Log.d(TAG, "[1] onStop");

        // [Ricardo] --------------- Logging activities ---------------------
        AnalyticsApplication application = (AnalyticsApplication) getApplication();

        // Log = ThisClass
        // Logging the overall usage of this screen. Does not log the user or current time.
        LoginActivity.logMessageToAnalytics(TAG + "- onStop", application);


        // Log = [Ricardo Edgard Caceffo][Thu Nov 17 14:36:22 GMT-02:00 2016][LoginActivity][onStop]
        String s = LoginActivity.generateLogMessage(TAG,"onStop");
        LoginActivity.logMessageToAnalytics(s, application);
        // [Ricardo] --------------- Logging activities ---------------------

    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "[1] onDestroy");

        // [Ricardo] --------------- Logging activities ---------------------
        AnalyticsApplication application = (AnalyticsApplication) getApplication();

        // Log = ThisClass
        // Logging the overall usage of this screen. Does not log the user or current time.
        LoginActivity.logMessageToAnalytics(TAG + "- onDestroy", application);

        // Log = [Ricardo Edgard Caceffo][Thu Nov 17 14:36:22 GMT-02:00 2016][LoginActivity][onDestroy]
        String s = LoginActivity.generateLogMessage(TAG,"onDestroy");
        LoginActivity.logMessageToAnalytics(s, application);
        // [Ricardo] --------------- Logging activities ---------------------


    }

    @Override
    public void onRestart() {
        super.onRestart();
        Log.d(TAG, "[1] onRestart");

        // [Ricardo] --------------- Logging activities ---------------------
        AnalyticsApplication application = (AnalyticsApplication) getApplication();

        // Log = ThisClass
        // Logging the overall usage of this screen. Does not log the user or current time.
        LoginActivity.logMessageToAnalytics(TAG + "- onRestart", application);

        // Log = [Ricardo Edgard Caceffo][Thu Nov 17 14:36:22 GMT-02:00 2016][LoginActivity][onRestart]
        String s = LoginActivity.generateLogMessage(TAG,"onRestart");
        LoginActivity.logMessageToAnalytics(s, application);
        // [Ricardo] --------------- Logging activities ---------------------


    }
    /****************************** Google Analytics Cycle Control ********************************/

}
