package br.unicamp.vecindario.model;

import java.io.Serializable;

public class User implements Serializable {

    private String id = "-1";
    private String name = "NO NAME";

    public User(String newId, String newName) {
        id = newId;
        name = newName;
    }

    public String getId() { return id; }

    public String getName() { return name; }

    public String getProfilePictureUrl() { return "https://graph.facebook.com/" + id + "/picture"; }
}
