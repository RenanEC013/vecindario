package br.unicamp.vecindario.remote;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.http.AndroidHttpClient;
import android.os.AsyncTask;
import android.util.Log;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.HttpGet;

import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;

import de.hdodenhof.circleimageview.CircleImageView;

public class FacebookRemote extends AsyncTask<String, Void, Boolean> {

    private CircleImageView circleImageView;
    private Bitmap picture;

    public FacebookRemote(CircleImageView newCircleImageView) {
        picture = null;
        circleImageView = newCircleImageView;
    }

    @Override
    protected Boolean doInBackground(String... params) {
        /*try {
            URL fbAvatarUrl = new URL(params[0]);
            picture = BitmapFactory.decodeStream(fbAvatarUrl.openConnection().getInputStream());

        } catch (IOException e) {
            e.printStackTrace();
        }*/

        picture = downloadBitmap(params[0]);

        return true;
    }

    @Override
    protected void onPostExecute(Boolean result) {
        super.onPostExecute(result);
        circleImageView.setImageBitmap(picture);
    }

    static Bitmap downloadBitmap(String url) {
        final AndroidHttpClient client = AndroidHttpClient.newInstance("Android");
        final HttpGet getRequest = new HttpGet(url);

        try {
            HttpResponse response = client.execute(getRequest);
            final int statusCode = response.getStatusLine().getStatusCode();
            if (statusCode != HttpStatus.SC_OK) {
                Log.w("ImageDownloader", "Error " + statusCode + " while retrieving bitmap from " + url);
                return null;
            }

            final HttpEntity entity = response.getEntity();
            if (entity != null) {
                FlushedInputStream inputStream = null;
                try {
                    inputStream = new FlushedInputStream(entity.getContent());
                    return BitmapFactory.decodeStream(inputStream);
                } finally {
                    if (inputStream != null) {
                        inputStream.close();
                    }
                    entity.consumeContent();
                }
            }
        } catch (Exception e) {
            // Could provide a more explicit error message for IOException or IllegalStateException
            getRequest.abort();
            //Log.w("ImageDownloader", "Error while retrieving bitmap from " + url, e.toString());
        } finally {
            if (client != null) {
                client.close();
            }
        }
        return null;
    }

    static class FlushedInputStream extends FilterInputStream {

        public FlushedInputStream(InputStream inputStream) {
            super(inputStream);
        }

        @Override
        public long skip(long n) throws IOException {
            long totalBytesSkipped = 0L;
            while (totalBytesSkipped < n) {
                long bytesSkipped = in.skip(n - totalBytesSkipped);
                if (bytesSkipped == 0L) {
                    int token = read();
                    if (token < 0) {
                        break;  // we reached EOF
                    } else {
                        bytesSkipped = 1; // we read one byte
                    }
                }
                totalBytesSkipped += bytesSkipped;
            }
            return totalBytesSkipped;
        }
    }
}
