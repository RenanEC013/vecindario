package br.unicamp.vecindario;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.analytics.Tracker;

public class UnaPalabra10bActivity extends AppCompatActivity {
    public UnaPalabra10bActivity() {
        // Required empty public constructor
    }

    TextView final_result;
    int alternativa = 0;
    Toolbar toolbar;

    /**
     * [Ricardo] GoogleAnalytics Tracker
     * The {@link Tracker} used to record screen views.
     */
    private static final String TAG = "UnaPalabra10bActivity";

    private static final int NONE = 0;
    private static final int SWIPE = 1;
    private int mode = NONE;
    private float startX;
    private float stopX;
    // We will only detect a swipe if the difference is at least 100 pixels
// Change this value to your needs
    private static final int TRESHOLD = 100;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // [Ricardo] --------------- Logging activities ---------------------
        Log.d(TAG, "[1] onCreate");
        AnalyticsApplication application = (AnalyticsApplication) getApplication();

        // Log = ThisClass
        // Logging the overall usage of this screen. Does not log the user or current time.
        LoginActivity.logMessageToAnalytics(TAG + "- onCreate", application);

        // Log = [Ricardo Edgard Caceffo][Thu Nov 17 14:36:22 GMT-02:00 2016][LoginActivity][onCreate]
        String s = LoginActivity.generateLogMessage(TAG,"onCreate");
        LoginActivity.logMessageToAnalytics(s, application);
        // [Ricardo] --------------- Logging activities ---------------------

        setContentView(R.layout.activity_unapalabra10b);

        RelativeLayout relativeLayout = (RelativeLayout)findViewById(R.id.unapalabra10b);
        relativeLayout.setOnTouchListener (
                new RelativeLayout.OnTouchListener() {

                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        switch (event.getAction() & MotionEvent.ACTION_MASK) {
                            case MotionEvent.ACTION_DOWN:

                                // [Ricardo] --------------- Logging activities ---------------------
                                String n5 = "ACTION Down";
                                AnalyticsApplication application5 = (AnalyticsApplication) getApplication();

                                // Log = ThisClass
                                // Logging the overall usage of this screen. Does not log the user or current time.
                                LoginActivity.logMessageToAnalytics(TAG + n5, application5);

                                // Log = [Ricardo Edgard Caceffo][Thu Nov 17 14:36:22 GMT-02:00 2016][LoginActivity][onStart]
                                String s5 = LoginActivity.generateLogMessage(TAG,n5);
                                LoginActivity.logMessageToAnalytics(s5, application5);
                                // [Ricardo] --------------- Logging activities ---------------------


                                // This happens when you touch the screen with two fingers
                                mode = SWIPE;
                                // You can also use event.getY(1) or the average of the two
                                startX = event.getX(0);
                                break;

                            case MotionEvent.ACTION_UP:

                                // [Ricardo] --------------- Logging activities ---------------------
                                String n2 = "ACTION UP";
                                AnalyticsApplication application2 = (AnalyticsApplication) getApplication();

                                // Log = ThisClass
                                // Logging the overall usage of this screen. Does not log the user or current time.
                                LoginActivity.logMessageToAnalytics(TAG + n2, application2);

                                // Log = [Ricardo Edgard Caceffo][Thu Nov 17 14:36:22 GMT-02:00 2016][LoginActivity][onStart]
                                String s2 = LoginActivity.generateLogMessage(TAG,n2);
                                LoginActivity.logMessageToAnalytics(s2, application2);
                                // [Ricardo] --------------- Logging activities ---------------------


                                // This happens when you release the second finger
                                mode = NONE;
                                if (Math.abs(startX - stopX) > TRESHOLD) {
                                    if (startX > stopX) {
                                        Intent intent = new Intent(v.getContext(),UnaPalabra11Activity.class);
                                        startActivity(intent);
                                    }
                                }
                                mode = NONE;
                                break;

                            case MotionEvent.ACTION_MOVE:

                                // [Ricardo] --------------- Logging activities ---------------------
                                String n3 = "ACTION MOVE";
                                AnalyticsApplication application3 = (AnalyticsApplication) getApplication();

                                // Log = ThisClass
                                // Logging the overall usage of this screen. Does not log the user or current time.
                                LoginActivity.logMessageToAnalytics(TAG + n3, application3);

                                // Log = [Ricardo Edgard Caceffo][Thu Nov 17 14:36:22 GMT-02:00 2016][LoginActivity][onStart]
                                String s3 = LoginActivity.generateLogMessage(TAG,n3);
                                LoginActivity.logMessageToAnalytics(s3, application3);
                                // [Ricardo] --------------- Logging activities ---------------------


                                if (mode == SWIPE) {
                                    stopX = event.getX(0);
                                }
                                break;
                        }

                        return true;
                    }
                });

    };

    @Override
    public boolean onCreateOptionsMenu (Menu menu) {

        // [Ricardo] --------------- Logging activities ---------------------
        Log.d(TAG, "[1] onCreateOptionsMenu");
        AnalyticsApplication application = (AnalyticsApplication) getApplication();

        // Log = ThisClass
        // Logging the overall usage of this screen. Does not log the user or current time.
        LoginActivity.logMessageToAnalytics(TAG + "- onCreateOptionsMenu", application);

        // Log = [Ricardo Edgard Caceffo][Thu Nov 17 14:36:22 GMT-02:00 2016][LoginActivity][onCreateOptionsMenu]
        String s = LoginActivity.generateLogMessage(TAG,"onCreateOptionsMenu");
        LoginActivity.logMessageToAnalytics(s, application);
        // [Ricardo] --------------- Logging activities ---------------------



        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.drawer, menu);

        return true;
    }

    /****************************** Google Analytics Cycle Control ********************************/

    @Override
    public void onStart() {
        super.onStart();
        Log.d(TAG, "[1] onStart");

        // [Ricardo] --------------- Logging activities ---------------------
        AnalyticsApplication application = (AnalyticsApplication) getApplication();

        // Log = ThisClass
        // Logging the overall usage of this screen. Does not log the user or current time.
        LoginActivity.logMessageToAnalytics(TAG + "- onStart", application);

        // Log = [Ricardo Edgard Caceffo][Thu Nov 17 14:36:22 GMT-02:00 2016][LoginActivity][onStart]
        String s = LoginActivity.generateLogMessage(TAG,"onStart");
        LoginActivity.logMessageToAnalytics(s, application);
        // [Ricardo] --------------- Logging activities ---------------------


    }


    @Override
    public void onResume() {
        super.onResume();
        Log.d(TAG, "[1] onResume");

        // [Ricardo] --------------- Logging activities ---------------------
        AnalyticsApplication application = (AnalyticsApplication) getApplication();

        // Log = ThisClass
        // Logging the overall usage of this screen. Does not log the user or current time.
        LoginActivity.logMessageToAnalytics(TAG + "- onResume", application);

        // Log = [Ricardo Edgard Caceffo][Thu Nov 17 14:36:22 GMT-02:00 2016][LoginActivity][onResume]
        String s = LoginActivity.generateLogMessage(TAG,"onResume");
        LoginActivity.logMessageToAnalytics(s, application);
        // [Ricardo] --------------- Logging activities ---------------------


    }


    @Override
    public void onStop() {
        super.onStop();
        Log.d(TAG, "[1] onStop");

        // [Ricardo] --------------- Logging activities ---------------------
        AnalyticsApplication application = (AnalyticsApplication) getApplication();

        // Log = ThisClass
        // Logging the overall usage of this screen. Does not log the user or current time.
        LoginActivity.logMessageToAnalytics(TAG + "- onStop", application);


        // Log = [Ricardo Edgard Caceffo][Thu Nov 17 14:36:22 GMT-02:00 2016][LoginActivity][onStop]
        String s = LoginActivity.generateLogMessage(TAG,"onStop");
        LoginActivity.logMessageToAnalytics(s, application);
        // [Ricardo] --------------- Logging activities ---------------------

    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "[1] onDestroy");

        // [Ricardo] --------------- Logging activities ---------------------
        AnalyticsApplication application = (AnalyticsApplication) getApplication();

        // Log = ThisClass
        // Logging the overall usage of this screen. Does not log the user or current time.
        LoginActivity.logMessageToAnalytics(TAG + "- onDestroy", application);

        // Log = [Ricardo Edgard Caceffo][Thu Nov 17 14:36:22 GMT-02:00 2016][LoginActivity][onDestroy]
        String s = LoginActivity.generateLogMessage(TAG,"onDestroy");
        LoginActivity.logMessageToAnalytics(s, application);
        // [Ricardo] --------------- Logging activities ---------------------


    }

    @Override
    public void onRestart() {
        super.onRestart();
        Log.d(TAG, "[1] onRestart");

        // [Ricardo] --------------- Logging activities ---------------------
        AnalyticsApplication application = (AnalyticsApplication) getApplication();

        // Log = ThisClass
        // Logging the overall usage of this screen. Does not log the user or current time.
        LoginActivity.logMessageToAnalytics(TAG + "- onRestart", application);

        // Log = [Ricardo Edgard Caceffo][Thu Nov 17 14:36:22 GMT-02:00 2016][LoginActivity][onRestart]
        String s = LoginActivity.generateLogMessage(TAG,"onRestart");
        LoginActivity.logMessageToAnalytics(s, application);
        // [Ricardo] --------------- Logging activities ---------------------


    }
    /****************************** Google Analytics Cycle Control ********************************/
}
