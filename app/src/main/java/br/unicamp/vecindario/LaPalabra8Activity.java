package br.unicamp.vecindario;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.analytics.Tracker;

public class LaPalabra8Activity extends AppCompatActivity {
    public LaPalabra8Activity() {
        // Required empty public constructor
    }

    /* 0: não preenchida;  1: preenchida corretamente; -1: preenchida incorretamente */
    int lacuna1 = 0;
    int lacuna2 = 0;

    Toolbar toolbar;

    /**
     * [Ricardo] GoogleAnalytics Tracker
     * The {@link Tracker} used to record screen views.
     */
    private static final String TAG = "LaPalabra8Activity";

    private static final int NONE = 0;
    private static final int SWIPE = 1;
    private int mode = NONE;
    private float startX;
    private float stopX;

    float Xstr1, Xstr2, Ystr1, Ystr2;

    // We will only detect a swipe if the difference is at least 100 pixels
// Change this value to your needs
    private static final int TRESHOLD = 100;

    private float x, y = 0.0f;
    private float elX, elY, laX, laY, losX, losY, lasX, lasY;
    boolean moving = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // [Ricardo] --------------- Logging activities ---------------------
        Log.d(TAG, "[1] onCreate");
        AnalyticsApplication application = (AnalyticsApplication) getApplication();

        // Log = ThisClass
        // Logging the overall usage of this screen. Does not log the user or current time.
        LoginActivity.logMessageToAnalytics(TAG + "- onCreate", application);

        // Log = [Ricardo Edgard Caceffo][Thu Nov 17 14:36:22 GMT-02:00 2016][LoginActivity][onCreate]
        String s = LoginActivity.generateLogMessage(TAG,"onCreate");
        LoginActivity.logMessageToAnalytics(s, application);
        // [Ricardo] --------------- Logging activities ---------------------


        setContentView(R.layout.activity_lapalabra8);

        final AlertDialog alertDialog2 = new AlertDialog.Builder(this).create();
        alertDialog2.setCanceledOnTouchOutside(true);
        alertDialog2.setTitle("Respuesta");

        SeekBar seekBar = (SeekBar)findViewById(R.id.seekBar2);
        seekBar.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return true;
            }
        });

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Módulo 1 - La Palabra");
        setSupportActionBar(toolbar);

        final TextView str1 = (TextView) findViewById(R.id.textView16);
        final TextView str2 = (TextView) findViewById(R.id.textView24);


        RelativeLayout relativeLayout = (RelativeLayout) findViewById(R.id.lapalabra8);

        relativeLayout.setOnTouchListener(
                new RelativeLayout.OnTouchListener() {

                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        switch (event.getAction() & MotionEvent.ACTION_MASK) {
                            case MotionEvent.ACTION_DOWN:

                                // [Ricardo] --------------- Logging activities ---------------------
                                String n = "ACTION DOWN";
                                AnalyticsApplication application = (AnalyticsApplication) getApplication();

                                // Log = ThisClass
                                // Logging the overall usage of this screen. Does not log the user or current time.
                                LoginActivity.logMessageToAnalytics(TAG + n, application);

                                // Log = [Ricardo Edgard Caceffo][Thu Nov 17 14:36:22 GMT-02:00 2016][LoginActivity][onStart]
                                String s = LoginActivity.generateLogMessage(TAG,n);
                                LoginActivity.logMessageToAnalytics(s, application);
                                // [Ricardo] --------------- Logging activities ---------------------


                                // This happens when you touch the screen with two fingers
                                mode = SWIPE;
                                // You can also use event.getY(1) or the average of the two
                                startX = event.getX(0);
                                break;

                            case MotionEvent.ACTION_UP:

                                // [Ricardo] --------------- Logging activities ---------------------
                                String n2 = "ACTION UP";
                                AnalyticsApplication application2 = (AnalyticsApplication) getApplication();

                                // Log = ThisClass
                                // Logging the overall usage of this screen. Does not log the user or current time.
                                LoginActivity.logMessageToAnalytics(TAG + n2, application2);

                                // Log = [Ricardo Edgard Caceffo][Thu Nov 17 14:36:22 GMT-02:00 2016][LoginActivity][onStart]
                                String s2 = LoginActivity.generateLogMessage(TAG,n2);
                                LoginActivity.logMessageToAnalytics(s2, application2);
                                // [Ricardo] --------------- Logging activities ---------------------


                                // This happens when you release the second finger
                                mode = NONE;
                                if (Math.abs(startX - stopX) > TRESHOLD) {
                                    if (startX > stopX) {
                                        System.out.println(str1.getText());
                                        System.out.println(str2.getText());
                                        if ((lacuna1 == 1) && (lacuna2 == 1)) {
                                            Intent intent = new Intent(v.getContext(), LaPalabra8bActivity.class);
                                            startActivity(intent);
                                        } else if ((lacuna1 == 0) || (lacuna2 == 0)) {
                                            alertDialog2.setMessage("Completar todos los huecos.");
                                            alertDialog2.setCanceledOnTouchOutside(true);
                                            alertDialog2.show();
                                        } else {
                                            Intent intent = new Intent(v.getContext(), LaPalabra8aActivity.class);
                                            startActivity(intent);
                                            lacuna1 = 0;
                                            lacuna2 = 0;
                                            str1.setText("____ ");
                                            str2.setText("____ leche.");

                                        }
                                    } else {
                                        Intent intent = new Intent(v.getContext(), LaPalabra7Activity.class);
                                        startActivity(intent);
                                    }
                                }
                                mode = NONE;
                                break;

                            case MotionEvent.ACTION_MOVE:

                                // [Ricardo] --------------- Logging activities ---------------------
                                String n3 = "ACTION MOVE";
                                AnalyticsApplication application3 = (AnalyticsApplication) getApplication();

                                // Log = ThisClass
                                // Logging the overall usage of this screen. Does not log the user or current time.
                                LoginActivity.logMessageToAnalytics(TAG + n3, application3);

                                // Log = [Ricardo Edgard Caceffo][Thu Nov 17 14:36:22 GMT-02:00 2016][LoginActivity][onStart]
                                String s3 = LoginActivity.generateLogMessage(TAG,n3);
                                LoginActivity.logMessageToAnalytics(s3, application3);
                                // [Ricardo] --------------- Logging activities ---------------------


                                if (mode == SWIPE) {
                                    stopX = event.getX(0);
                                }
                                break;
                        }

                        return true;
                    }
                });


        final Button el = (Button) findViewById(R.id.un);

        final int[] changedEl = {0};

        el.setOnTouchListener(
                new RelativeLayout.OnTouchListener() {

                    @Override
                    public boolean onTouch(View v, MotionEvent event) {

                        Xstr1 = str1.getX();
                        Ystr1 = str1.getY();
                        Xstr2 = str2.getX();
                        Ystr2 = str2.getY();

                        switch (event.getAction() & MotionEvent.ACTION_MASK) {
                            case MotionEvent.ACTION_DOWN:
                                // This happens when you touch the screen with two fingers
                                mode = SWIPE;
                                moving = true;

                                break;

                            case MotionEvent.ACTION_UP:
                                // This happens when you release the second finger
                                mode = NONE;
                                moving = false;

                                if ((Math.abs(x - Xstr1) < 60) && (Math.abs(y - Ystr1) < 60)) {
                                    x = elX;
                                    y = elY;

                                    lacuna1 = -1;
                                    str1.setText("EL ");

                                } else if ((Math.abs(x - Xstr2) < 60) && (Math.abs(y - Ystr2) < 60)) {
                                    x = elX;
                                    y = elY;

                                    lacuna2 = -1;
                                    str2.setText("EL leche.");
                                }

                                el.setX(x);
                                el.setY(y);


                                break;

                            case MotionEvent.ACTION_MOVE:
                                if (mode == SWIPE) {
                                    stopX = event.getX(0);
                                }

                                if (moving) {
                                    if (changedEl[0] == 0) {
                                        changedEl[0] = 1;
                                        elX = el.getX();
                                        elY = el.getY();
                                    }

                                    x = event.getRawX() - el.getWidth() / 2;
                                    y = event.getRawY() - el.getHeight() / 2;

                                    el.setX(x);
                                    el.setY(y);

                                }

                                break;
                        }

                        return true;
                    }

                });


        final Button la = (Button) findViewById(R.id.una);

        final int[] changedLa = {0};

        la.setOnTouchListener(
                new RelativeLayout.OnTouchListener() {

                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        Xstr1 = str1.getX();
                        Ystr1 = str1.getY();
                        Xstr2 = str2.getX();
                        Ystr2 = str2.getY();

                        switch (event.getAction() & MotionEvent.ACTION_MASK) {
                            case MotionEvent.ACTION_DOWN:
                                // This happens when you touch the screen with two fingers
                                mode = SWIPE;
                                moving = true;

                                break;

                            case MotionEvent.ACTION_UP:
                                // This happens when you release the second finger
                                mode = NONE;
                                moving = false;

                                if ((Math.abs(x - Xstr1) < 60) && (Math.abs(y - Ystr1) < 60)) {
                                    x = laX;
                                    y = laY;

                                    lacuna1 = 1;
                                    str1.setText("LA ");

                                } else  if ((Math.abs(x - Xstr2) < 60) && (Math.abs(y - Ystr2) < 60))  {
                                    x = laX;
                                    y = laY;

                                    lacuna2 = 1;
                                    str2.setText("LA leche.");
                                }

                                la.setX(x);
                                la.setY(y);

                                break;

                            case MotionEvent.ACTION_MOVE:
                                if (mode == SWIPE) {
                                    stopX = event.getX(0);
                                }

                                if (moving) {
                                    if (changedLa[0] == 0) {
                                        changedLa[0] = 1;
                                        laX = la.getX();
                                        laY = la.getY();
                                    }

                                    x = event.getRawX() - la.getWidth() / 2;
                                    y = event.getRawY() - la.getHeight() / 2;

                                }

                                la.setX(x);
                                la.setY(y);


                                break;
                        }

                        return true;
                    }

                });

        final Button los = (Button) findViewById(R.id.unos);

        final int[] changedLos = {0};

        los.setOnTouchListener(
                new RelativeLayout.OnTouchListener() {

                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        Xstr1 = str1.getX();
                        Ystr1 = str1.getY();
                        Xstr2 = str2.getX();
                        Ystr2 = str2.getY();

                        switch (event.getAction() & MotionEvent.ACTION_MASK) {
                            case MotionEvent.ACTION_DOWN:
                                // This happens when you touch the screen with two fingers
                                mode = SWIPE;
                                moving = true;

                                break;

                            case MotionEvent.ACTION_UP:
                                // This happens when you release the second finger
                                mode = NONE;
                                moving = false;

                                if  ((Math.abs(x - Xstr1) < 60) && (Math.abs(y - Ystr1) < 60))  {
                                    x = losX;
                                    y = losY;

                                    lacuna1 = -1;
                                    str1.setText("LOS ");

                                } else  if ((Math.abs(x - Xstr2) < 60) && (Math.abs(y - Ystr2) < 60)) {
                                    x = losX;
                                    y = losY;

                                    lacuna2 = -1;
                                    str2.setText("LOS leche.");
                                }

                                los.setX(x);
                                los.setY(y);

                                break;

                            case MotionEvent.ACTION_MOVE:
                                if (mode == SWIPE) {
                                    stopX = event.getX(0);
                                }

                                if (moving) {

                                    if (changedLos[0] == 0) {
                                        changedLos[0] = 1;
                                        losX = los.getX();
                                        losY = los.getY();
                                    }

                                    x = event.getRawX() - los.getWidth() / 2;
                                    y = event.getRawY() - los.getHeight() / 2;

                                }

                                los.setX(x);
                                los.setY(y);

                                break;
                        }

                        return true;
                    }

                });

        final Button las = (Button) findViewById(R.id.unas);

        final int[] changedLas = {0};

        las.setOnTouchListener(
                new RelativeLayout.OnTouchListener() {

                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        Xstr1 = str1.getX();
                        Ystr1 = str1.getY();
                        Xstr2 = str2.getX();
                        Ystr2 = str2.getY();

                        switch (event.getAction() & MotionEvent.ACTION_MASK) {
                            case MotionEvent.ACTION_DOWN:
                                // This happens when you touch the screen with two fingers
                                mode = SWIPE;
                                moving = true;
                                // You can also use event.getY(1) or the average of the two
                                startX = event.getX(0);
                                break;

                            case MotionEvent.ACTION_UP:
                                // This happens when you release the second finger
                                mode = NONE;
                                moving = false;

                                if ((Math.abs(x - Xstr1) < 60) && (Math.abs(y - Ystr1) < 60)) {
                                    x = lasX;
                                    y = lasY;

                                    lacuna1 = -1;
                                    str1.setText("LAS ");

                                } else  if ((Math.abs(x - Xstr2) < 60) && (Math.abs(y - Ystr2) < 60)) {
                                    x = lasX;
                                    y = lasY;

                                    lacuna2 = -1;
                                    str2.setText("LAS leche.");
                                }

                                las.setX(x);
                                las.setY(y);

                                break;

                            case MotionEvent.ACTION_MOVE:
                                if (mode == SWIPE) {
                                    stopX = event.getX(0);
                                }

                                if (moving) {
                                    if (changedLas[0] == 0) {
                                        changedLas[0] = 1;
                                        lasX = las.getX();
                                        lasY = las.getY();
                                    }

                                    x = event.getRawX() - las.getWidth() / 2;
                                    y = event.getRawY() - las.getHeight() / 2;

                                    las.setX(x);
                                    las.setY(y);

                                }

                                break;
                        }

                        return true;
                    }

                });

    }


    @Override
    public boolean onCreateOptionsMenu (Menu menu) {

        // [Ricardo] --------------- Logging activities ---------------------
        Log.d(TAG, "[1] onCreateOptionsMenu");
        AnalyticsApplication application = (AnalyticsApplication) getApplication();

        // Log = ThisClass
        // Logging the overall usage of this screen. Does not log the user or current time.
        LoginActivity.logMessageToAnalytics(TAG + "- onCreateOptionsMenu", application);

        // Log = [Ricardo Edgard Caceffo][Thu Nov 17 14:36:22 GMT-02:00 2016][LoginActivity][onCreateOptionsMenu]
        String s = LoginActivity.generateLogMessage(TAG,"onCreateOptionsMenu");
        LoginActivity.logMessageToAnalytics(s, application);
        // [Ricardo] --------------- Logging activities ---------------------


        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.drawer, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {


        // [Ricardo] --------------- Logging activities ---------------------
        Log.d(TAG, "[1] onOptionsItemSelected");
        AnalyticsApplication application = (AnalyticsApplication) getApplication();

        // Log = ThisClass
        // Logging the overall usage of this screen. Does not log the user or current time.
        LoginActivity.logMessageToAnalytics(TAG + "- onOptionsItemSelected  [item.getItemId() = " + item.getItemId() + "]", application);

        // Log = [Ricardo Edgard Caceffo][Thu Nov 17 14:36:22 GMT-02:00 2016][LoginActivity][onOptionsItemSelected]
        String s = LoginActivity.generateLogMessage(TAG,"onOptionsItemSelected [item.getItemId()= " + item.getItemId() + "]");
        LoginActivity.logMessageToAnalytics(s, application);
        // [Ricardo] --------------- Logging activities ---------------------

        switch (item.getItemId()) {
            case R.id.traduccion:
                final AlertDialog alertDialog = new AlertDialog.Builder(this).create();
                alertDialog.setTitle("Traducción");
                alertDialog.setMessage("Aeromoça: O senhor, o que deseja?" +
                        "\n"
                        + "José: Quero o suco de laranja e os biscoitos."
                        + "\n\n" + "José: Hum... Por favor, quero mudar!"
                        + "\n" + "Aeromoça: O que deseja?"
                        + "\n" + "José: Quero a maçã e o leite."
                        + "\n" + "Aeromoça: Não temos leite.");
                alertDialog.setCanceledOnTouchOutside(true);

                alertDialog.show();

                break;

            case R.id.drawernav_modules:
                Intent intent = new Intent(this, Menu3ModulosActivity.class);
                startActivity(intent);
                break;

            case R.id.drawernav_home:
                intent = new Intent(this, MenuModulosActivity.class);
                startActivity(intent);
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    /****************************** Google Analytics Cycle Control ********************************/

    @Override
    public void onStart() {
        super.onStart();
        Log.d(TAG, "[1] onStart");

        // [Ricardo] --------------- Logging activities ---------------------
        AnalyticsApplication application = (AnalyticsApplication) getApplication();

        // Log = ThisClass
        // Logging the overall usage of this screen. Does not log the user or current time.
        LoginActivity.logMessageToAnalytics(TAG + "- onStart", application);

        // Log = [Ricardo Edgard Caceffo][Thu Nov 17 14:36:22 GMT-02:00 2016][LoginActivity][onStart]
        String s = LoginActivity.generateLogMessage(TAG,"onStart");
        LoginActivity.logMessageToAnalytics(s, application);
        // [Ricardo] --------------- Logging activities ---------------------


    }


    @Override
    public void onResume() {
        super.onResume();
        Log.d(TAG, "[1] onResume");

        // [Ricardo] --------------- Logging activities ---------------------
        AnalyticsApplication application = (AnalyticsApplication) getApplication();

        // Log = ThisClass
        // Logging the overall usage of this screen. Does not log the user or current time.
        LoginActivity.logMessageToAnalytics(TAG + "- onResume", application);

        // Log = [Ricardo Edgard Caceffo][Thu Nov 17 14:36:22 GMT-02:00 2016][LoginActivity][onResume]
        String s = LoginActivity.generateLogMessage(TAG,"onResume");
        LoginActivity.logMessageToAnalytics(s, application);
        // [Ricardo] --------------- Logging activities ---------------------


    }


    @Override
    public void onStop() {
        super.onStop();
        Log.d(TAG, "[1] onStop");

        // [Ricardo] --------------- Logging activities ---------------------
        AnalyticsApplication application = (AnalyticsApplication) getApplication();

        // Log = ThisClass
        // Logging the overall usage of this screen. Does not log the user or current time.
        LoginActivity.logMessageToAnalytics(TAG + "- onStop", application);


        // Log = [Ricardo Edgard Caceffo][Thu Nov 17 14:36:22 GMT-02:00 2016][LoginActivity][onStop]
        String s = LoginActivity.generateLogMessage(TAG,"onStop");
        LoginActivity.logMessageToAnalytics(s, application);
        // [Ricardo] --------------- Logging activities ---------------------

    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "[1] onDestroy");

        // [Ricardo] --------------- Logging activities ---------------------
        AnalyticsApplication application = (AnalyticsApplication) getApplication();

        // Log = ThisClass
        // Logging the overall usage of this screen. Does not log the user or current time.
        LoginActivity.logMessageToAnalytics(TAG + "- onDestroy", application);

        // Log = [Ricardo Edgard Caceffo][Thu Nov 17 14:36:22 GMT-02:00 2016][LoginActivity][onDestroy]
        String s = LoginActivity.generateLogMessage(TAG,"onDestroy");
        LoginActivity.logMessageToAnalytics(s, application);
        // [Ricardo] --------------- Logging activities ---------------------


    }

    @Override
    public void onRestart() {
        super.onRestart();
        Log.d(TAG, "[1] onRestart");

        // [Ricardo] --------------- Logging activities ---------------------
        AnalyticsApplication application = (AnalyticsApplication) getApplication();

        // Log = ThisClass
        // Logging the overall usage of this screen. Does not log the user or current time.
        LoginActivity.logMessageToAnalytics(TAG + "- onRestart", application);

        // Log = [Ricardo Edgard Caceffo][Thu Nov 17 14:36:22 GMT-02:00 2016][LoginActivity][onRestart]
        String s = LoginActivity.generateLogMessage(TAG,"onRestart");
        LoginActivity.logMessageToAnalytics(s, application);
        // [Ricardo] --------------- Logging activities ---------------------


    }
    /****************************** Google Analytics Cycle Control ********************************/

}
