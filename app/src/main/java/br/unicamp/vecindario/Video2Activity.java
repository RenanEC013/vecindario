package br.unicamp.vecindario;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.android.gms.analytics.Tracker;
import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayer.ErrorReason;
import com.google.android.youtube.player.YouTubePlayer.PlaybackEventListener;
import com.google.android.youtube.player.YouTubePlayer.PlayerStateChangeListener;
import com.google.android.youtube.player.YouTubePlayer.Provider;
import com.google.android.youtube.player.YouTubePlayerView;

public class Video2Activity extends YouTubeBaseActivity implements YouTubePlayer.OnInitializedListener {

    private static final int RQS_ErrorDialog = 1;

    public static final String API_KEY = "AIzaSyDBQ0lgcWilunoPUm8GmfnXVIlb5Jdu-hI";

    //http://youtu.be/<VIDEO_ID>
    public static String VIDEO_ID = "0hMU_a-DJsE";

    /**
     * [Ricardo] GoogleAnalytics Tracker
     * The {@link Tracker} used to record screen views.
     */
    private static final String TAG = "Video2Activity";

    private static final int NONE = 0;
    private static final int SWIPE = 1;
    private int mode = NONE;
    private float startX;
    private float stopX;
    // We will only detect a swipe if the difference is at least 100 pixels
// Change this value to your needs
    private static final int TRESHOLD = 100;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // [Ricardo] --------------- Logging activities ---------------------
        Log.d(TAG, "[1] onCreate");
        AnalyticsApplication application = (AnalyticsApplication) getApplication();

        // Log = ThisClass
        // Logging the overall usage of this screen. Does not log the user or current time.
        LoginActivity.logMessageToAnalytics(TAG + "- onCreate", application);

        // Log = [Ricardo Edgard Caceffo][Thu Nov 17 14:36:22 GMT-02:00 2016][LoginActivity][onCreate]
        String s = LoginActivity.generateLogMessage(TAG,"onCreate");
        LoginActivity.logMessageToAnalytics(s, application);
        // [Ricardo] --------------- Logging activities ---------------------


        /** attaching layout xml **/
        setContentView(R.layout.activity_video2);

        /** Initializing YouTube player view **/
        YouTubePlayerView youTubePlayerView = (YouTubePlayerView) findViewById(R.id.youtube_view);
        youTubePlayerView.initialize(API_KEY, this);

        final AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setTitle("Traducción");
        alertDialog.setMessage("Por que estudar na Unicamp? Escute o que os nossos amigos disseram." + "\n\n" +
                "ARMANDO: O grupo de pesquisa daqui tem parceria com um dos professores com que eu já" +
                "tinha um trabalho prévio realizado. Ele me indicou, que tinha uma oportunidade de fazer o" +
                "doutorado aqui e foi por isso que decidi vir para cá." + "\n\n" +
                "JADISHA: Porque sei da experiência de vários amigos, professores, que estudam na" +
                "universidade. Então me contaram da experiência aqui no Brasil, que a educação é muito" +
                "diferente da que se oferece em outros países e também porque, a verdade é que o Brasil é" +
                "uma potência, ou seja, é um país desenvolvido e que tem um nível de educação superior ao" +
                "dos demais países daqui da América Latina." +
                "\n\n" +
                "MARCELO: Bom, porque é muito próximo do meu país. Além de que eu gostei muito das" +
                "pessoas, do quanto são alegres... e além disso é um país irmão, é um país latinoamericano.");
        alertDialog.setCanceledOnTouchOutside(true);

        final AlertDialog alertDialog2 = new AlertDialog.Builder(this).create();
        alertDialog2.setTitle("La Palabra");
        alertDialog2.setMessage("¿Porqué estudias en Unicamp? Observa qué dijeron nuestros amigos:");
        alertDialog2.setCanceledOnTouchOutside(true);
        alertDialog2.show();


        RelativeLayout relativeLayout = (RelativeLayout)findViewById(R.id.video2);
        relativeLayout.setOnTouchListener (
                new RelativeLayout.OnTouchListener() {

                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        switch (event.getAction() & MotionEvent.ACTION_MASK) {
                            case MotionEvent.ACTION_DOWN:
                                // This happens when you touch the screen with two fingers
                                mode = SWIPE;
                                // You can also use event.getY(1) or the average of the two
                                startX = event.getX(0);
                                break;

                            case MotionEvent.ACTION_UP:
                                // This happens when you release the second finger
                                mode = NONE;
                                if (Math.abs(startX - stopX) > TRESHOLD) {
                                    if (startX > stopX) {
                                        Intent intent = new Intent(v.getContext(), LaPalabra2Activity.class);
                                        startActivity(intent);
                                    } else {
                                        Intent intent = new Intent(v.getContext(), LaPalabra1Activity.class);
                                        startActivity(intent);
                                    }
                                }
                                mode = NONE;
                                break;

                            case MotionEvent.ACTION_MOVE:
                                if (mode == SWIPE) {
                                    stopX = event.getX(0);
                                }
                                break;
                        }

                        return true;
                    }
                });
        ImageButton next2 = (ImageButton) findViewById(R.id.button_id2);
        next2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                alertDialog.show();
            }
        });
    }

    @Override
    public void onInitializationFailure(Provider provider,
                                        YouTubeInitializationResult result) {

        // [Ricardo] --------------- Logging activities ---------------------
        Log.d(TAG, "[90] onInitializationFailure");
        AnalyticsApplication application = (AnalyticsApplication) getApplication();

        // Log = ThisClass
        // Logging the overall usage of this screen. Does not log the user or current time.
        LoginActivity.logMessageToAnalytics(TAG + "onInitializationFailure", application);

        // Log = [Ricardo Edgard Caceffo][Thu Nov 17 14:36:22 GMT-02:00 2016][LoginActivity][onInitializationFailure]
        String s = LoginActivity.generateLogMessage(TAG,"onInitializationFailure");
        LoginActivity.logMessageToAnalytics(s, application);
        // [Ricardo] --------------- Logging activities ---------------------

        if (result.isUserRecoverableError()) {
            result.getErrorDialog(this, RQS_ErrorDialog).show();
            System.out.println("abc");
        } else {
            Toast.makeText(this,
                    "YouTubePlayer.onInitializationFailure(): " + result.toString(),
                    Toast.LENGTH_LONG).show();
            System.out.println("cde");
        }
    }


    @Override
    public void onInitializationSuccess(Provider provider, YouTubePlayer player, boolean wasRestored) {

        // [Ricardo] --------------- Logging activities ---------------------
        Log.d(TAG, "[91] onInitializationSuccess");
        AnalyticsApplication application = (AnalyticsApplication) getApplication();

        // Log = ThisClass
        // Logging the overall usage of this screen. Does not log the user or current time.
        LoginActivity.logMessageToAnalytics(TAG + "onInitializationSuccess", application);

        // Log = [Ricardo Edgard Caceffo][Thu Nov 17 14:36:22 GMT-02:00 2016][LoginActivity][onInitializationSuccess]
        String s = LoginActivity.generateLogMessage(TAG,"onInitializationSuccess");
        LoginActivity.logMessageToAnalytics(s, application);
        // [Ricardo] --------------- Logging activities ---------------------

        /** add listeners to YouTubePlayer instance **/
        player.setPlayerStateChangeListener(playerStateChangeListener);
        player.setPlaybackEventListener(playbackEventListener);

        /** Start buffering **/
        if (!wasRestored) {
            player.cueVideo(VIDEO_ID);
        }
    }

    private PlaybackEventListener playbackEventListener = new PlaybackEventListener() {



        @Override
        public void onBuffering(boolean arg0) {
        }

        @Override
        public void onPaused() {
        }

        @Override
        public void onPlaying() {
        }

        @Override
        public void onSeekTo(int arg0) {
        }

        @Override
        public void onStopped() {
        }

    };

    private PlayerStateChangeListener playerStateChangeListener = new PlayerStateChangeListener() {

        @Override
        public void onAdStarted() {
        }

        @Override
        public void onError(ErrorReason arg0) {
        }

        @Override
        public void onLoaded(String arg0) {
        }

        @Override
        public void onLoading() {
        }

        @Override
        public void onVideoEnded() {
        }

        @Override
        public void onVideoStarted() {
        }

    };
    /****************************** Google Analytics Cycle Control ********************************/

    @Override
    public void onStart() {
        super.onStart();
        Log.d(TAG, "[1] onStart");

        // [Ricardo] --------------- Logging activities ---------------------
        AnalyticsApplication application = (AnalyticsApplication) getApplication();

        // Log = ThisClass
        // Logging the overall usage of this screen. Does not log the user or current time.
        LoginActivity.logMessageToAnalytics(TAG + "- onStart", application);

        // Log = [Ricardo Edgard Caceffo][Thu Nov 17 14:36:22 GMT-02:00 2016][LoginActivity][onStart]
        String s = LoginActivity.generateLogMessage(TAG,"onStart");
        LoginActivity.logMessageToAnalytics(s, application);
        // [Ricardo] --------------- Logging activities ---------------------


    }


    @Override
    public void onResume() {
        super.onResume();
        Log.d(TAG, "[1] onResume");

        // [Ricardo] --------------- Logging activities ---------------------
        AnalyticsApplication application = (AnalyticsApplication) getApplication();

        // Log = ThisClass
        // Logging the overall usage of this screen. Does not log the user or current time.
        LoginActivity.logMessageToAnalytics(TAG + "- onResume", application);

        // Log = [Ricardo Edgard Caceffo][Thu Nov 17 14:36:22 GMT-02:00 2016][LoginActivity][onResume]
        String s = LoginActivity.generateLogMessage(TAG,"onResume");
        LoginActivity.logMessageToAnalytics(s, application);
        // [Ricardo] --------------- Logging activities ---------------------


    }


    @Override
    public void onStop() {
        super.onStop();
        Log.d(TAG, "[1] onStop");

        // [Ricardo] --------------- Logging activities ---------------------
        AnalyticsApplication application = (AnalyticsApplication) getApplication();

        // Log = ThisClass
        // Logging the overall usage of this screen. Does not log the user or current time.
        LoginActivity.logMessageToAnalytics(TAG + "- onStop", application);


        // Log = [Ricardo Edgard Caceffo][Thu Nov 17 14:36:22 GMT-02:00 2016][LoginActivity][onStop]
        String s = LoginActivity.generateLogMessage(TAG,"onStop");
        LoginActivity.logMessageToAnalytics(s, application);
        // [Ricardo] --------------- Logging activities ---------------------

    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "[1] onDestroy");

        // [Ricardo] --------------- Logging activities ---------------------
        AnalyticsApplication application = (AnalyticsApplication) getApplication();

        // Log = ThisClass
        // Logging the overall usage of this screen. Does not log the user or current time.
        LoginActivity.logMessageToAnalytics(TAG + "- onDestroy", application);

        // Log = [Ricardo Edgard Caceffo][Thu Nov 17 14:36:22 GMT-02:00 2016][LoginActivity][onDestroy]
        String s = LoginActivity.generateLogMessage(TAG,"onDestroy");
        LoginActivity.logMessageToAnalytics(s, application);
        // [Ricardo] --------------- Logging activities ---------------------


    }

    @Override
    public void onRestart() {
        super.onRestart();
        Log.d(TAG, "[1] onRestart");

        // [Ricardo] --------------- Logging activities ---------------------
        AnalyticsApplication application = (AnalyticsApplication) getApplication();

        // Log = ThisClass
        // Logging the overall usage of this screen. Does not log the user or current time.
        LoginActivity.logMessageToAnalytics(TAG + "- onRestart", application);

        // Log = [Ricardo Edgard Caceffo][Thu Nov 17 14:36:22 GMT-02:00 2016][LoginActivity][onRestart]
        String s = LoginActivity.generateLogMessage(TAG,"onRestart");
        LoginActivity.logMessageToAnalytics(s, application);
        // [Ricardo] --------------- Logging activities ---------------------


    }
    /****************************** Google Analytics Cycle Control ********************************/
}