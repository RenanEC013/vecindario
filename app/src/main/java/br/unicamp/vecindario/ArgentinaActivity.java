package br.unicamp.vecindario;

import android.app.AlertDialog;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.google.android.gms.analytics.Tracker;

/**
 * Created by renan on 16/09/2016.
 */
public class ArgentinaActivity extends AppCompatActivity {
    public ArgentinaActivity() {

    }

    Toolbar toolbar;

    /**
     * [Ricardo] GoogleAnalytics Tracker
     * The {@link Tracker} used to record screen views.
     */
    private static final String TAG = "ArgentinaActivity";

    private static final int NONE = 0;
    private static final int SWIPE = 1;
    private int mode = NONE;
    private float startX;
    private float stopX;
    // We will only detect a swipe if the difference is at least 100 pixels
// Change this value to your needs
    private static final int TRESHOLD = 100;

    int palabra = 0;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // [Ricardo] --------------- Logging activities ---------------------
        Log.d(TAG, "[1] onCreate");
        AnalyticsApplication application = (AnalyticsApplication) getApplication();

        // Log = ThisClass
        // Logging the overall usage of this screen. Does not log the user or current time.
        LoginActivity.logMessageToAnalytics(TAG + "- onCreate", application);

        // Log = [Ricardo Edgard Caceffo][Thu Nov 17 14:36:22 GMT-02:00 2016][LoginActivity][onCreate]
        String s = LoginActivity.generateLogMessage(TAG,"onCreate");
        LoginActivity.logMessageToAnalytics(s, application);
        // [Ricardo] --------------- Logging activities ---------------------

        setContentView(R.layout.activity_argentina);

        SeekBar seekBar = (SeekBar)findViewById(R.id.seekBar2);
        seekBar.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return true;
            }
        });

        TextView textView1 = (TextView) findViewById(R.id.textView1);
        TextView textView3 = (TextView) findViewById(R.id.textView3);
        TextView textView4 = (TextView) findViewById(R.id.textView4);
        TextView textView7 = (TextView) findViewById(R.id.textView7);
        TextView textView8 = (TextView) findViewById(R.id.textView8);

        ImageButton next1 = (ImageButton) this.findViewById(R.id.imageButton1);
        ImageButton next2 = (ImageButton) this.findViewById(R.id.imageButton2);
        ImageButton next3 = (ImageButton) this.findViewById(R.id.imageButton3);
        ImageButton next4 = (ImageButton) this.findViewById(R.id.imageButton4);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            String value = extras.getString("key");
            System.out.println("value = ");
            System.out.println(value);
            if (value.equals ("argentina")) {
                System.out.println("Hermanos");
                palabra = 1;
                final MediaPlayer mp1 = MediaPlayer.create(this, R.raw.vocabularioargentina);
                next1.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View view) {

                        // [Ricardo] --------------- Logging activities ---------------------
                        Log.d(TAG, "[1] Play Argentina mp1");
                        AnalyticsApplication application = (AnalyticsApplication) getApplication();

                        // Log = ThisClass
                        // Logging the overall usage of this screen. Does not log the user or current time.
                        LoginActivity.logMessageToAnalytics(TAG + "- Play Argentina mp1", application);

                        // Log = [Ricardo Edgard Caceffo][Thu Nov 17 14:36:22 GMT-02:00 2016][LoginActivity][Play mp1]
                        String s = LoginActivity.generateLogMessage(TAG,"Play Argentina mp1");
                        LoginActivity.logMessageToAnalytics(s, application);
                        // [Ricardo] --------------- Logging activities ---------------------

                        mp1.start();
                    }
                });

                final MediaPlayer mp2 = MediaPlayer.create(this, R.raw.vocabulariogente);
                next2.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View view) {

                        // [Ricardo] --------------- Logging activities ---------------------
                        Log.d(TAG, "[1] Play Argentina mp2");
                        AnalyticsApplication application = (AnalyticsApplication) getApplication();

                        // Log = ThisClass
                        // Logging the overall usage of this screen. Does not log the user or current time.
                        LoginActivity.logMessageToAnalytics(TAG + "- Play Argentina mp2", application);

                        // Log = [Ricardo Edgard Caceffo][Thu Nov 17 14:36:22 GMT-02:00 2016][LoginActivity][Play Argentina mp2]
                        String s = LoginActivity.generateLogMessage(TAG,"Play Argentina mp2");
                        LoginActivity.logMessageToAnalytics(s, application);
                        // [Ricardo] --------------- Logging activities ---------------------

                        mp2.start();
                    }
                });

                final MediaPlayer mp3 = MediaPlayer.create(this, R.raw.vocabulariogirar);
                next3.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View view) {

                        // [Ricardo] --------------- Logging activities ---------------------
                        Log.d(TAG, "[1] Play Argentina mp3");
                        AnalyticsApplication application = (AnalyticsApplication) getApplication();

                        // Log = ThisClass
                        // Logging the overall usage of this screen. Does not log the user or current time.
                        LoginActivity.logMessageToAnalytics(TAG + "- Play Argentina mp3", application);

                        // Log = [Ricardo Edgard Caceffo][Thu Nov 17 14:36:22 GMT-02:00 2016][LoginActivity][Play Argentina mp3]
                        String s = LoginActivity.generateLogMessage(TAG,"Play Argentina mp3");
                        LoginActivity.logMessageToAnalytics(s, application);
                        // [Ricardo] --------------- Logging activities ---------------------

                        mp3.start();
                    }
                });

                final MediaPlayer mp4 = MediaPlayer.create(this, R.raw.vocabulariogema);
                next4.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View view) {
                        // [Ricardo] --------------- Logging activities ---------------------
                        Log.d(TAG, "[1] Play Argentina mp4");
                        AnalyticsApplication application = (AnalyticsApplication) getApplication();

                        // Log = ThisClass
                        // Logging the overall usage of this screen. Does not log the user or current time.
                        LoginActivity.logMessageToAnalytics(TAG + "- Play Argentina mp4", application);

                        // Log = [Ricardo Edgard Caceffo][Thu Nov 17 14:36:22 GMT-02:00 2016][LoginActivity][Play Argentina mp4]
                        String s = LoginActivity.generateLogMessage(TAG,"Play Argentina mp4");
                        LoginActivity.logMessageToAnalytics(s, application);
                        // [Ricardo] --------------- Logging activities ---------------------
                        mp4.start();
                    }
                });

                textView1.setText("La ge, delante de E e I, tiene un sonido aspirado.");
                textView3.setText("Argentina /x/");
                textView4.setText("gente");
                textView7.setText("girar");
                textView8.setText("gema");

            } else if (value.equals("madrid")) {
                System.out.println("Passei por aqui");
                palabra = 2;
                final MediaPlayer mp1 = MediaPlayer.create(this, R.raw.vocabulariomadrid);
                next1.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View view) {
                        // [Ricardo] --------------- Logging activities ---------------------
                        Log.d(TAG, "[1] Play Madrid mp1");
                        AnalyticsApplication application = (AnalyticsApplication) getApplication();

                        // Log = ThisClass
                        // Logging the overall usage of this screen. Does not log the user or current time.
                        LoginActivity.logMessageToAnalytics(TAG + "- Play Play Madrid mp1", application);

                        // Log = [Ricardo Edgard Caceffo][Thu Nov 17 14:36:22 GMT-02:00 2016][LoginActivity][Play Play Madrid mp1]
                        String s = LoginActivity.generateLogMessage(TAG,"Play Madrid mp1");
                        LoginActivity.logMessageToAnalytics(s, application);
                        // [Ricardo] --------------- Logging activities ---------------------
                        mp1.start();
                    }
                });

                final MediaPlayer mp2 = MediaPlayer.create(this, R.raw.vocabularionovedad);
                next2.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View view) {

                        // [Ricardo] --------------- Logging activities ---------------------
                        String n = "[1] Play Madrid mp2";
                        Log.d(TAG, "[1] " + n);
                        AnalyticsApplication application = (AnalyticsApplication) getApplication();

                        // Log = ThisClass
                        // Logging the overall usage of this screen. Does not log the user or current time.
                        LoginActivity.logMessageToAnalytics(TAG + n, application);

                        // Log = [Ricardo Edgard Caceffo][Thu Nov 17 14:36:22 GMT-02:00 2016][LoginActivity][Play Play Madrid mp1]
                        String s = LoginActivity.generateLogMessage(TAG,n);
                        LoginActivity.logMessageToAnalytics(s, application);
                        // [Ricardo] --------------- Logging activities ---------------------

                        mp2.start();
                    }
                });

                final MediaPlayer mp3 = MediaPlayer.create(this, R.raw.vocabulariociudad);
                next3.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View view) {

                        // [Ricardo] --------------- Logging activities ---------------------
                        String n = "[1] Play Madrid mp3";
                        Log.d(TAG, "[1] " + n);
                        AnalyticsApplication application = (AnalyticsApplication) getApplication();

                        // Log = ThisClass
                        // Logging the overall usage of this screen. Does not log the user or current time.
                        LoginActivity.logMessageToAnalytics(TAG + n, application);

                        // Log = [Ricardo Edgard Caceffo][Thu Nov 17 14:36:22 GMT-02:00 2016][LoginActivity][Play Play Madrid mp1]
                        String s = LoginActivity.generateLogMessage(TAG,n);
                        LoginActivity.logMessageToAnalytics(s, application);
                        // [Ricardo] --------------- Logging activities ---------------------

                        mp3.start();
                    }
                });

                final MediaPlayer mp4 = MediaPlayer.create(this, R.raw.vocabulariouniversidad);
                next4.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View view) {


                        // [Ricardo] --------------- Logging activities ---------------------
                        String n = "[1] Play Madrid mp4";
                        Log.d(TAG, "[1] " + n);
                        AnalyticsApplication application = (AnalyticsApplication) getApplication();

                        // Log = ThisClass
                        // Logging the overall usage of this screen. Does not log the user or current time.
                        LoginActivity.logMessageToAnalytics(TAG + n, application);

                        // Log = [Ricardo Edgard Caceffo][Thu Nov 17 14:36:22 GMT-02:00 2016][LoginActivity][Play Play Madrid mp1]
                        String s = LoginActivity.generateLogMessage(TAG,n);
                        LoginActivity.logMessageToAnalytics(s, application);
                        // [Ricardo] --------------- Logging activities ---------------------

                        mp4.start();
                    }
                });

                textView1.setText("La DE, en el final de la palabra se pronuncia de forma suave y no lleva vocal delante.");
                textView3.setText("Madrid /d/");
                textView4.setText("novedad");
                textView7.setText("ciudad");
                textView8.setText("universidad");


            } else if (value.equals("zapatos")) {
                palabra = 3;
                final MediaPlayer mp1 = MediaPlayer.create(this, R.raw.vocabulariozapato);
                next1.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View view) {

                        // [Ricardo] --------------- Logging activities ---------------------
                        String n = "[1] Play Zapatos mp1";
                        Log.d(TAG, "[1] " + n);
                        AnalyticsApplication application = (AnalyticsApplication) getApplication();

                        // Log = ThisClass
                        // Logging the overall usage of this screen. Does not log the user or current time.
                        LoginActivity.logMessageToAnalytics(TAG + n, application);

                        // Log = [Ricardo Edgard Caceffo][Thu Nov 17 14:36:22 GMT-02:00 2016][LoginActivity][Play Play Madrid mp1]
                        String s = LoginActivity.generateLogMessage(TAG,n);
                        LoginActivity.logMessageToAnalytics(s, application);
                        // [Ricardo] --------------- Logging activities ---------------------


                        mp1.start();
                    }
                });

                final MediaPlayer mp2 = MediaPlayer.create(this, R.raw.vocabularioazafata);
                next2.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View view) {

                        // [Ricardo] --------------- Logging activities ---------------------
                        String n = "[1] Play Zapatos mp2";
                        Log.d(TAG, "[1] " + n);
                        AnalyticsApplication application = (AnalyticsApplication) getApplication();

                        // Log = ThisClass
                        // Logging the overall usage of this screen. Does not log the user or current time.
                        LoginActivity.logMessageToAnalytics(TAG + n, application);

                        // Log = [Ricardo Edgard Caceffo][Thu Nov 17 14:36:22 GMT-02:00 2016][LoginActivity][Play Play Madrid mp1]
                        String s = LoginActivity.generateLogMessage(TAG,n);
                        LoginActivity.logMessageToAnalytics(s, application);
                        // [Ricardo] --------------- Logging activities ---------------------

                        mp2.start();
                    }
                });

                final MediaPlayer mp3 = MediaPlayer.create(this, R.raw.vocabulariorazon);
                next3.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View view) {

                        // [Ricardo] --------------- Logging activities ---------------------
                        String n = "[1] Play Zapatos mp3";
                        Log.d(TAG, "[1] " + n);
                        AnalyticsApplication application = (AnalyticsApplication) getApplication();

                        // Log = ThisClass
                        // Logging the overall usage of this screen. Does not log the user or current time.
                        LoginActivity.logMessageToAnalytics(TAG + n, application);

                        // Log = [Ricardo Edgard Caceffo][Thu Nov 17 14:36:22 GMT-02:00 2016][LoginActivity][Play Play Madrid mp1]
                        String s = LoginActivity.generateLogMessage(TAG,n);
                        LoginActivity.logMessageToAnalytics(s, application);
                        // [Ricardo] --------------- Logging activities ---------------------


                        mp3.start();
                    }
                });

                final MediaPlayer mp4 = MediaPlayer.create(this, R.raw.vocabulariocazador);
                next4.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View view) {

                        // [Ricardo] --------------- Logging activities ---------------------
                        String n = "[1] Play Zapatos mp4";
                        Log.d(TAG, "[1] " + n);
                        AnalyticsApplication application = (AnalyticsApplication) getApplication();

                        // Log = ThisClass
                        // Logging the overall usage of this screen. Does not log the user or current time.
                        LoginActivity.logMessageToAnalytics(TAG + n, application);

                        // Log = [Ricardo Edgard Caceffo][Thu Nov 17 14:36:22 GMT-02:00 2016][LoginActivity][Play Play Madrid mp1]
                        String s = LoginActivity.generateLogMessage(TAG,n);
                        LoginActivity.logMessageToAnalytics(s, application);
                        // [Ricardo] --------------- Logging activities ---------------------

                        mp4.start();
                    }
                });

                textView1.setText("La ZETA, en España, se pronuncia de forma interdental. En hispanoamérica, se pronuncia como una S.");
                textView3.setText("Zapato /z/");
                textView4.setText("azafata");
                textView7.setText("razón");
                textView8.setText("cazador");

            } else if (value.equals("mexico")) {
                palabra = 4;
                final MediaPlayer mp1 = MediaPlayer.create(this, R.raw.vocabulariomexico);
                next1.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View view) {

                        // [Ricardo] --------------- Logging activities ---------------------
                        String n = "[1] Play Mexico mp1";
                        Log.d(TAG, "[1] " + n);
                        AnalyticsApplication application = (AnalyticsApplication) getApplication();

                        // Log = ThisClass
                        // Logging the overall usage of this screen. Does not log the user or current time.
                        LoginActivity.logMessageToAnalytics(TAG + n, application);

                        // Log = [Ricardo Edgard Caceffo][Thu Nov 17 14:36:22 GMT-02:00 2016][LoginActivity][Play Play Madrid mp1]
                        String s = LoginActivity.generateLogMessage(TAG,n);
                        LoginActivity.logMessageToAnalytics(s, application);
                        // [Ricardo] --------------- Logging activities ---------------------

                        mp1.start();
                    }
                });

                final MediaPlayer mp2 = MediaPlayer.create(this, R.raw.vocabularioextranjero);
                next2.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View view) {

                        // [Ricardo] --------------- Logging activities ---------------------
                        String n = "[1] Play Mexico mp2";
                        Log.d(TAG, "[1] " + n);
                        AnalyticsApplication application = (AnalyticsApplication) getApplication();

                        // Log = ThisClass
                        // Logging the overall usage of this screen. Does not log the user or current time.
                        LoginActivity.logMessageToAnalytics(TAG + n, application);

                        // Log = [Ricardo Edgard Caceffo][Thu Nov 17 14:36:22 GMT-02:00 2016][LoginActivity][Play Play Madrid mp1]
                        String s = LoginActivity.generateLogMessage(TAG,n);
                        LoginActivity.logMessageToAnalytics(s, application);
                        // [Ricardo] --------------- Logging activities ---------------------

                        mp2.start();
                    }
                });

                final MediaPlayer mp3 = MediaPlayer.create(this, R.raw.vocabularioexperiencia);
                next3.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View view) {


                        // [Ricardo] --------------- Logging activities ---------------------
                        String n = "[1] Play Mexico mp3";
                        Log.d(TAG, "[1] " + n);
                        AnalyticsApplication application = (AnalyticsApplication) getApplication();

                        // Log = ThisClass
                        // Logging the overall usage of this screen. Does not log the user or current time.
                        LoginActivity.logMessageToAnalytics(TAG + n, application);

                        // Log = [Ricardo Edgard Caceffo][Thu Nov 17 14:36:22 GMT-02:00 2016][LoginActivity][Play Play Madrid mp1]
                        String s = LoginActivity.generateLogMessage(TAG, n);
                        LoginActivity.logMessageToAnalytics(s, application);
                        // [Ricardo] --------------- Logging activities ---------------------

                        mp3.start();
                    }
                });

                final MediaPlayer mp4 = MediaPlayer.create(this, R.raw.vocabularioexposicion);
                next4.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View view) {


                        // [Ricardo] --------------- Logging activities ---------------------
                        String n = "[1] Play Mexico mp4";
                        Log.d(TAG, "[1] " + n);
                        AnalyticsApplication application = (AnalyticsApplication) getApplication();

                        // Log = ThisClass
                        // Logging the overall usage of this screen. Does not log the user or current time.
                        LoginActivity.logMessageToAnalytics(TAG + n, application);

                        // Log = [Ricardo Edgard Caceffo][Thu Nov 17 14:36:22 GMT-02:00 2016][LoginActivity][Play Play Madrid mp1]
                        String s = LoginActivity.generateLogMessage(TAG,n);
                        LoginActivity.logMessageToAnalytics(s, application);
                        // [Ricardo] --------------- Logging activities ---------------------


                        mp4.start();
                    }
                });

                textView1.setText("La EQUIS se pronuncia de dos formas: R; KS");
                textView3.setText("México /x/");
                textView4.setText("extranjero");
                textView7.setText("experiencia");
                textView8.setText("exposición");

            } else if (value.equals("coche")) {
                palabra = 5;
                final MediaPlayer mp1 = MediaPlayer.create(this, R.raw.vocabulariocoche);
                next1.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View view) {


                        // [Ricardo] --------------- Logging activities ---------------------
                        String n = "[1] Play Coche mp1";
                        Log.d(TAG, "[1] " + n);
                        AnalyticsApplication application = (AnalyticsApplication) getApplication();

                        // Log = ThisClass
                        // Logging the overall usage of this screen. Does not log the user or current time.
                        LoginActivity.logMessageToAnalytics(TAG + n, application);

                        // Log = [Ricardo Edgard Caceffo][Thu Nov 17 14:36:22 GMT-02:00 2016][LoginActivity][Play Play Madrid mp1]
                        String s = LoginActivity.generateLogMessage(TAG,n);
                        LoginActivity.logMessageToAnalytics(s, application);
                        // [Ricardo] --------------- Logging activities ---------------------

                        mp1.start();
                    }
                });

                final MediaPlayer mp2 = MediaPlayer.create(this, R.raw.vocabulariomucho);
                next2.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View view) {


                        // [Ricardo] --------------- Logging activities ---------------------
                        String n = "[1] Play Coche mp2";
                        Log.d(TAG, "[1] " + n);
                        AnalyticsApplication application = (AnalyticsApplication) getApplication();

                        // Log = ThisClass
                        // Logging the overall usage of this screen. Does not log the user or current time.
                        LoginActivity.logMessageToAnalytics(TAG + n, application);

                        // Log = [Ricardo Edgard Caceffo][Thu Nov 17 14:36:22 GMT-02:00 2016][LoginActivity][Play Play Madrid mp1]
                        String s = LoginActivity.generateLogMessage(TAG,n);
                        LoginActivity.logMessageToAnalytics(s, application);
                        // [Ricardo] --------------- Logging activities ---------------------

                        mp2.start();
                    }
                });

                final MediaPlayer mp3 = MediaPlayer.create(this, R.raw.vocabularionoche);
                next3.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View view) {

                        // [Ricardo] --------------- Logging activities ---------------------
                        String n = "[1] Play Coche mp3";
                        Log.d(TAG, "[1] " + n);
                        AnalyticsApplication application = (AnalyticsApplication) getApplication();

                        // Log = ThisClass
                        // Logging the overall usage of this screen. Does not log the user or current time.
                        LoginActivity.logMessageToAnalytics(TAG + n, application);

                        // Log = [Ricardo Edgard Caceffo][Thu Nov 17 14:36:22 GMT-02:00 2016][LoginActivity][Play Play Madrid mp1]
                        String s = LoginActivity.generateLogMessage(TAG,n);
                        LoginActivity.logMessageToAnalytics(s, application);
                        // [Ricardo] --------------- Logging activities ---------------------

                        mp3.start();
                    }
                });

                final MediaPlayer mp4 = MediaPlayer.create(this, R.raw.vocabularioocho);
                next4.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View view) {

                        // [Ricardo] --------------- Logging activities ---------------------
                        String n = "[1] Play Coche mp4";
                        Log.d(TAG, "[1] " + n);
                        AnalyticsApplication application = (AnalyticsApplication) getApplication();

                        // Log = ThisClass
                        // Logging the overall usage of this screen. Does not log the user or current time.
                        LoginActivity.logMessageToAnalytics(TAG + n, application);

                        // Log = [Ricardo Edgard Caceffo][Thu Nov 17 14:36:22 GMT-02:00 2016][LoginActivity][Play Play Madrid mp1]
                        String s = LoginActivity.generateLogMessage(TAG,n);
                        LoginActivity.logMessageToAnalytics(s, application);
                        // [Ricardo] --------------- Logging activities ---------------------


                        mp4.start();
                    }
                });

                textView1.setText("La CHE es un dígrafo formado por las consonantes agrupadas CH. Se pronuncia como TCH.");
                textView3.setText("coche /ch/");
                textView4.setText("mucho");
                textView7.setText("noche");
                textView8.setText("ocho");

            } else if (value.equals("gato")) {
                palabra = 6;
                final MediaPlayer mp1 = MediaPlayer.create(this, R.raw.vocabulariogato);
                next1.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View view) {

                        // [Ricardo] --------------- Logging activities ---------------------
                        String n = "[1] Play Gato mp1";
                        Log.d(TAG, "[1] " + n);
                        AnalyticsApplication application = (AnalyticsApplication) getApplication();

                        // Log = ThisClass
                        // Logging the overall usage of this screen. Does not log the user or current time.
                        LoginActivity.logMessageToAnalytics(TAG + n, application);

                        // Log = [Ricardo Edgard Caceffo][Thu Nov 17 14:36:22 GMT-02:00 2016][LoginActivity][Play Play Madrid mp1]
                        String s = LoginActivity.generateLogMessage(TAG,n);
                        LoginActivity.logMessageToAnalytics(s, application);
                        // [Ricardo] --------------- Logging activities ---------------------

                        mp1.start();
                    }
                });

                final MediaPlayer mp2 = MediaPlayer.create(this, R.raw.vocabulariogarganta);
                next2.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View view) {

                        // [Ricardo] --------------- Logging activities ---------------------
                        String n = "[1] Play Gato mp2";
                        Log.d(TAG, "[1] " + n);
                        AnalyticsApplication application = (AnalyticsApplication) getApplication();

                        // Log = ThisClass
                        // Logging the overall usage of this screen. Does not log the user or current time.
                        LoginActivity.logMessageToAnalytics(TAG + n, application);

                        // Log = [Ricardo Edgard Caceffo][Thu Nov 17 14:36:22 GMT-02:00 2016][LoginActivity][Play Play Madrid mp1]
                        String s = LoginActivity.generateLogMessage(TAG,n);
                        LoginActivity.logMessageToAnalytics(s, application);
                        // [Ricardo] --------------- Logging activities ---------------------

                        mp2.start();
                    }
                });

                final MediaPlayer mp3 = MediaPlayer.create(this, R.raw.vocabulariogota);
                next3.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View view) {


                        // [Ricardo] --------------- Logging activities ---------------------
                        String n = "[1] Play Gato mp3";
                        Log.d(TAG, "[1] " + n);
                        AnalyticsApplication application = (AnalyticsApplication) getApplication();

                        // Log = ThisClass
                        // Logging the overall usage of this screen. Does not log the user or current time.
                        LoginActivity.logMessageToAnalytics(TAG + n, application);

                        // Log = [Ricardo Edgard Caceffo][Thu Nov 17 14:36:22 GMT-02:00 2016][LoginActivity][Play Play Madrid mp1]
                        String s = LoginActivity.generateLogMessage(TAG,n);
                        LoginActivity.logMessageToAnalytics(s, application);
                        // [Ricardo] --------------- Logging activities ---------------------

                        mp3.start();
                    }
                });

                final MediaPlayer mp4 = MediaPlayer.create(this, R.raw.vocabulariogusto);
                next4.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View view) {


                        // [Ricardo] --------------- Logging activities ---------------------
                        String n = "[1] Play Gato mp4";
                        Log.d(TAG, "[1] " + n);
                        AnalyticsApplication application = (AnalyticsApplication) getApplication();

                        // Log = ThisClass
                        // Logging the overall usage of this screen. Does not log the user or current time.
                        LoginActivity.logMessageToAnalytics(TAG + n, application);

                        // Log = [Ricardo Edgard Caceffo][Thu Nov 17 14:36:22 GMT-02:00 2016][LoginActivity][Play Play Madrid mp1]
                        String s = LoginActivity.generateLogMessage(TAG,n);
                        LoginActivity.logMessageToAnalytics(s, application);
                        // [Ricardo] --------------- Logging activities ---------------------

                        mp4.start();
                    }
                });

                textView1.setText("La GE, delante de A, O, U se pronuncia de forma gutural.");
                textView3.setText("gato /d/");
                textView4.setText("garganta");
                textView7.setText("gota");
                textView8.setText("gusto");

            } else if (value.equals("vaca")) {
                palabra = 7;
                final MediaPlayer mp1 = MediaPlayer.create(this, R.raw.vocabulariovaca);
                next1.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View view) {

                        // [Ricardo] --------------- Logging activities ---------------------
                        String n = "[1] Play Vaca mp1";
                        Log.d(TAG, "[1] " + n);
                        AnalyticsApplication application = (AnalyticsApplication) getApplication();

                        // Log = ThisClass
                        // Logging the overall usage of this screen. Does not log the user or current time.
                        LoginActivity.logMessageToAnalytics(TAG + n, application);

                        // Log = [Ricardo Edgard Caceffo][Thu Nov 17 14:36:22 GMT-02:00 2016][LoginActivity][Play Play Madrid mp1]
                        String s = LoginActivity.generateLogMessage(TAG,n);
                        LoginActivity.logMessageToAnalytics(s, application);
                        // [Ricardo] --------------- Logging activities ---------------------


                        mp1.start();
                    }
                });

                final MediaPlayer mp2 = MediaPlayer.create(this, R.raw.vocabulariovecino);
                next2.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View view) {

                        // [Ricardo] --------------- Logging activities ---------------------
                        String n = "[1] Play Vaca mp2";
                        Log.d(TAG, "[1] " + n);
                        AnalyticsApplication application = (AnalyticsApplication) getApplication();

                        // Log = ThisClass
                        // Logging the overall usage of this screen. Does not log the user or current time.
                        LoginActivity.logMessageToAnalytics(TAG + n, application);

                        // Log = [Ricardo Edgard Caceffo][Thu Nov 17 14:36:22 GMT-02:00 2016][LoginActivity][Play Play Madrid mp1]
                        String s = LoginActivity.generateLogMessage(TAG,n);
                        LoginActivity.logMessageToAnalytics(s, application);
                        // [Ricardo] --------------- Logging activities ---------------------


                        mp2.start();
                    }
                });

                final MediaPlayer mp3 = MediaPlayer.create(this, R.raw.vocabularioverano);
                next3.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View view) {

                        // [Ricardo] --------------- Logging activities ---------------------
                        String n = "[1] Play Vaca mp3";
                        Log.d(TAG, "[1] " + n);
                        AnalyticsApplication application = (AnalyticsApplication) getApplication();

                        // Log = ThisClass
                        // Logging the overall usage of this screen. Does not log the user or current time.
                        LoginActivity.logMessageToAnalytics(TAG + n, application);

                        // Log = [Ricardo Edgard Caceffo][Thu Nov 17 14:36:22 GMT-02:00 2016][LoginActivity][Play Play Madrid mp1]
                        String s = LoginActivity.generateLogMessage(TAG,n);
                        LoginActivity.logMessageToAnalytics(s, application);
                        // [Ricardo] --------------- Logging activities ---------------------

                        mp3.start();
                    }
                });

                final MediaPlayer mp4 = MediaPlayer.create(this, R.raw.vocabulariovecindario);
                next4.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View view) {

                        // [Ricardo] --------------- Logging activities ---------------------
                        String n = "[1] Play Vaca mp4";
                        Log.d(TAG, "[1] " + n);
                        AnalyticsApplication application = (AnalyticsApplication) getApplication();

                        // Log = ThisClass
                        // Logging the overall usage of this screen. Does not log the user or current time.
                        LoginActivity.logMessageToAnalytics(TAG + n, application);

                        // Log = [Ricardo Edgard Caceffo][Thu Nov 17 14:36:22 GMT-02:00 2016][LoginActivity][Play Play Madrid mp1]
                        String s = LoginActivity.generateLogMessage(TAG,n);
                        LoginActivity.logMessageToAnalytics(s, application);
                        // [Ricardo] --------------- Logging activities ---------------------


                        mp4.start();
                    }
                });

                textView1.setText("La UVE siempre se pronuncia como una B.");
                textView3.setText("vaca /v/");
                textView4.setText("vecino");
                textView7.setText("verano");
                textView8.setText("vecindario");

            } else if (value.equals("raton")) {
                palabra = 8;
                final MediaPlayer mp1 = MediaPlayer.create(this, R.raw.vocabularioraton);
                next1.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View view) {

                        // [Ricardo] --------------- Logging activities ---------------------
                        String n = "[1] Play Raton mp1";
                        Log.d(TAG, "[1] " + n);
                        AnalyticsApplication application = (AnalyticsApplication) getApplication();

                        // Log = ThisClass
                        // Logging the overall usage of this screen. Does not log the user or current time.
                        LoginActivity.logMessageToAnalytics(TAG + n, application);

                        // Log = [Ricardo Edgard Caceffo][Thu Nov 17 14:36:22 GMT-02:00 2016][LoginActivity][Play Play Madrid mp1]
                        String s = LoginActivity.generateLogMessage(TAG,n);
                        LoginActivity.logMessageToAnalytics(s, application);
                        // [Ricardo] --------------- Logging activities ---------------------


                        mp1.start();
                    }
                });

                final MediaPlayer mp2 = MediaPlayer.create(this, R.raw.vocabularioresumen);
                next2.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View view) {


                        // [Ricardo] --------------- Logging activities ---------------------
                        String n = "[1] Play Raton mp2";
                        Log.d(TAG, "[1] " + n);
                        AnalyticsApplication application = (AnalyticsApplication) getApplication();

                        // Log = ThisClass
                        // Logging the overall usage of this screen. Does not log the user or current time.
                        LoginActivity.logMessageToAnalytics(TAG + n, application);

                        // Log = [Ricardo Edgard Caceffo][Thu Nov 17 14:36:22 GMT-02:00 2016][LoginActivity][Play Play Madrid mp1]
                        String s = LoginActivity.generateLogMessage(TAG,n);
                        LoginActivity.logMessageToAnalytics(s, application);
                        // [Ricardo] --------------- Logging activities ---------------------

                        mp2.start();
                    }
                });

                final MediaPlayer mp3 = MediaPlayer.create(this, R.raw.vocabulariorealizar);
                next3.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View view) {

                        // [Ricardo] --------------- Logging activities ---------------------
                        String n = "[1] Play Raton mp3";
                        Log.d(TAG, "[1] " + n);
                        AnalyticsApplication application = (AnalyticsApplication) getApplication();

                        // Log = ThisClass
                        // Logging the overall usage of this screen. Does not log the user or current time.
                        LoginActivity.logMessageToAnalytics(TAG + n, application);

                        // Log = [Ricardo Edgard Caceffo][Thu Nov 17 14:36:22 GMT-02:00 2016][LoginActivity][Play Play Madrid mp1]
                        String s = LoginActivity.generateLogMessage(TAG,n);
                        LoginActivity.logMessageToAnalytics(s, application);
                        // [Ricardo] --------------- Logging activities ---------------------

                        mp3.start();
                    }
                });

                final MediaPlayer mp4 = MediaPlayer.create(this, R.raw.vocabulariorincon);
                next4.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View view) {

                        // [Ricardo] --------------- Logging activities ---------------------
                        String n = "[1] Play Raton mp4";
                        Log.d(TAG, "[1] " + n);
                        AnalyticsApplication application = (AnalyticsApplication) getApplication();

                        // Log = ThisClass
                        // Logging the overall usage of this screen. Does not log the user or current time.
                        LoginActivity.logMessageToAnalytics(TAG + n, application);

                        // Log = [Ricardo Edgard Caceffo][Thu Nov 17 14:36:22 GMT-02:00 2016][LoginActivity][Play Play Madrid mp1]
                        String s = LoginActivity.generateLogMessage(TAG,n);
                        LoginActivity.logMessageToAnalytics(s, application);
                        // [Ricardo] --------------- Logging activities ---------------------

                        mp4.start();
                    }
                });

                textView1.setText("En el início de la palabra, la ERE tiene pronúncia vibrante.");
                textView3.setText("ratón /r/");
                textView4.setText("resumen");
                textView7.setText("realizar");
                textView8.setText("rincón");

            } else if (value.equals("hoy")) {
                palabra = 9;
                final MediaPlayer mp1 = MediaPlayer.create(this, R.raw.vocabulariohoy);
                next1.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View view) {


                        // [Ricardo] --------------- Logging activities ---------------------
                        String n = "[1] Play Hoy mp1";
                        Log.d(TAG, "[1] " + n);
                        AnalyticsApplication application = (AnalyticsApplication) getApplication();

                        // Log = ThisClass
                        // Logging the overall usage of this screen. Does not log the user or current time.
                        LoginActivity.logMessageToAnalytics(TAG + n, application);

                        // Log = [Ricardo Edgard Caceffo][Thu Nov 17 14:36:22 GMT-02:00 2016][LoginActivity][Play Play Madrid mp1]
                        String s = LoginActivity.generateLogMessage(TAG,n);
                        LoginActivity.logMessageToAnalytics(s, application);
                        // [Ricardo] --------------- Logging activities ---------------------

                        mp1.start();
                    }
                });

                final MediaPlayer mp2 = MediaPlayer.create(this, R.raw.vocabulariohuir);
                next2.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View view) {

                        // [Ricardo] --------------- Logging activities ---------------------
                        String n = "[1] Play Hoy mp2";
                        Log.d(TAG, "[1] " + n);
                        AnalyticsApplication application = (AnalyticsApplication) getApplication();

                        // Log = ThisClass
                        // Logging the overall usage of this screen. Does not log the user or current time.
                        LoginActivity.logMessageToAnalytics(TAG + n, application);

                        // Log = [Ricardo Edgard Caceffo][Thu Nov 17 14:36:22 GMT-02:00 2016][LoginActivity][Play Play Madrid mp1]
                        String s = LoginActivity.generateLogMessage(TAG,n);
                        LoginActivity.logMessageToAnalytics(s, application);
                        // [Ricardo] --------------- Logging activities ---------------------

                        mp2.start();
                    }
                });

                final MediaPlayer mp3 = MediaPlayer.create(this, R.raw.vocabulariohacer);
                next3.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View view) {

                        // [Ricardo] --------------- Logging activities ---------------------
                        String n = "[1] Play Hoy mp3";
                        Log.d(TAG, "[1] " + n);
                        AnalyticsApplication application = (AnalyticsApplication) getApplication();

                        // Log = ThisClass
                        // Logging the overall usage of this screen. Does not log the user or current time.
                        LoginActivity.logMessageToAnalytics(TAG + n, application);

                        // Log = [Ricardo Edgard Caceffo][Thu Nov 17 14:36:22 GMT-02:00 2016][LoginActivity][Play Play Madrid mp1]
                        String s = LoginActivity.generateLogMessage(TAG,n);
                        LoginActivity.logMessageToAnalytics(s, application);
                        // [Ricardo] --------------- Logging activities ---------------------

                        mp3.start();
                    }
                });

                final MediaPlayer mp4 = MediaPlayer.create(this, R.raw.vocabulariohablar);
                next4.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View view) {

                        // [Ricardo] --------------- Logging activities ---------------------
                        String n = "[1] Play Hoy mp4";
                        Log.d(TAG, "[1] " + n);
                        AnalyticsApplication application = (AnalyticsApplication) getApplication();

                        // Log = ThisClass
                        // Logging the overall usage of this screen. Does not log the user or current time.
                        LoginActivity.logMessageToAnalytics(TAG + n, application);

                        // Log = [Ricardo Edgard Caceffo][Thu Nov 17 14:36:22 GMT-02:00 2016][LoginActivity][Play Play Madrid mp1]
                        String s = LoginActivity.generateLogMessage(TAG,n);
                        LoginActivity.logMessageToAnalytics(s, application);
                        // [Ricardo] --------------- Logging activities ---------------------

                        mp4.start();
                    }
                });

                textView1.setText("La HACHE, en general, tiene el sonido aspirado o ausente.");
                textView3.setText("hoy /h/");
                textView4.setText("huir");
                textView7.setText("hacer");
                textView8.setText("hablar");

            } else if (value.equals("lluvia")) {
                palabra = 10;
                final MediaPlayer mp1 = MediaPlayer.create(this, R.raw.vocabulariolluvia);
                next1.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View view) {


                        // [Ricardo] --------------- Logging activities ---------------------
                        String n = "[1] Play Iluvia mp1";
                        Log.d(TAG, "[1] " + n);
                        AnalyticsApplication application = (AnalyticsApplication) getApplication();

                        // Log = ThisClass
                        // Logging the overall usage of this screen. Does not log the user or current time.
                        LoginActivity.logMessageToAnalytics(TAG + n, application);

                        // Log = [Ricardo Edgard Caceffo][Thu Nov 17 14:36:22 GMT-02:00 2016][LoginActivity][Play Play Madrid mp1]
                        String s = LoginActivity.generateLogMessage(TAG,n);
                        LoginActivity.logMessageToAnalytics(s, application);
                        // [Ricardo] --------------- Logging activities ---------------------

                        mp1.start();
                    }
                });

                final MediaPlayer mp2 = MediaPlayer.create(this, R.raw.vocabulariosilla);
                next2.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View view) {


                        // [Ricardo] --------------- Logging activities ---------------------
                        String n = "[1] Play Iluvia mp2";
                        Log.d(TAG, "[1] " + n);
                        AnalyticsApplication application = (AnalyticsApplication) getApplication();

                        // Log = ThisClass
                        // Logging the overall usage of this screen. Does not log the user or current time.
                        LoginActivity.logMessageToAnalytics(TAG + n, application);

                        // Log = [Ricardo Edgard Caceffo][Thu Nov 17 14:36:22 GMT-02:00 2016][LoginActivity][Play Play Madrid mp1]
                        String s = LoginActivity.generateLogMessage(TAG,n);
                        LoginActivity.logMessageToAnalytics(s, application);
                        // [Ricardo] --------------- Logging activities ---------------------

                        mp2.start();
                    }
                });

                final MediaPlayer mp3 = MediaPlayer.create(this, R.raw.vocabulariollegar);
                next3.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View view) {

                        // [Ricardo] --------------- Logging activities ---------------------
                        String n = "[1] Play Iluvia mp3";
                        Log.d(TAG, "[1] " + n);
                        AnalyticsApplication application = (AnalyticsApplication) getApplication();

                        // Log = ThisClass
                        // Logging the overall usage of this screen. Does not log the user or current time.
                        LoginActivity.logMessageToAnalytics(TAG + n, application);

                        // Log = [Ricardo Edgard Caceffo][Thu Nov 17 14:36:22 GMT-02:00 2016][LoginActivity][Play Play Madrid mp1]
                        String s = LoginActivity.generateLogMessage(TAG, n);
                        LoginActivity.logMessageToAnalytics(s, application);
                        // [Ricardo] --------------- Logging activities ---------------------

                        mp3.start();
                    }
                });

                final MediaPlayer mp4 = MediaPlayer.create(this, R.raw.vocabulariodesarrollo);
                next4.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View view) {

                        // [Ricardo] --------------- Logging activities ---------------------
                        String n = "[1] Play Iluvia mp4";
                        Log.d(TAG, "[1] " + n);
                        AnalyticsApplication application = (AnalyticsApplication) getApplication();

                        // Log = ThisClass
                        // Logging the overall usage of this screen. Does not log the user or current time.
                        LoginActivity.logMessageToAnalytics(TAG + n, application);

                        // Log = [Ricardo Edgard Caceffo][Thu Nov 17 14:36:22 GMT-02:00 2016][LoginActivity][Play Play Madrid mp1]
                        String s = LoginActivity.generateLogMessage(TAG,n);
                        LoginActivity.logMessageToAnalytics(s, application);
                        // [Ricardo] --------------- Logging activities ---------------------

                        mp4.start();
                    }
                });

                textView1.setText("La ELLE está formada por las consonantes agrupadas LL. Es posible pronunciar de diferentes formas, como: DJ; LH");
                textView3.setText("lluvia /ll/");
                textView4.setText("silla");
                textView7.setText("llegar");
                textView8.setText("desarrollo");

            } else if (value.equals("carretera")) {
                palabra = 11;
                final MediaPlayer mp1 = MediaPlayer.create(this, R.raw.vocabulariocarretera);
                next1.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View view) {

                        // [Ricardo] --------------- Logging activities ---------------------
                        String n = "[1] Play Carretera mp1";
                        Log.d(TAG, "[1] " + n);
                        AnalyticsApplication application = (AnalyticsApplication) getApplication();

                        // Log = ThisClass
                        // Logging the overall usage of this screen. Does not log the user or current time.
                        LoginActivity.logMessageToAnalytics(TAG + n, application);

                        // Log = [Ricardo Edgard Caceffo][Thu Nov 17 14:36:22 GMT-02:00 2016][LoginActivity][Play Play Madrid mp1]
                        String s = LoginActivity.generateLogMessage(TAG,n);
                        LoginActivity.logMessageToAnalytics(s, application);
                        // [Ricardo] --------------- Logging activities ---------------------


                        mp1.start();
                    }
                });

                final MediaPlayer mp2 = MediaPlayer.create(this, R.raw.vocabulariocarrera);
                next2.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View view) {

                        // [Ricardo] --------------- Logging activities ---------------------
                        String n = "[1] Play Carretera mp2";
                        Log.d(TAG, "[1] " + n);
                        AnalyticsApplication application = (AnalyticsApplication) getApplication();

                        // Log = ThisClass
                        // Logging the overall usage of this screen. Does not log the user or current time.
                        LoginActivity.logMessageToAnalytics(TAG + n, application);

                        // Log = [Ricardo Edgard Caceffo][Thu Nov 17 14:36:22 GMT-02:00 2016][LoginActivity][Play Play Madrid mp1]
                        String s = LoginActivity.generateLogMessage(TAG,n);
                        LoginActivity.logMessageToAnalytics(s, application);
                        // [Ricardo] --------------- Logging activities ---------------------
                        mp2.start();
                    }
                });

                final MediaPlayer mp3 = MediaPlayer.create(this, R.raw.vocabulariocorrer);
                next3.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View view) {


                        // [Ricardo] --------------- Logging activities ---------------------
                        String n = "[1] Play Carretera mp3";
                        Log.d(TAG, "[1] " + n);
                        AnalyticsApplication application = (AnalyticsApplication) getApplication();

                        // Log = ThisClass
                        // Logging the overall usage of this screen. Does not log the user or current time.
                        LoginActivity.logMessageToAnalytics(TAG + n, application);

                        // Log = [Ricardo Edgard Caceffo][Thu Nov 17 14:36:22 GMT-02:00 2016][LoginActivity][Play Play Madrid mp1]
                        String s = LoginActivity.generateLogMessage(TAG,n);
                        LoginActivity.logMessageToAnalytics(s, application);
                        // [Ricardo] --------------- Logging activities ---------------------

                        mp3.start();
                    }
                });

                final MediaPlayer mp4 = MediaPlayer.create(this, R.raw.vocabularioarroz);
                next4.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View view) {


                        // [Ricardo] --------------- Logging activities ---------------------
                        String n = "[1] Play Carretera mp4";
                        Log.d(TAG, "[1] " + n);
                        AnalyticsApplication application = (AnalyticsApplication) getApplication();

                        // Log = ThisClass
                        // Logging the overall usage of this screen. Does not log the user or current time.
                        LoginActivity.logMessageToAnalytics(TAG + n, application);

                        // Log = [Ricardo Edgard Caceffo][Thu Nov 17 14:36:22 GMT-02:00 2016][LoginActivity][Play Play Madrid mp1]
                        String s = LoginActivity.generateLogMessage(TAG,n);
                        LoginActivity.logMessageToAnalytics(s, application);
                        // [Ricardo] --------------- Logging activities ---------------------

                        mp4.start();
                    }
                });

                textView1.setText("La ERRE está formada por las consonantes agrupadas RR. Su pronúncia es siempre vibrante. Siempre está en el medio de la palabra, nunca en el inicio.\n");
                textView3.setText("carretera /rr/");
                textView4.setText("carrera");
                textView7.setText("correr");
                textView8.setText("arroz");

            } else if (value.equals("nino")) {
                palabra = 12;
                final MediaPlayer mp1 = MediaPlayer.create(this, R.raw.vocabularionino);
                next1.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View view) {

                        // [Ricardo] --------------- Logging activities ---------------------
                        String n = "[1] Play Nino mp1";
                        Log.d(TAG, "[1] " + n);
                        AnalyticsApplication application = (AnalyticsApplication) getApplication();

                        // Log = ThisClass
                        // Logging the overall usage of this screen. Does not log the user or current time.
                        LoginActivity.logMessageToAnalytics(TAG + n, application);

                        // Log = [Ricardo Edgard Caceffo][Thu Nov 17 14:36:22 GMT-02:00 2016][LoginActivity][Play Play Madrid mp1]
                        String s = LoginActivity.generateLogMessage(TAG,n);
                        LoginActivity.logMessageToAnalytics(s, application);
                        // [Ricardo] --------------- Logging activities ---------------------

                        mp1.start();
                    }
                });

                final MediaPlayer mp2 = MediaPlayer.create(this, R.raw.vocabularioanorar);
                next2.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View view) {


                        // [Ricardo] --------------- Logging activities ---------------------
                        String n = "[1] Play Nino mp2";
                        Log.d(TAG, "[1] " + n);
                        AnalyticsApplication application = (AnalyticsApplication) getApplication();

                        // Log = ThisClass
                        // Logging the overall usage of this screen. Does not log the user or current time.
                        LoginActivity.logMessageToAnalytics(TAG + n, application);

                        // Log = [Ricardo Edgard Caceffo][Thu Nov 17 14:36:22 GMT-02:00 2016][LoginActivity][Play Play Madrid mp1]
                        String s = LoginActivity.generateLogMessage(TAG,n);
                        LoginActivity.logMessageToAnalytics(s, application);
                        // [Ricardo] --------------- Logging activities ---------------------

                        mp2.start();
                    }
                });

                final MediaPlayer mp3 = MediaPlayer.create(this, R.raw.vocabularioextranar);
                next3.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View view) {

                        // [Ricardo] --------------- Logging activities ---------------------
                        String n = "[1] Play Nino mp3";
                        Log.d(TAG, "[1] " + n);
                        AnalyticsApplication application = (AnalyticsApplication) getApplication();

                        // Log = ThisClass
                        // Logging the overall usage of this screen. Does not log the user or current time.
                        LoginActivity.logMessageToAnalytics(TAG + n, application);

                        // Log = [Ricardo Edgard Caceffo][Thu Nov 17 14:36:22 GMT-02:00 2016][LoginActivity][Play Play Madrid mp1]
                        String s = LoginActivity.generateLogMessage(TAG,n);
                        LoginActivity.logMessageToAnalytics(s, application);
                        // [Ricardo] --------------- Logging activities ---------------------


                        mp3.start();
                    }
                });

                final MediaPlayer mp4 = MediaPlayer.create(this, R.raw.vocabulariomanana);
                next4.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View view) {


                        // [Ricardo] --------------- Logging activities ---------------------
                        String n = "[1] Play Nino mp4";
                        Log.d(TAG, "[1] " + n);
                        AnalyticsApplication application = (AnalyticsApplication) getApplication();

                        // Log = ThisClass
                        // Logging the overall usage of this screen. Does not log the user or current time.
                        LoginActivity.logMessageToAnalytics(TAG + n, application);

                        // Log = [Ricardo Edgard Caceffo][Thu Nov 17 14:36:22 GMT-02:00 2016][LoginActivity][Play Play Madrid mp1]
                        String s = LoginActivity.generateLogMessage(TAG,n);
                        LoginActivity.logMessageToAnalytics(s, application);
                        // [Ricardo] --------------- Logging activities ---------------------

                        mp4.start();
                    }
                });

                textView1.setText("La EÑE se pronuncia como NH.");
                textView3.setText("nino /ñ/");
                textView4.setText("añorar");
                textView7.setText("extrañar");
                textView8.setText("mañana");

            }
            //The key argument here must match that used in the other activity
        }

      /*  final MediaPlayer mp1 = MediaPlayer.create(this, R.raw.laartigo);
        next1.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                mp1.start();
            }
        });

        final MediaPlayer mp2 = MediaPlayer.create(this, R.raw.laexperiencia);
        next2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                mp2.start();
            }
        });

        final MediaPlayer mp3 = MediaPlayer.create(this, R.raw.launiversidad);
        next3.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                mp3.start();
            }
        });

        final MediaPlayer mp4 = MediaPlayer.create(this, R.raw.lagente);
        next4.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                mp4.start();
            }
        }); */

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Sopa de Letras");
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        RelativeLayout relativeLayout = (RelativeLayout)findViewById(R.id.argentina);
        relativeLayout.setOnTouchListener(
                new RelativeLayout.OnTouchListener() {

                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        switch (event.getAction() & MotionEvent.ACTION_MASK) {
                            case MotionEvent.ACTION_DOWN:
                                // This happens when you touch the screen with two fingers
                                mode = SWIPE;
                                // You can also use event.getY(1) or the average of the two
                                startX = event.getX(0);
                                break;

                            case MotionEvent.ACTION_UP:
                                // This happens when you release the second finger
                                mode = NONE;
                                if (Math.abs(startX - stopX) > TRESHOLD) {
                                    if (startX > stopX) {
                                        Intent intent = new Intent(v.getContext(), SopaDeLetras2Activity.class);
                                        startActivity(intent);
                                    } else {
                                        Intent intent = new Intent(v.getContext(), SopaDeLetras2Activity.class);
                                        startActivity(intent);
                                    }
                                }
                                mode = NONE;
                                break;

                            case MotionEvent.ACTION_MOVE:
                                if (mode == SWIPE) {
                                    stopX = event.getX(0);
                                }
                                break;
                        }

                        return true;
                    }
                });

    }

    @Override
    public boolean onCreateOptionsMenu (Menu menu) {

        // [Ricardo] --------------- Logging activities ---------------------
        Log.d(TAG, "[1] onCreateOptionsMenu");
        AnalyticsApplication application = (AnalyticsApplication) getApplication();

        // Log = ThisClass
        // Logging the overall usage of this screen. Does not log the user or current time.
        LoginActivity.logMessageToAnalytics(TAG + "- onCreateOptionsMenu", application);

        // Log = [Ricardo Edgard Caceffo][Thu Nov 17 14:36:22 GMT-02:00 2016][LoginActivity][onCreateOptionsMenu]
        String s = LoginActivity.generateLogMessage(TAG,"onCreateOptionsMenu");
        LoginActivity.logMessageToAnalytics(s, application);
        // [Ricardo] --------------- Logging activities ---------------------


        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.drawer, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {


        // [Ricardo] --------------- Logging activities ---------------------
        Log.d(TAG, "[1] onOptionsItemSelected");
        AnalyticsApplication application = (AnalyticsApplication) getApplication();

        // Log = ThisClass
        // Logging the overall usage of this screen. Does not log the user or current time.
        LoginActivity.logMessageToAnalytics(TAG + "- onOptionsItemSelected  [item.getItemId() = " + item.getItemId() + "]", application);

        // Log = [Ricardo Edgard Caceffo][Thu Nov 17 14:36:22 GMT-02:00 2016][LoginActivity][onOptionsItemSelected]
        String s = LoginActivity.generateLogMessage(TAG,"onOptionsItemSelected [item.getItemId()= " + item.getItemId() + "]");
        LoginActivity.logMessageToAnalytics(s, application);
        // [Ricardo] --------------- Logging activities ---------------------

        switch (item.getItemId()) {
            case R.id.traduccion:
                final AlertDialog alertDialog = new AlertDialog.Builder(this).create();
                if (palabra == 1) {
                    alertDialog.setTitle("Traducción");
                    alertDialog.setMessage("Argentina. A letra GE, diante de E e I, tem um som aspirado." + "\n\n"
                            + "Outras palavras com o mesmo som:" + "\n" +
                            "gente" + "\n" + "girar" + "\n" + "gema");
                    alertDialog.setCanceledOnTouchOutside(true);
                    alertDialog.show();
                } else if (palabra == 2) {
                    alertDialog.setTitle("Traducción");
                    alertDialog.setMessage("Madri. A letra DE, no final da palavra, se pronuncia de forma suave e não tem vogal diante dela." + "\n\n"
                            + "Outras palavras com o mesmo som:" + "\n" +
                            "novidade" + "\n" + "cidade" + "\n" + "universidade");
                    alertDialog.setCanceledOnTouchOutside(true);
                    alertDialog.show();
                } else if (palabra == 3) {
                    alertDialog.setTitle("Traducción");
                    alertDialog.setMessage("Sapato. A letra Z, na Espanha, se pronuncia de forma interdental. Na América Latina, se pronuncia como uma S." + "\n\n"
                            + "Outras palavras com o mesmo som:" + "\n" +
                            "aeromoça" + "\n" + "razão" + "\n" + "caçador");
                    alertDialog.setCanceledOnTouchOutside(true);
                    alertDialog.show();
                } else if (palabra == 4) {
                    alertDialog.setTitle("Traducción");
                    alertDialog.setMessage("México. A letra X se pronuncia de duas formas: R; KS\n" +
                            "\n" +
                            "Outras palavras com o mesmo som:\n" +
                            "estrangeiro\n" +
                            "experiência\n" +
                            "exposição");
                    alertDialog.setCanceledOnTouchOutside(true);
                    alertDialog.show();
                } else if (palabra == 5) {
                    alertDialog.setTitle("Traducción");
                    alertDialog.setMessage("Carro. A CH é um dígrafo formado pelo agrupamento das consoantes C e H. Se pronuncia como TCH." + "\n\n"
                            + "Outras palavras com o mesmo som:" + "\n" +
                            "muito" + "\n" + "noite" + "\n" + "oito");
                    alertDialog.setCanceledOnTouchOutside(true);
                    alertDialog.show();
                } else if (palabra == 6) {
                    alertDialog.setTitle("Traducción");
                    alertDialog.setMessage("Gato. A letra GE diante de A, O, U, se pronuncia de forma agotal."+ "\n\n"
                            + "Outras palavras com o mesmo som:" + "\n" +
                            "gota" + "\n" + "garganta" + "\n" + "gosto");
                    alertDialog.setCanceledOnTouchOutside(true);
                    alertDialog.show();
                } else if (palabra == 7) {
                    alertDialog.setTitle("Traducción");
                    alertDialog.setMessage("Vaca. A letra V sempre se pronuncia como uma B" + "\n\n"
                            + "Outras palavras com o mesmo som:" + "\n" +
                            "vizinho" + "\n" + "verão" + "\n" + "vizinhança");
                    alertDialog.setCanceledOnTouchOutside(true);
                    alertDialog.show();
                } else if (palabra == 8) {
                    alertDialog.setTitle("Traducción");
                    alertDialog.setMessage("Rato. No início da palavra, a R tem som vibrante." + "\n\n"
                            + "Outras palavras com o mesmo som:" + "\n" +
                            "resumo" + "\n" + "realizar" + "\n" + "lugar");
                    alertDialog.setCanceledOnTouchOutside(true);
                    alertDialog.show();
                } else if (palabra == 9) {
                    alertDialog.setTitle("Traducción");
                    alertDialog.setMessage("Hoje. A letra H, em geral, tem som aspirado ou ausente." + "\n\n"
                            + "Outras palavras com o mesmo som:" + "\n" +
                            "fugir" + "\n" + "fazer" + "\n" + "falar");
                    alertDialog.setCanceledOnTouchOutside(true);
                    alertDialog.show();
                } else if (palabra == 10) {
                    alertDialog.setTitle("Traducción");
                    alertDialog.setMessage("Chuva. A LL está formada pelo agrupamento das consoantes L e L. É possível pronunciar de diferentes formas, como: DJ e LH." + "\n\n"
                            + "Outras palavras com o mesmo som:" + "\n" +
                            "cadeira" + "\n" + "chegar" + "\n" + "desenvolvimento");
                    alertDialog.setCanceledOnTouchOutside(true);
                    alertDialog.show();
                } else if (palabra == 11) {
                    alertDialog.setTitle("Traducción");
                    alertDialog.setMessage("Estrada. O dígrafo ERRE está formado pelo agrupamento das consoantes R e R. A sua pronúncia é sempre vibrante. Sempre está no meio da palavra, nunca no início.\n" + "\n\n"
                            + "Outras palavras com o mesmo som:" + "\n" +
                            "carreira" + "\n" + "correr" + "\n" + "arroz");
                    alertDialog.setCanceledOnTouchOutside(true);
                    alertDialog.show();
                } else if (palabra == 12) {
                    alertDialog.setTitle("Traducción");
                    alertDialog.setMessage("Criança. A letra Ñ se pronuncia como NH." + "\n\n"
                            + "Outras palavras com o mesmo som:" + "\n" +
                            "ter saudade" + "\n" + "estranhar" + "\n" + "manhã");
                    alertDialog.setCanceledOnTouchOutside(true);
                    alertDialog.show();
                }


                break;

            case R.id.drawernav_modules:
                Intent intent = new Intent(this, Menu3ModulosActivity.class);
                startActivity(intent);
                break;

            case R.id.drawernav_home:
                intent = new Intent(this, MenuModulosActivity.class);
                startActivity(intent);
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    /****************************** Google Analytics Cycle Control ********************************/

    @Override
    public void onStart() {
        super.onStart();
        Log.d(TAG, "[1] onStart");

        // [Ricardo] --------------- Logging activities ---------------------
        AnalyticsApplication application = (AnalyticsApplication) getApplication();

        // Log = ThisClass
        // Logging the overall usage of this screen. Does not log the user or current time.
        LoginActivity.logMessageToAnalytics(TAG + "- onStart", application);

        // Log = [Ricardo Edgard Caceffo][Thu Nov 17 14:36:22 GMT-02:00 2016][LoginActivity][onStart]
        String s = LoginActivity.generateLogMessage(TAG,"onStart");
        LoginActivity.logMessageToAnalytics(s, application);
        // [Ricardo] --------------- Logging activities ---------------------


    }


    @Override
    public void onResume() {
        super.onResume();
        Log.d(TAG, "[1] onResume");

        // [Ricardo] --------------- Logging activities ---------------------
        AnalyticsApplication application = (AnalyticsApplication) getApplication();

        // Log = ThisClass
        // Logging the overall usage of this screen. Does not log the user or current time.
        LoginActivity.logMessageToAnalytics(TAG + "- onResume", application);

        // Log = [Ricardo Edgard Caceffo][Thu Nov 17 14:36:22 GMT-02:00 2016][LoginActivity][onResume]
        String s = LoginActivity.generateLogMessage(TAG,"onResume");
        LoginActivity.logMessageToAnalytics(s, application);
        // [Ricardo] --------------- Logging activities ---------------------


    }


    @Override
    public void onStop() {
        super.onStop();
        Log.d(TAG, "[1] onStop");

        // [Ricardo] --------------- Logging activities ---------------------
        AnalyticsApplication application = (AnalyticsApplication) getApplication();

        // Log = ThisClass
        // Logging the overall usage of this screen. Does not log the user or current time.
        LoginActivity.logMessageToAnalytics(TAG + "- onStop", application);


        // Log = [Ricardo Edgard Caceffo][Thu Nov 17 14:36:22 GMT-02:00 2016][LoginActivity][onStop]
        String s = LoginActivity.generateLogMessage(TAG,"onStop");
        LoginActivity.logMessageToAnalytics(s, application);
        // [Ricardo] --------------- Logging activities ---------------------

    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "[1] onDestroy");

        // [Ricardo] --------------- Logging activities ---------------------
        AnalyticsApplication application = (AnalyticsApplication) getApplication();

        // Log = ThisClass
        // Logging the overall usage of this screen. Does not log the user or current time.
        LoginActivity.logMessageToAnalytics(TAG + "- onDestroy", application);

        // Log = [Ricardo Edgard Caceffo][Thu Nov 17 14:36:22 GMT-02:00 2016][LoginActivity][onDestroy]
        String s = LoginActivity.generateLogMessage(TAG,"onDestroy");
        LoginActivity.logMessageToAnalytics(s, application);
        // [Ricardo] --------------- Logging activities ---------------------


    }

    @Override
    public void onRestart() {
        super.onRestart();
        Log.d(TAG, "[1] onRestart");

        // [Ricardo] --------------- Logging activities ---------------------
        AnalyticsApplication application = (AnalyticsApplication) getApplication();

        // Log = ThisClass
        // Logging the overall usage of this screen. Does not log the user or current time.
        LoginActivity.logMessageToAnalytics(TAG + "- onRestart", application);

        // Log = [Ricardo Edgard Caceffo][Thu Nov 17 14:36:22 GMT-02:00 2016][LoginActivity][onRestart]
        String s = LoginActivity.generateLogMessage(TAG,"onRestart");
        LoginActivity.logMessageToAnalytics(s, application);
        // [Ricardo] --------------- Logging activities ---------------------


    }
    /****************************** Google Analytics Cycle Control ********************************/

}
