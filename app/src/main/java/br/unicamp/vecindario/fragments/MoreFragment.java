package br.unicamp.vecindario.fragments;

import android.os.Bundle;

import br.unicamp.vecindario.R;

public class MoreFragment extends BaseFragment {


    public static MoreFragment newInstance() {
        return new MoreFragment();
    }

    public MoreFragment() {
        // Required empty public constructor
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    protected int getTitle() {
        return R.string.fragtitle_more;
    }

    @Override
    public boolean hasCustomToolbar() {
        return true;
    }

    @Override
    protected int getLayout() {
        return R.layout.fragment_more;
    }
}