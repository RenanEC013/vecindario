package br.unicamp.vecindario.fragments;


import android.os.Bundle;
import android.os.Environment;
import android.media.MediaRecorder;
import android.media.MediaPlayer;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;
import java.io.IOException;

import br.unicamp.vecindario.R;

public class PortfolioFragment extends BaseFragment {
    private MediaRecorder recorder;
    private String outputSound = null;
    private boolean enableRecord = true;
    Button play, record;

    public static PortfolioFragment newInstance() {
        return new PortfolioFragment();
    }

    public PortfolioFragment() {
        // Required empty public constructor
    }
/*Instanciacao do gravador de som*/
    private void setRecorder(){
        recorder = new MediaRecorder();
        recorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        recorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
        recorder.setAudioEncoder(MediaRecorder.OutputFormat.AMR_NB);
        recorder.setOutputFile(outputSound);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        play = (Button)getActivity().findViewById(R.id.button_id2);
        record = (Button)getActivity().findViewById(R.id.button_id3);
        record.setEnabled(true);
        play.setEnabled(false);
        outputSound = Environment.getExternalStorageDirectory().getAbsolutePath() + "/recording.3gp";
/*Grava o som, se estiver gravando o botao de play fica desabilitado*/
        record.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) throws IllegalStateException{
                if (enableRecord == true) {
                    if(recorder==null) {
                        setRecorder();
                    }
                    if(play.isEnabled()){
                        play.setEnabled(false);
                    }
                    try {
                        recorder.prepare();
                        recorder.start();
                    } catch (IllegalStateException e) {
                        Toast.makeText(getActivity().getApplicationContext(), getString(R.string.generic_error), Toast.LENGTH_LONG).show();
                        e.printStackTrace();
                    } catch (IOException e) {
                        Toast.makeText(getActivity().getApplicationContext(), getString(R.string.generic_error), Toast.LENGTH_LONG).show();
                        e.printStackTrace();
                    }
                    enableRecord = false;
                    Button button = (Button) getActivity().findViewById(R.id.button_id3);
                    button.setText(getResources().getString(R.string.stop_button));
                    Toast.makeText(getActivity().getApplicationContext(), getString(R.string.recording_audio), Toast.LENGTH_SHORT).show();
                } else {
                    recorder.stop();
                    recorder.reset();
                    recorder.release();
                    recorder = null;
                    play.setEnabled(true);
                    enableRecord = true;
                    Button button = (Button) getActivity().findViewById(R.id.button_id3);
                    button.setText(getResources().getString(R.string.record_button));
                    Toast.makeText(getActivity().getApplicationContext(), getString(R.string.recorded_audio), Toast.LENGTH_SHORT).show();
                }
            }
        });
/*Reproduz o som, se estiver reproduzindo, o botao de gravar fica desabilitado*/
/*BUG- MULTIPLAS REPRODUCOES DE SOM- ADICIONAR STOP*/
        play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                MediaPlayer player = new MediaPlayer();
                try {
                    player.setDataSource(outputSound);
                }
                catch (IOException e){
                    Toast.makeText(getActivity().getApplicationContext(), getString(R.string.generic_error), Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                }
                try{
                    player.prepare();
                }
                catch (IOException e){
                    Toast.makeText(getActivity().getApplicationContext(), getString(R.string.generic_error), Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                }
                record.setEnabled(false);
                player.start();
                Toast.makeText(getActivity().getApplicationContext(), getString(R.string.play_audio), Toast.LENGTH_SHORT).show();
                player.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mp) {
                        record.setEnabled(true);
                    }
                });
            }
        });
    }

    @Override
    protected int getTitle() {
        return R.string.fragtitle_portfolio;
    }

    @Override
    public boolean hasCustomToolbar() {
        return true;
    }

    @Override
    protected int getLayout() {
        return R.layout.fragment_portfolio;
    }
}