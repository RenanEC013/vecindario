package br.unicamp.vecindario.fragments;

import android.os.Bundle;

import br.unicamp.vecindario.R;

public class RankingFragment extends BaseFragment {


    public static RankingFragment newInstance() {
        return new RankingFragment();
    }

    public RankingFragment() {
        // Required empty public constructor
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    protected int getTitle() {
        return R.string.fragtitle_ranking;
    }

    @Override
    public boolean hasCustomToolbar() {
        return true;
    }

    @Override
    protected int getLayout() {
        return R.layout.fragment_ranking;
    }
}