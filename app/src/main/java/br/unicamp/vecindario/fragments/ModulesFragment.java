package br.unicamp.vecindario.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import br.unicamp.vecindario.Empezar1Activity;
import br.unicamp.vecindario.R;
import br.unicamp.vecindario.VideoActivity;

public class ModulesFragment extends BaseFragment {

    public static ModulesFragment newInstance() {
        return new ModulesFragment();
    }

    public ModulesFragment() {
        // Required empty public constructor
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        /*Segue para a proxima tela, o modulo 1*/
        Button next = (Button) getActivity().findViewById(R.id.button_id1);
        next.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                /* O comando abaixo (que está comentado) vai para a tela com opções para abrir o
                podcast e o vídeo. O comando que NÃO está comentado vai diretamente para o vídeo,
                fazendo a alteração do link (VIDEO_ID) conforme o módulo selecionado
                Intent intent = new Intent(view.getContext(), ScrollActivity.class); */
                VideoActivity.VIDEO_ID = "72l21IlygX4";
                Intent intent = new Intent(view.getContext(), Empezar1Activity.class);
                startActivity(intent);
            }
        });

        Button next2 = (Button) getActivity().findViewById(R.id.button_id2);
        next2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                /* O comando abaixo (que está comentado) vai para a tela com opções para abrir o
                podcast e o vídeo. O comando que NÃO está comentado vai diretamente para o vídeo
                Intent intent = new Intent(view.getContext(), ScrollActivity.class); */
                Intent intent = new Intent(view.getContext(), VideoActivity.class);
                VideoActivity.VIDEO_ID = "cP9nzQJdfak";
                startActivity(intent);
            }
        });

        Button next3 = (Button) getActivity().findViewById(R.id.button_id3);
        next3.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                VideoActivity.VIDEO_ID = "b7UwSt04i14";
                Intent intent = new Intent(view.getContext(), VideoActivity.class);
                startActivity(intent);
            }
        });

    }

    @Override
    protected int getTitle() {
        return R.string.fragtitle_modules;
    }

    @Override
    public boolean hasCustomToolbar() {
        return true;
    }

    @Override
    protected int getLayout() {
        return R.layout.fragment_modules;
    }
}