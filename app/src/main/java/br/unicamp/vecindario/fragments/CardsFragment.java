package br.unicamp.vecindario.fragments;

import android.os.Bundle;

import br.unicamp.vecindario.R;

public class CardsFragment extends BaseFragment {


    public static CardsFragment newInstance() {
        return new CardsFragment();
    }

    public CardsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    protected int getTitle() {
        return R.string.fragtitle_cards;
    }

    @Override
    public boolean hasCustomToolbar() {
        return true;
    }

    @Override
    protected int getLayout() {
        return R.layout.fragment_cards;
    }
}