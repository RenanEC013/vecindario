package br.unicamp.vecindario;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.google.android.gms.analytics.Tracker;
import com.google.android.gms.appindexing.AppIndex;

import java.util.ArrayList;

/**
 * Created by renan on 24/07/2016.
 */
public class Accion5Activity extends AppCompatActivity {
    Toolbar toolbar;
    int opcao = 0;

    /**
     * [Ricardo] GoogleAnalytics Tracker
     * The {@link Tracker} used to record screen views.
     */
    private static final String TAG = "Accion5Activity";

    private static final int NONE = 0;
    private static final int SWIPE = 1;
    private int mode = NONE;
    private float startX;
    private float stopX;
    // We will only detect a swipe if the difference is at least 100 pixels
// Change this value to your needs
    private static final int TRESHOLD = 100;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // [Ricardo] --------------- Logging activities ---------------------
        Log.d(TAG, "[1] onCreate");
        AnalyticsApplication application = (AnalyticsApplication) getApplication();

        // Log = ThisClass
        // Logging the overall usage of this screen. Does not log the user or current time.
        LoginActivity.logMessageToAnalytics(TAG + "- onCreate", application);

        // Log = [Ricardo Edgard Caceffo][Thu Nov 17 14:36:22 GMT-02:00 2016][LoginActivity][onStart]
        String s = LoginActivity.generateLogMessage(TAG,"onCreate");
        LoginActivity.logMessageToAnalytics(s, application);
        // [Ricardo] --------------- Logging activities ---------------------

        setContentView(R.layout.activity_accion5);

        ListView pronome = (ListView) findViewById(R.id.pronome);
        ListView verbo = (ListView) findViewById(R.id.verbo);

        SeekBar seekBar = (SeekBar)findViewById(R.id.seekBar2);
        seekBar.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return true;
            }
        });

        /* Criar os ArrayLists para armazenar os verbos conjugados e os pronomes pessoais */
        ArrayList<String> pronomes = preencheDados("PRONOMBRES PERSONALES","Yo", "Tú", "Él/Ella/Usted", "Nosotros", "Vosotros", "Ellos/Ellas/Ustedes");
        ArrayList<String> verbos = preencheDados("VERBO PARTIR","PARTO", "PARTES", "PARTE", "PARTIMOS", "PARTIS", "PARTEN");

        ArrayAdapter<String> arrayAdapter1 = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, pronomes) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent){
                /// Get the Item from ListView
                View view = super.getView(position, convertView, parent);

                TextView tv = (TextView) view.findViewById(android.R.id.text1);

                // Set the text size 25 dip for ListView each item
                tv.setTextSize(TypedValue.COMPLEX_UNIT_DIP,16);

                // Return the view
                return view;
            }
        };

        ArrayAdapter<String> arrayAdapter2 = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, verbos) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent){
                /// Get the Item from ListView
                View view = super.getView(position, convertView, parent);

                TextView tv = (TextView) view.findViewById(android.R.id.text1);

                // Set the text size 25 dip for ListView each item
                tv.setTextSize(TypedValue.COMPLEX_UNIT_DIP,16);

                // Return the view
                return view;
            }
        };


        pronome.setAdapter(arrayAdapter1);
        verbo.setAdapter(arrayAdapter2);

        RelativeLayout relativeLayout = (RelativeLayout)findViewById(R.id.accion5);
        relativeLayout.setOnTouchListener(
                new RelativeLayout.OnTouchListener() {

                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        switch (event.getAction() & MotionEvent.ACTION_MASK) {
                            case MotionEvent.ACTION_DOWN:
                                // This happens when you touch the screen with two fingers
                                mode = SWIPE;
                                // You can also use event.getY(1) or the average of the two
                                startX = event.getX(0);
                                break;

                            case MotionEvent.ACTION_UP:
                                // This happens when you release the second finger
                                mode = NONE;
                                if (Math.abs(startX - stopX) > TRESHOLD) {
                                    if (startX > stopX) {
                                        Intent intent = new Intent(v.getContext(), Accion6Activity.class);
                                        startActivity(intent);
                                    } else {
                                        Intent intent = new Intent(v.getContext(), Accion4Activity.class);
                                        startActivity(intent);
                                    }
                                }
                                mode = NONE;
                                break;

                            case MotionEvent.ACTION_MOVE:
                                if (mode == SWIPE) {
                                    stopX = event.getX(0);
                                }
                                break;
                        }

                        return true;
                    }
                });

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Acción");
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

    }


    /* Função que preenche os dados do ArrayList */
    private ArrayList<String> preencheDados(String s1, String s2, String s3, String s4, String s5, String s6, String s7) {
        ArrayList<String> dados = new ArrayList<String>();
        dados.add(s1);
        dados.add(s2);
        dados.add(s3);
        dados.add(s4);
        dados.add(s5);
        dados.add(s6);
        dados.add(s7);

        return dados;
    }

    @Override
    public boolean onCreateOptionsMenu (Menu menu) {
        // [Ricardo] --------------- Logging activities ---------------------
        Log.d(TAG, "[1] onCreateOptionsMenu");
        AnalyticsApplication application = (AnalyticsApplication) getApplication();

        // Log = ThisClass
        // Logging the overall usage of this screen. Does not log the user or current time.
        LoginActivity.logMessageToAnalytics(TAG + "- onCreateOptionsMenu", application);

        // Log = [Ricardo Edgard Caceffo][Thu Nov 17 14:36:22 GMT-02:00 2016][LoginActivity][onCreateOptionsMenu]
        String s = LoginActivity.generateLogMessage(TAG,"onCreateOptionsMenu");
        LoginActivity.logMessageToAnalytics(s, application);
        // [Ricardo] --------------- Logging activities ---------------------

        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.drawer, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        // [Ricardo] --------------- Logging activities ---------------------
        Log.d(TAG, "[1] onOptionsItemSelected");
        AnalyticsApplication application = (AnalyticsApplication) getApplication();

        // Log = ThisClass
        // Logging the overall usage of this screen. Does not log the user or current time.
        LoginActivity.logMessageToAnalytics(TAG + "- onOptionsItemSelected  [item.getItemId() = " + item.getItemId() + "]", application);

        // Log = [Ricardo Edgard Caceffo][Thu Nov 17 14:36:22 GMT-02:00 2016][LoginActivity][onOptionsItemSelected]
        String s = LoginActivity.generateLogMessage(TAG,"onOptionsItemSelected [item.getItemId()= " + item.getItemId() + "]");
        LoginActivity.logMessageToAnalytics(s, application);
        // [Ricardo] --------------- Logging activities ---------------------


        switch (item.getItemId()) {
            case R.id.traduccion:
                final AlertDialog alertDialog = new AlertDialog.Builder(this).create();
                alertDialog.setTitle("Traducción");
                alertDialog.setMessage("PRONOMES PESSOAIS  /  VERBO PARTIR:" +
                        "\n\n"
                        + "Eu PARTO" +
                        "\n"
                        + "Tu PARTES" +
                        "\n"
                        + "Ele/Ela/Você PARTE" +
                        "\n"
                        + "Nós PARTIMOS" +
                        "\n"
                        + "Vós PARTIS" +
                        "\n"
                        + "Eles/Elas/Vocês PARTEM");
                alertDialog.setCanceledOnTouchOutside(true);

                alertDialog.show();

                break;

            case R.id.drawernav_modules:
                Intent intent = new Intent(this, Menu3ModulosActivity.class);
                startActivity(intent);
                break;

            case R.id.drawernav_home:
                intent = new Intent(this, MenuModulosActivity.class);
                startActivity(intent);
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    /****************************** Google Analytics Cycle Control ********************************/

    @Override
    public void onStart() {
        super.onStart();
        Log.d(TAG, "[1] onStart");

        // [Ricardo] --------------- Logging activities ---------------------
        AnalyticsApplication application = (AnalyticsApplication) getApplication();

        // Log = ThisClass
        // Logging the overall usage of this screen. Does not log the user or current time.
        LoginActivity.logMessageToAnalytics(TAG + "- onStart", application);

        // Log = [Ricardo Edgard Caceffo][Thu Nov 17 14:36:22 GMT-02:00 2016][LoginActivity][onStart]
        String s = LoginActivity.generateLogMessage(TAG,"onStart");
        LoginActivity.logMessageToAnalytics(s, application);
        // [Ricardo] --------------- Logging activities ---------------------


    }


    @Override
    public void onResume() {
        super.onResume();
        Log.d(TAG, "[1] onResume");

        // [Ricardo] --------------- Logging activities ---------------------
        AnalyticsApplication application = (AnalyticsApplication) getApplication();

        // Log = ThisClass
        // Logging the overall usage of this screen. Does not log the user or current time.
        LoginActivity.logMessageToAnalytics(TAG + "- onResume", application);

        // Log = [Ricardo Edgard Caceffo][Thu Nov 17 14:36:22 GMT-02:00 2016][LoginActivity][onResume]
        String s = LoginActivity.generateLogMessage(TAG,"onResume");
        LoginActivity.logMessageToAnalytics(s, application);
        // [Ricardo] --------------- Logging activities ---------------------


    }


    @Override
    public void onStop() {
        super.onStop();
        Log.d(TAG, "[1] onStop");

        // [Ricardo] --------------- Logging activities ---------------------
        AnalyticsApplication application = (AnalyticsApplication) getApplication();

        // Log = ThisClass
        // Logging the overall usage of this screen. Does not log the user or current time.
        LoginActivity.logMessageToAnalytics(TAG + "- onStop", application);


        // Log = [Ricardo Edgard Caceffo][Thu Nov 17 14:36:22 GMT-02:00 2016][LoginActivity][onStop]
        String s = LoginActivity.generateLogMessage(TAG,"onStop");
        LoginActivity.logMessageToAnalytics(s, application);
        // [Ricardo] --------------- Logging activities ---------------------

    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "[1] onDestroy");

        // [Ricardo] --------------- Logging activities ---------------------
        AnalyticsApplication application = (AnalyticsApplication) getApplication();

        // Log = ThisClass
        // Logging the overall usage of this screen. Does not log the user or current time.
        LoginActivity.logMessageToAnalytics(TAG + "- onDestroy", application);

        // Log = [Ricardo Edgard Caceffo][Thu Nov 17 14:36:22 GMT-02:00 2016][LoginActivity][onDestroy]
        String s = LoginActivity.generateLogMessage(TAG,"onDestroy");
        LoginActivity.logMessageToAnalytics(s, application);
        // [Ricardo] --------------- Logging activities ---------------------


    }

    @Override
    public void onRestart() {
        super.onRestart();
        Log.d(TAG, "[1] onRestart");

        // [Ricardo] --------------- Logging activities ---------------------
        AnalyticsApplication application = (AnalyticsApplication) getApplication();

        // Log = ThisClass
        // Logging the overall usage of this screen. Does not log the user or current time.
        LoginActivity.logMessageToAnalytics(TAG + "- onRestart", application);

        // Log = [Ricardo Edgard Caceffo][Thu Nov 17 14:36:22 GMT-02:00 2016][LoginActivity][onRestart]
        String s = LoginActivity.generateLogMessage(TAG,"onRestart");
        LoginActivity.logMessageToAnalytics(s, application);
        // [Ricardo] --------------- Logging activities ---------------------


    }
    /****************************** Google Analytics Cycle Control ********************************/
}

