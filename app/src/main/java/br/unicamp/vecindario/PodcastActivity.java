package br.unicamp.vecindario;

import android.app.AlertDialog;
import android.content.Intent;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.content.SharedPreferences;
import android.widget.Toast;

import android.os.Build;

import com.google.android.gms.analytics.Tracker;

import java.io.File;
import java.io.IOException;

public class PodcastActivity extends AppCompatActivity {
    private MediaRecorder recorder;
    private String outputSound = null;
    private boolean enableRecord = true;
    Button play;
    Button record;
    Button whatsapp;
    Boolean gravado = false;

    /**
     * [Ricardo] GoogleAnalytics Tracker
     * The {@link Tracker} used to record screen views.
     */
    private static final String TAG = "PodcastActivity";

    private static final int NONE = 0;
    private static final int SWIPE = 1;
    private int mode = NONE;
    private float startX;
    private float stopX;
    // We will only detect a swipe if the difference is at least 100 pixels
// Change this value to your needs
    private static final int TRESHOLD = 100;

    /*Instanciacao do gravador de som*/
    private void setRecorder(){
        recorder = new MediaRecorder();
        recorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        recorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
        recorder.setAudioEncoder(MediaRecorder.OutputFormat.AMR_NB);
        recorder.setOutputFile(outputSound);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // [Ricardo] --------------- Logging activities ---------------------
        Log.d(TAG, "[1] onCreate");
        AnalyticsApplication application = (AnalyticsApplication) getApplication();

        // Log = ThisClass
        // Logging the overall usage of this screen. Does not log the user or current time.
        LoginActivity.logMessageToAnalytics(TAG + "- onCreate", application);

        // Log = [Ricardo Edgard Caceffo][Thu Nov 17 14:36:22 GMT-02:00 2016][LoginActivity][onCreate]
        final String s = LoginActivity.generateLogMessage(TAG,"onCreate");
        LoginActivity.logMessageToAnalytics(s, application);
        // [Ricardo] --------------- Logging activities ---------------------


        setContentView(R.layout.activity_podcast);
        /*Qual o modulo que chamou o podcast*/
     //   Intent intent=getIntent();
     //   String moduleNumber=intent.getStringExtra(ScrollActivity.MODULE_NUMBER);

        //SharedPreferences sharedPref = getSharedPreferences("mypref", 0);
        //final String str =  sharedPref.getString("userName", "error_retrieving_user_name");

        final AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setTitle("Presentarse");
        alertDialog.setMessage("Usa las expresiones para hacer tu presentación personal." + "\n\n" +
                "1. Pulsa en el botón y graba tu presentación en audio." + "\n" +
                "2. Puedes repetir cuantas veces queiras." + "\n" +
                "3. Cuando terminares, pulsa el botón de WhatsApp." + "\n" +
                "4. El audio será enviado para el foro e compartido con sus compañeros.");
        alertDialog.setCanceledOnTouchOutside(true);

        alertDialog.show();

        SeekBar seekBar = (SeekBar) findViewById(R.id.seekBar2);
        seekBar.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return true;
            }
        });

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Presentarse");
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        play = (Button)findViewById(R.id.button_id1);
        record = (Button)findViewById(R.id.button_id2);
        whatsapp = (Button)findViewById(R.id.button);
        whatsapp.setEnabled(false);

        record.setEnabled(true);
        play.setEnabled(false);
        outputSound = Environment.getExternalStorageDirectory().getAbsolutePath() + "/recording.mp3";
/*Grava o som, se estiver gravando o botao de play fica desabilitado*/
        record.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) throws IllegalStateException{

                // [Ricardo] --------------- Logging activities ---------------------
                String n = "Gravando o audio";
                AnalyticsApplication application = (AnalyticsApplication) getApplication();

                // Log = ThisClass
                // Logging the overall usage of this screen. Does not log the user or current time.
                LoginActivity.logMessageToAnalytics(TAG + n, application);

                // Log = [Ricardo Edgard Caceffo][Thu Nov 17 14:36:22 GMT-02:00 2016][LoginActivity][onCreate]
                String s = LoginActivity.generateLogMessage(TAG,n);
                LoginActivity.logMessageToAnalytics(s, application);
                // [Ricardo] --------------- Logging activities ---------------------

                if (enableRecord == true) {
                    if(recorder==null) {
                        setRecorder();
                    }
                    if(play.isEnabled()){
                        play.setEnabled(false);
                    }
                    try {
                        recorder.prepare();
                        recorder.start();
                        gravado = true;
                        whatsapp.setEnabled(true);
                        whatsapp.setBackgroundResource(R.drawable.whatsapp);
                    } catch (IllegalStateException e) {
                        Toast.makeText(getApplicationContext(), getString(R.string.generic_error), Toast.LENGTH_LONG).show();
                        e.printStackTrace();
                    } catch (IOException e) {
                        Toast.makeText(getApplicationContext(), getString(R.string.generic_error), Toast.LENGTH_LONG).show();
                        e.printStackTrace();
                    }
                    enableRecord = false;
                    Button button = (Button) findViewById(R.id.button_id2);
                    //button.setText(getResources().getString(R.string.stop_button));
                    record.setBackgroundResource(R.drawable.ic_stop);
                    Toast.makeText(getApplicationContext(), getString(R.string.recording_audio), Toast.LENGTH_SHORT).show();
                } else {
                    recorder.stop();
                    recorder.reset();
                    recorder.release();
                    recorder = null;
                    play.setEnabled(true);
                    enableRecord = true;
                    record.setBackgroundResource(R.drawable.record);
                    Toast.makeText(getApplicationContext(), getString(R.string.recorded_audio), Toast.LENGTH_SHORT).show();
                }
            }
        });
/*Reproduz o som, se estiver reproduzindo, o botao de gravar fica desabilitado*/
        play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // [Ricardo] --------------- Logging activities ---------------------
                String n = "Reproduzindo o audio";
                AnalyticsApplication application = (AnalyticsApplication) getApplication();

                // Log = ThisClass
                // Logging the overall usage of this screen. Does not log the user or current time.
                LoginActivity.logMessageToAnalytics(TAG + n, application);

                // Log = [Ricardo Edgard Caceffo][Thu Nov 17 14:36:22 GMT-02:00 2016][LoginActivity][onCreate]
                String s = LoginActivity.generateLogMessage(TAG,n);
                LoginActivity.logMessageToAnalytics(s, application);
                // [Ricardo] --------------- Logging activities ---------------------

                MediaPlayer player = new MediaPlayer();
                try {
                    player.setDataSource(outputSound);
                } catch (IOException e) {
                    Toast.makeText(getApplicationContext(), getString(R.string.generic_error), Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                }
                try {
                    player.prepare();
                    player.start();
                } catch (IOException e) {
                    Toast.makeText(getApplicationContext(), getString(R.string.generic_error), Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                }
                record.setEnabled(false);
                Toast.makeText(getApplicationContext(), getString(R.string.play_audio), Toast.LENGTH_SHORT).show();
                player.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mp) {
                        record.setEnabled(true);
                    }
                });
            }
        });


        whatsapp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (gravado == true) {
                    final Intent shareIntent = new Intent(android.content.Intent.ACTION_SEND);
                    shareIntent.setType("audio/mp3");
                    shareIntent.putExtra(android.content.Intent.EXTRA_STREAM, Uri.parse("file://" + outputSound));
                    shareIntent.setPackage("com.whatsapp");
                   // shareIntent.putExtra(Intent.EXTRA_TEXT, str + ": " + getAndroidVersion());
                    startActivity(Intent.createChooser(shareIntent, "Enviar presentación"));
                }
            }
        });


        RelativeLayout relativeLayout = (RelativeLayout) findViewById(R.id.podcast);
        relativeLayout.setOnTouchListener(
                new RelativeLayout.OnTouchListener() {
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        switch (event.getAction() & MotionEvent.ACTION_MASK) {
                            case MotionEvent.ACTION_DOWN:
                                // This happens when you touch the screen with two fingers
                                mode = SWIPE;
                                // You can also use event.getY(1) or the average of the two
                                startX = event.getX(0);
                                break;

                            case MotionEvent.ACTION_UP:
                                // This happens when you release the second finger
                                mode = NONE;
                                if (Math.abs(startX - stopX) > TRESHOLD) {
                                    if (startX > stopX) {
                                        Intent intent = new Intent(v.getContext(), MenuModulosActivity.class);
                                        startActivity(intent);
                                    } else {
                                        Intent intent = new Intent(v.getContext(), Podcast1Activity.class);
                                        startActivity(intent);
                                    }
                                }
                                mode = NONE;
                                break;
                             case MotionEvent.ACTION_MOVE:
                                 if (mode == SWIPE) {
                                     stopX = event.getX(0);
                                 }
                                 break;
                        }

                        return true;
                    }
                });

        }

            @Override
            public boolean onCreateOptionsMenu(Menu menu) {

                // [Ricardo] --------------- Logging activities ---------------------
                Log.d(TAG, "[1] onCreateOptionsMenu");
                AnalyticsApplication application = (AnalyticsApplication) getApplication();

                // Log = ThisClass
                // Logging the overall usage of this screen. Does not log the user or current time.
                LoginActivity.logMessageToAnalytics(TAG + "- onCreateOptionsMenu", application);

                // Log = [Ricardo Edgard Caceffo][Thu Nov 17 14:36:22 GMT-02:00 2016][LoginActivity][onCreateOptionsMenu]
                String s = LoginActivity.generateLogMessage(TAG, "onCreateOptionsMenu");
                LoginActivity.logMessageToAnalytics(s, application);
                // [Ricardo] --------------- Logging activities ---------------------


                MenuInflater menuInflater = getMenuInflater();
                menuInflater.inflate(R.menu.drawer, menu);

                return super.onCreateOptionsMenu(menu);
            }

            @Override
            public boolean onOptionsItemSelected(MenuItem item) {

                // [Ricardo] --------------- Logging activities ---------------------
                Log.d(TAG, "[1] onOptionsItemSelected");
                AnalyticsApplication application = (AnalyticsApplication) getApplication();

                // Log = ThisClass
                // Logging the overall usage of this screen. Does not log the user or current time.
                LoginActivity.logMessageToAnalytics(TAG + "- onOptionsItemSelected  [item.getItemId() = " + item.getItemId() + "]", application);

                // Log = [Ricardo Edgard Caceffo][Thu Nov 17 14:36:22 GMT-02:00 2016][LoginActivity][onOptionsItemSelected]
                String s = LoginActivity.generateLogMessage(TAG, "onOptionsItemSelected [item.getItemId()= " + item.getItemId() + "]");
                LoginActivity.logMessageToAnalytics(s, application);
                // [Ricardo] --------------- Logging activities ---------------------

                switch (item.getItemId()) {

                    case R.id.drawernav_modules:
                        Intent intent = new Intent(this, MenuModulosActivity.class);
                        startActivity(intent);
                        break;
                }

                return super.onOptionsItemSelected(item);
            }

            /****************************** Google Analytics Cycle Control ********************************/

            @Override
            public void onStart() {
                super.onStart();
                Log.d(TAG, "[1] onStart");

                // [Ricardo] --------------- Logging activities ---------------------
                AnalyticsApplication application = (AnalyticsApplication) getApplication();

                // Log = ThisClass
                // Logging the overall usage of this screen. Does not log the user or current time.
                LoginActivity.logMessageToAnalytics(TAG + "- onStart", application);

                // Log = [Ricardo Edgard Caceffo][Thu Nov 17 14:36:22 GMT-02:00 2016][LoginActivity][onStart]
                String s = LoginActivity.generateLogMessage(TAG, "onStart");
                LoginActivity.logMessageToAnalytics(s, application);
                // [Ricardo] --------------- Logging activities ---------------------


            }


            @Override
            public void onResume() {
                super.onResume();
                Log.d(TAG, "[1] onResume");

                // [Ricardo] --------------- Logging activities ---------------------
                AnalyticsApplication application = (AnalyticsApplication) getApplication();

                // Log = ThisClass
                // Logging the overall usage of this screen. Does not log the user or current time.
                LoginActivity.logMessageToAnalytics(TAG + "- onResume", application);

                // Log = [Ricardo Edgard Caceffo][Thu Nov 17 14:36:22 GMT-02:00 2016][LoginActivity][onResume]
                String s = LoginActivity.generateLogMessage(TAG, "onResume");
                LoginActivity.logMessageToAnalytics(s, application);
                // [Ricardo] --------------- Logging activities ---------------------


            }

            public String getAndroidVersion() {
                String release = Build.VERSION.RELEASE;
                int sdkVersion = Build.VERSION.SDK_INT;
                return "Android SDK: " + sdkVersion + " (" + release +")";
            }


            @Override
            public void onStop() {
                super.onStop();
                Log.d(TAG, "[1] onStop");

                // [Ricardo] --------------- Logging activities ---------------------
                AnalyticsApplication application = (AnalyticsApplication) getApplication();

                // Log = ThisClass
                // Logging the overall usage of this screen. Does not log the user or current time.
                LoginActivity.logMessageToAnalytics(TAG + "- onStop", application);


                // Log = [Ricardo Edgard Caceffo][Thu Nov 17 14:36:22 GMT-02:00 2016][LoginActivity][onStop]
                String s = LoginActivity.generateLogMessage(TAG, "onStop");
                LoginActivity.logMessageToAnalytics(s, application);
                // [Ricardo] --------------- Logging activities ---------------------

            }


            @Override
            public void onDestroy() {
                super.onDestroy();
                Log.d(TAG, "[1] onDestroy");

                // [Ricardo] --------------- Logging activities ---------------------
                AnalyticsApplication application = (AnalyticsApplication) getApplication();

                // Log = ThisClass
                // Logging the overall usage of this screen. Does not log the user or current time.
                LoginActivity.logMessageToAnalytics(TAG + "- onDestroy", application);

                // Log = [Ricardo Edgard Caceffo][Thu Nov 17 14:36:22 GMT-02:00 2016][LoginActivity][onDestroy]
                String s = LoginActivity.generateLogMessage(TAG, "onDestroy");
                LoginActivity.logMessageToAnalytics(s, application);
                // [Ricardo] --------------- Logging activities ---------------------


            }

            @Override
            public void onRestart() {
                super.onRestart();
                Log.d(TAG, "[1] onRestart");

                // [Ricardo] --------------- Logging activities ---------------------
                AnalyticsApplication application = (AnalyticsApplication) getApplication();

                // Log = ThisClass
                // Logging the overall usage of this screen. Does not log the user or current time.
                LoginActivity.logMessageToAnalytics(TAG + "- onRestart", application);

                // Log = [Ricardo Edgard Caceffo][Thu Nov 17 14:36:22 GMT-02:00 2016][LoginActivity][onRestart]
                String s = LoginActivity.generateLogMessage(TAG, "onRestart");
                LoginActivity.logMessageToAnalytics(s, application);
                // [Ricardo] --------------- Logging activities ---------------------


            }
            /****************************** Google Analytics Cycle Control ********************************/

        }
