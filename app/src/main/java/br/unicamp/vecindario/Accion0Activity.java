package br.unicamp.vecindario;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;

/**
 * Created by renan on 19/11/2016.
 */
public class Accion0Activity extends YouTubeBaseActivity implements YouTubePlayer.OnInitializedListener {

    private static final int RQS_ErrorDialog = 1;

    public static final String API_KEY = "AIzaSyDBQ0lgcWilunoPUm8GmfnXVIlb5Jdu-hI";

    //http://youtu.be/<VIDEO_ID>
    public static String VIDEO_ID = "IqWQ568OTQY";

    private static final int NONE = 0;
    private static final int SWIPE = 1;
    private int mode = NONE;
    private float startX;
    private float stopX;
    // We will only detect a swipe if the difference is at least 100 pixels
// Change this value to your needs
    private static final int TRESHOLD = 100;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /** attaching layout xml **/
        setContentView(R.layout.activity_accion0);

        /** Initializing YouTube player view **/
        YouTubePlayerView youTubePlayerView = (YouTubePlayerView) findViewById(R.id.youtube_view);
        youTubePlayerView.initialize(API_KEY, this);

        final AlertDialog alertDialog1 = new AlertDialog.Builder(this).create();
        alertDialog1.setTitle("Acción");
        alertDialog1.setMessage("¿Los extranjeros comprenden el portugués?\n" +
                "\n" +
                "Observa los comentarios en el video:");
        alertDialog1.setCanceledOnTouchOutside(true);
        alertDialog1.show();

        final AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setTitle("Traducción");
        alertDialog.setMessage("Jadisha: Foi muito difícil conseguir entender bem aos brasileiros.\n" +
                "Manoel: Quando cheguei eu não sabia nada do idioma e não conhecia ninguém." +
                " Por sorte, na viagem eu encontrei pessoas que são da minha cidade no Equador." +
                " Conversei com eles e eles moram aqui em Campinas. Então vim com eles até Campinas e me" +
                " trouxeram até uma pensão universitária perto do terminal. Então eu fiquei ali" +
                " por um dia até encontrar onde morar.\n" +
                "Marcelo: Bom, eu já conhecia antes o Brasil. Na segunda vez que cheguei aqui, foi… no dia em que" +
                " cheguei, estava chovendo bastante quando sai com o meu tio, veio buscar a mim e ao meu pai," +
                " veio nos buscar no aeroporto de Guarulhos, ficamos na cada dele e no dia seguinte pegamos" +
                " um ônibus para vir conhecer Campinas e a Unicamp.");
        alertDialog.setCanceledOnTouchOutside(true);


        RelativeLayout relativeLayout = (RelativeLayout)findViewById(R.id.accion0);
        relativeLayout.setOnTouchListener (
                new RelativeLayout.OnTouchListener() {

                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        switch (event.getAction() & MotionEvent.ACTION_MASK) {
                            case MotionEvent.ACTION_DOWN:
                                // This happens when you touch the screen with two fingers
                                mode = SWIPE;
                                // You can also use event.getY(1) or the average of the two
                                startX = event.getX(0);
                                break;

                            case MotionEvent.ACTION_UP:
                                // This happens when you release the second finger
                                mode = NONE;
                                if (Math.abs(startX - stopX) > TRESHOLD) {
                                    if (startX > stopX) {
                                        Intent intent = new Intent(v.getContext(), Accion2Activity.class);
                                        startActivity(intent);
                                    } else {
                                        Intent intent = new Intent(v.getContext(), Accion1Activity.class);
                                        startActivity(intent);
                                    }
                                }
                                mode = NONE;
                                break;

                            case MotionEvent.ACTION_MOVE:
                                if (mode == SWIPE) {
                                    stopX = event.getX(0);
                                }
                                break;
                        }

                        return true;
                    }
                });
        ImageButton next2 = (ImageButton) findViewById(R.id.button_id2);
        next2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                alertDialog.show();
            }
        });
    }


    @Override
    public void onInitializationFailure(YouTubePlayer.Provider provider,
                                        YouTubeInitializationResult result) {

        if (result.isUserRecoverableError()) {
            result.getErrorDialog(this, RQS_ErrorDialog).show();
            System.out.println("abc");
        } else {
            Toast.makeText(this,
                    "YouTubePlayer.onInitializationFailure(): " + result.toString(),
                    Toast.LENGTH_LONG).show();
            System.out.println("cde");
        }
    }


    @Override
    public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer player, boolean wasRestored) {
        /** add listeners to YouTubePlayer instance **/
        player.setPlayerStateChangeListener(playerStateChangeListener);
        player.setPlaybackEventListener(playbackEventListener);

        /** Start buffering **/
        if (!wasRestored) {
            player.cueVideo(VIDEO_ID);
        }
    }

    private YouTubePlayer.PlaybackEventListener playbackEventListener = new YouTubePlayer.PlaybackEventListener() {

        @Override
        public void onBuffering(boolean arg0) {
        }

        @Override
        public void onPaused() {
        }

        @Override
        public void onPlaying() {
        }

        @Override
        public void onSeekTo(int arg0) {
        }

        @Override
        public void onStopped() {
        }

    };

    private YouTubePlayer.PlayerStateChangeListener playerStateChangeListener = new YouTubePlayer.PlayerStateChangeListener() {

        @Override
        public void onAdStarted() {
        }

        @Override
        public void onError(YouTubePlayer.ErrorReason arg0) {
        }

        @Override
        public void onLoaded(String arg0) {
        }

        @Override
        public void onLoading() {
        }

        @Override
        public void onVideoEnded() {
        }

        @Override
        public void onVideoStarted() {
        }

    };
}